import React from "react";
import { useParams } from "react-router-dom";

import Error401 from "./error401"
import Error403 from "./error403"
import Error404 from "./error404"
import Error500 from "./error500"
import ErrorXXX from "./ErrorXXX"


function Error(){
    let {id} = useParams();

    if(id === '401'){
        return <Error401 />
    }
    else if (id === '403'){
        return <Error403 />
    }
    else if (id === '404'){
        return <Error404 />
    }
    else if (id === '500'){
        return <Error500 />
    }
    else {
        return <ErrorXXX />
    }
}
export default Error;
import React from 'react';


import NavBar from '../Page-Components/Navbar';
import SideBar from '../Page-Components/Sidebar';
import Footer from '../Page-Components/Footer';

import { Outlet } from 'react-router-dom';

function PartialPageLayout(){
    return(
        <div className="container-scroller">
          <SideBar/>
        <div className="container-fluid page-body-wrapper">
          <NavBar/> 
          <div className="main-panel">
            <div className="content-wrapper">
              <Outlet/>
            </div>
            <Footer/> 
          </div>
        </div>
      </div>
    );

}
export default PartialPageLayout;
import React from 'react';

import '../Components/css/style.css';
import { Outlet } from 'react-router-dom';


function FullPageLayout(){
    return(
        <div className="container-scroller">
            <div className="container-fluid page-body-wrapper full-page-wrapper">
                <div className="main-panel" style={{padding: 0}}>
                    <div className="content-wrapper" style={{padding: 0}}>
                        

                        <Outlet />

                    </div>
                </div>
            </div>
        </div>
        
    );
}
export default FullPageLayout;
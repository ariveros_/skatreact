import React, { useContext, useEffect, useState } from "react";
import { Link} from 'react-router-dom';
import { NavLink, useLocation } from "react-router-dom";
import { Collapse, Dropdown, Nav } from 'react-bootstrap';
import {LoginContext} from '../Contexts/LoginContext'
import Logo from '../Components/images/logoSKAT.svg'
import LogoMini from '../Components/images/logoSKATmini.svg';
import '../Components/css/style.css'
import { event } from "jquery";
import url from '../enviroment';

function SideBar(){
 //Devolvera un sidebar dependiendo de si el user está logueado o no

 //Del context
const {logueado} = useContext(LoginContext);
    if(!logueado){
        return <SideBarNotLoginFunctional />
    }
    else {
        return <SidebarIfLogin />
    }
}

function SidebarIfLogin(){
//Devolverá el sidebar según el rol del user loggueado
const {userLogueado} = useContext(LoginContext);
    const [rol, setRol] = useState();
    //si existe el usuario, que se busque su rol
    useEffect(()=>{
        if(userLogueado.idRol !== undefined){
            getRole();
        }
    },[userLogueado])
    async function getRole(){
        const response = await fetch(url+`/RolsAPI/ConsultarRol?rolID=${userLogueado.idRol}`,{
            credentials: "include"
        })
        if(response.status === 200){
            let actualData = await response.json()
            setRol(actualData)
        }
    }
    return(
        <SideBarLoginFunctional rol={rol} />
    );

}
function SideBarNotLoginFunctional(){
    const[isActive, setIsActive] = useState(false)
    const [isActiveContact, setIsActiveContact] = useState(false)

    const handleClick = event =>{
        setIsActive (true)
        setIsActiveContact(false)
    }
    const handleClickContact = event =>{
        setIsActiveContact (true)
        setIsActive(false)
    }

    return (
        <nav className="sidebar sidebar-offcanvas" id="sidebar">
            <div className="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">
                <a className="sidebar-brand brand-logo" href="/"><img src={Logo} alt="logo" /></a>
                <a className="sidebar-brand brand-logo-mini" href="/"><img src={LogoMini} alt="logo" /></a>
            </div>
            <ul className="nav">
                <li className="nav-item nav-category">
                    <span className="nav-link">Opciones</span>
                </li>
                <li exact className={isActive? 'nav-item menu-items active':'nav-item menu-items'} onClick={handleClick}>
                    <NavLink to="/" className="nav-link" activeclassname ="active">
                    <span className="menu-icon"><i className="mdi mdi-home"></i></span>
                    <span className="menu-title">Inicio</span>
                    </NavLink>
                </li>
                <li className='nav-item menu-items'>
                    <NavLink to="/Login" exact className="nav-link" activeclassname = "active">
                    <span className="menu-icon"><i className="mdi mdi-login"></i></span>
                    <span className="menu-title">Iniciar sesión</span>
                    </NavLink>
                </li>
                <li className="nav-item nav-category">
                    <span className="nav-link">Más</span>
                </li>
                {/* Agregar coso para contacto */}
                <li exact className={isActiveContact? 'nav-item menu-items active' : 'nav-item menu-items'} onClick={handleClickContact}>
                    <NavLink to="/Contacto" exact className="nav-link" activeclassname = "active">
                    <span className="menu-icon"><i className="mdi mdi-security"></i></span>
                    <span className="menu-title">Contacto</span>
                    </NavLink>
                </li>
            </ul>
        </nav>
    );

}

// function SideBarLoginFunctional({ rol }) {
//     const location = useLocation();
//     const [selectedMenu, setSelectedMenu] = useState("");
//     const [menuIsOpen, setMenuIsOpen] = useState("");

//     // Synchronize selectedMenu with the current URL
//     useEffect(() => {
//         const currentPath = location.pathname;
//         rol.menuVMs.forEach(menu => {
//             if (menu.urlMenu === currentPath) {
//                 setSelectedMenu(menu.nombreMenu);
//                 if (menu.idPadre) {
//                     setMenuIsOpen(menu.nombreMenuOpen);
//                 }
//             }
//             if (menu.submenus) {
//                 menu.submenus.forEach(submenu => {
//                     if (submenu.urlMenu === currentPath) {
//                         setSelectedMenu(submenu.nombreMenu);
//                         setMenuIsOpen(menu.nombreMenuOpen);
//                     }
//                 });
//             }
//         });
//     }, [location.pathname, rol.menuVMs]);

//     const isMenuOpen = (nombreMenuOpen) => menuIsOpen === nombreMenuOpen;
//     const changeMenu = (nombreMenu) => setMenuIsOpen(menuIsOpen === nombreMenu ? "" : nombreMenu);

//     return (
//         <nav className="sidebar sidebar-offcanvas" id="sidebar">
//             <div className="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">
//                 <a className="sidebar-brand brand-logo" href="/"><img src={Logo} alt="logo" /></a>
//                 <a className="sidebar-brand brand-logo-mini" href="/"><img src={LogoMini} alt="logo" /></a>
//             </div>
//             <ul className="nav">
//                 <li className="nav-item nav-category">
//                     <span className="nav-link">Opciones</span>
//                 </li>
//                 {rol && rol.menuVMs && rol.menuVMs.map(menu => (
//                     <li key={menu.idMenu} className={`nav-item menu-items ${selectedMenu === menu.nombreMenu ? "active" : ""}`}>
//                         {menu.esPadre ? (
//                             <>
//                                 <div className={`nav-link ${isMenuOpen(menu.nombreMenuOpen) ? "menu-expanded" : ""}`} onClick={() => changeMenu(menu.nombreMenuOpen)} data-toggle='collapse'>
//                                     <span className='menu-icon'><i className={menu.iconoMenu}></i></span>
//                                     <span className='menu-title'>{menu.nombreMenu}</span><i className='menu-arrow'></i>
//                                 </div>
//                                 <Collapse in={isMenuOpen(menu.nombreMenuOpen)}>
//                                     <div>
//                                         <ul className='nav flex-column sub-menu'>
//                                             {menu.submenus && menu.submenus.map(submenu => (
//                                                 <li key={submenu.idMenu} className='nav-item'>
//                                                     <NavLink to={submenu.urlMenu} className='nav-link' activeclassname="active">
//                                                         {submenu.nombreMenu}
//                                                     </NavLink>
//                                                 </li>
//                                             ))}
//                                         </ul>
//                                     </div>
//                                 </Collapse>
//                             </>
//                         ) : (
//                             <NavLink to={menu.urlMenu} className="nav-link" activeclassname="active" onClick={() => setSelectedMenu(menu.nombreMenu)}>
//                                 <span className="menu-icon"><i className={menu.iconoMenu}></i></span>
//                                 <span className="menu-title">{menu.nombreMenu}</span>
//                             </NavLink>
//                         )}
//                     </li>
//                 ))}
//             </ul>
//         </nav>
//     );
// }

function SideBarLoginFunctional({rol}){
    const [selectedMenu, setSelectedMenu] = useState("Home");

    const isInicio = selectedMenu === "Inicio";
    const isHome = selectedMenu === "Home";
    const isETDistro = selectedMenu === "Reportes";

    const isETExterna = selectedMenu ==="ReportesExterna";
    const isAdmin = selectedMenu === "Admin";

    const [menuIsOpen, setMenuIsOpen] = useState("");

    const isMenuOpen = menuIsOpen ==="etMenuOpen";
    const isUserPagesMenuOpen = menuIsOpen ==="userPagesMenuOpen";
    const isMenuExternasOpen = menuIsOpen ==="etExternasOpen";
    
    const hasMenu = (menuName) => {
        return rol && rol.menuVMs.some(menu => menu.nombreMenu === menuName);
    };
    const changeMenu = (nombreMenu)=> {
        if(menuIsOpen === ""){
            setMenuIsOpen(nombreMenu)
            // console.log(menuIsOpen + "esto lo asigna")
        }else{
            setMenuIsOpen("")
            // console.log(menuIsOpen + "esto lo vacia")
        }
    }
    
    
    return(
        <nav className="sidebar sidebar-offcanvas" id="sidebar">
            <div className="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">
                <a className="sidebar-brand brand-logo" href="/"><img src={Logo} alt="logo" /></a>
                <a className="sidebar-brand brand-logo-mini" href="/"><img src={LogoMini} alt="logo" /></a>
            </div>
            <ul className="nav">
                <li className="nav-item nav-category">
                    <span className="nav-link">Opciones</span>
                </li>
                {hasMenu("Inicio") && (
                    <li className={`nav-item menu-items ${isInicio ? "active" : ""}`} onClick={() => setSelectedMenu ("Inicio")}>
                        <NavLink to="/" className="nav-link" activeclassname ="active">
                                <span className="menu-icon"><i className="mdi mdi-home"></i></span>
                                <span className="menu-title">Inicio</span>
                        </NavLink>
                    </li>
                )}
                {hasMenu("Home") && (
                    <li className={`nav-item menu-items ${isHome ? "active" : ""}`} onClick={() => setSelectedMenu ("Home")}>
                        <NavLink className = 'nav-link' to = '/Home'>
                            <span className='menu-icon'><i className='mdi mdi-chart-line'></i></span>
                            <span className = 'menu-title' >Home</ span >
                        </NavLink>
                    </ li >  
                )}
                
                {hasMenu("EE.TT. Distrocuyo") && (
                <li className={`nav-item menu-items ${isETDistro? "active":""}`} onClick={() => setSelectedMenu ("Reportes")}> 
                    <div className={ `nav-link ${isMenuOpen? "menu-expanded":""} `} onClick={ () => changeMenu('etMenuOpen') } data-toggle='collapse'>
                        <span className='menu-icon'>< i className ='mdi mdi-home-modern'></i> </span >
                        < span className='menu-title'> EE. TT. Distrocuyo</span> <i className = 'menu-arrow'> </i> 
                    </div>
                    <Collapse in={ isMenuOpen}>
                        <div> 
                            <ul className='nav flex-column sub-menu'>
                                <li className='nav-item'> <NavLink to='/Reportes/ET/Anchoris' className='nav-link' activeclassname ="active" >Anchoris</NavLink></li>  
                                <li className='nav-item'> <NavLink to='/Reportes/ET/BRT' className='nav-link' activeclassname = "active" >Bajo Río Tunuyán</NavLink></li>  
                                <li className='nav-item'> <NavLink to='/Reportes/ET/CaniadaHonda' className='nav-link' activeclassname ="active" >Cañada Honda</NavLink></li>  
                                <li className='nav-item'> <NavLink to='/Reportes/ET/Capiz' className='nav-link' activeclassname ="active" >Capiz</NavLink></li>  
                                <li className='nav-item'> <NavLink to='/Reportes/ET/CDP' className= 'nav-link' activeclassname = "active" >Cruz De Piedra</NavLink></li>
                                <li className='nav-item'> <NavLink to='/Reportes/ET/Lujan' className='nav-link' activeclassname = "active" >Luján de Cuyo</NavLink></li>
                                <li className='nav-item'> <NavLink to='/Reportes/ET/Montecaseros' className='nav-link' activeclassname = "active" >Montecaseros</NavLink></li>
                                <li className='nav-item'> <NavLink to='/Reportes/ET/PedroVargas' className='nav-link' activeclassname = "active" >Pedro Vargas</NavLink></li>
                                <li className='nav-item'> <NavLink to='/Reportes/ET/SanRafel' className='nav-link' activeclassname = "active" >San Rafael</NavLink></li>
                                <li className='nav-item'> <NavLink to='/Reportes/ET/SanJuan' className='nav-link' activeclassname = "active" >San Juan</NavLink></li>
                                
                            </ul> 
                        </div> 
                    </Collapse>
                </li>)}

                {hasMenu("EE.TT. Externas") &&(
                    <li className={`nav-item menu-items ${isETExterna ? "active":""}`} onClick={() => setSelectedMenu ("ReportesExterna")}> 
                    <div className={ `nav-link ${isMenuExternasOpen? "menu-expanded":""} `} onClick={ () => changeMenu('etExternasOpen') } data-toggle='collapse'>
                        <span className='menu-icon'>< i className ='mdi mdi-home-modern'></i> </span >
                        < span className='menu-title'> Otras EE.TT.</span> <i className = 'menu-arrow'> </i> 
                    </div>
                    <Collapse in={ isMenuExternasOpen}>
                        <div> 
                            <ul className='nav flex-column sub-menu'>
                                <li className='nav-item'> <NavLink to='/Reportes/ET/GMdz' className='nav-link' activeclassname ="active" >Gran Mendoza</NavLink></li>  
                                <li className='nav-item'> <NavLink to='/Reportes/ET/Guaymallen' className='nav-link' activeclassname = "active" >Guaymallén</NavLink></li>
                                <li className='nav-item'> <NavLink to='/Reportes/ET/LaBebida' className='nav-link' activeclassname = "active" >La Bebida</NavLink></li>
                                <li className='nav-item'> <NavLink to='/Reportes/ET/LasHeras' className='nav-link' activeclassname = "active" >Las Heras</NavLink></li>
                                <li className='nav-item'> <NavLink to='/Reportes/ET/RioDiamante' className='nav-link' activeclassname = "active" >Río Diamante</NavLink></li>
                                <li className='nav-item'> <NavLink to='/Reportes/ET/RodeoCruz' className='nav-link' activeclassname = "active" >Rodeo de la Cruz</NavLink></li>
                                <li className='nav-item'> <NavLink to='/Reportes/ET/SanMartin' className='nav-link' activeclassname = "active" >San Martín</NavLink></li>
                                <li className='nav-item'> <NavLink to='/Reportes/ET/Sarmiento' className='nav-link' activeclassname = "active" >Sarmiento</NavLink></li>
                                <li className='nav-item'> <NavLink to='/Reportes/ET/SE100YPF' className='nav-link' activeclassname = "active" >SE100 YPF</NavLink></li>

                            </ul> 
                        </div> 
                    </Collapse>
                </li> )}
                
                { hasMenu("Configuración") &&(
                    <li className={`nav-item menu-items ${isAdmin ? "active" : ""} `} onClick={() => setSelectedMenu ("Admin")}>
                        <div className={ `nav-link ${isUserPagesMenuOpen ? "menu-expanded" : ""}`} onClick={ () =>changeMenu('userPagesMenuOpen') } data-toggle="collapse">
                            <span className="menu-icon">
                                <i className="mdi mdi-security"></i>
                            </span>
                            <span className="menu-title">Configuración</span>
                            <i className="menu-arrow"></i>
                        </div>
                        <Collapse in={ isUserPagesMenuOpen}>
                            <div>
                                <ul className="nav flex-column sub-menu">
                                    
                                    <li className="nav-item"> <NavLink to="Admin/Seguridad" className='nav-link' activeclassname = "active" >Seguridad</NavLink></li>
                                    <li className="nav-item"> <NavLink to="Admin/Roles" className='nav-link' activeclassname ="active" >Roles</NavLink></li>
                                    <li className="nav-item"> <NavLink to="Admin/Usuarios" className='nav-link' >Usuarios</NavLink></li>
                                </ul>
                            </div>
                        </Collapse>
                    </li>)}
                </ul>
                </nav>
        );
}


export default SideBar;
import React,{ useContext,useEffect} from 'react';
import { Dropdown } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import Logo from '../Components/images/logoSKATmini.svg';
import '../Components/css/style.css'
import '../Components/vendors/mdi/css/materialdesignicons.min.css';
import {LoginContext} from '../Contexts/LoginContext'
import url from "../enviroment"

function NavBar(props){
  //Context
  const {logueado} = useContext(LoginContext);

  if(logueado){
    return <NavBarIfLogin />
  }
  else {
    return <NavBarNotLogin />
  }
}

function NavBarNotLogin(){

  return (
    <>
  <nav className="navbar p-0 fixed-top d-flex flex-row">
        <div className="navbar-brand-wrapper d-flex d-lg-none align-items-center justify-content-center">
          <a className="navbar-brand brand-logo-mini" href='/'><img src={Logo} alt="logo" fluid="true" width={50} height={30}/></a>
        </div>
        <div className="navbar-menu-wrapper flex-grow d-flex align-items-stretch">
          <button className="navbar-toggler align-self-center" type="button" onClick={ () => document.body.classList.toggle('sidebar-icon-only') }>
            <span className="mdi mdi-menu"></span>
          </button>
          <ul className="navbar-nav w-100">
            <li className="nav-item w-100">
              {/* <form className="nav-link mt-2 mt-md-0 d-none d-lg-flex search">
                {/* <input type="text" className="form-control" placeholder="Search products" /> 
              </form> */}
            </li>
          </ul>
          <ul className="navbar-nav navbar-nav-right">
            {/* <div className="navbar-profile">
                  <p className="mb-0 d-none d-sm-block navbar-profile-name">Bienvenido</p>
                  {/* <i className="mdi mdi-menu-down d-none d-sm-block"></i> </div> */}
              
            <Dropdown alignright="true" as="li" className="nav-item">
              <Dropdown.Toggle as="a" className="nav-link cursor-pointer no-caret">
                <div className="navbar-profile">
                  <p className="mb-0 d-none d-sm-block navbar-profile-name">Bienvenido</p>
                  {/* <i className="mdi mdi-menu-down d-none d-sm-block"></i> */}
                </div>
              </Dropdown.Toggle>
              <Dropdown.Menu className="navbar-dropdown preview-list navbar-profile-dropdown-menu">
                <h6 className="p-3 mb-0">Más</h6>
                <Dropdown.Divider />
                <Dropdown.Divider />
                <Dropdown.Item href='/Login'   className="preview-item">
                  <div className="preview-thumbnail">
                    <div className="preview-icon bg-dark rounded-circle">
                    <span className="menu-icon text-info"><i className="mdi mdi-login"></i></span>
                    </div>
                  </div>
                  <div className="preview-item-content">
                  <span className="menu-title">Iniciar sesión</span>
                  </div>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </ul>
          <button className="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" >
            <span className="mdi mdi-format-line-spacing"></span>
          </button>
        </div>
      </nav>
  
    </>
    );

}


function NavBarIfLogin(){
  //Context
  const {setLogueado} = useContext(LoginContext);
  const {userLogueado} = useContext(LoginContext);
  const {setUserLogueado} = useContext(LoginContext);

  let navigate = useNavigate(); 
  let path = '/'; 
  

// useEffect(()=>{
//   checkAuthStatus();
// },[])
// async function checkAuthStatus() {
//   const response = await fetch(url+`/AuthAPI/usuario`,{
//       credentials: "include"
//   });
//   if (response.status === 200) {
//     let actualData = await response.json();
//     setUserLogueado(actualData);
//     setLogueado(true);
//     localStorage.setItem('userExists', 'true');
//   } else {
//     setLogueado(false);
//     setUserLogueado({})
//     localStorage.removeItem('userExists');
//     logOut()

//   }
// }
  const logOut = async () => {
    await fetch(url+`/AuthAPI/logout`,{
        method: 'POST',
        headers: {'Content-type': 'application/json'},
        credentials: 'include',
    }).then(response=> response.json()).catch(error => console.log(error))
    setLogueado(false);
    localStorage.removeItem('userExists');
    setUserLogueado({});
    navigate(path);
  }
  return (
    <>
  <nav className="navbar p-0 fixed-top d-flex flex-row">
        <div className="navbar-brand-wrapper d-flex d-lg-none align-items-center justify-content-center">
          <a className="navbar-brand brand-logo-mini" href='/'><img src={Logo} alt="logo" fluid="true" width={50} height={30}/></a>
        </div>
        <div className="navbar-menu-wrapper flex-grow d-flex align-items-stretch">
          <button className="navbar-toggler align-self-center" type="button" onClick={ () => document.body.classList.toggle('sidebar-icon-only') }>
            <span className="mdi mdi-menu"></span>
          </button>
          <ul className="navbar-nav w-100">
            <li className="nav-item w-100">
              <form className="nav-link mt-2 mt-md-0 d-none d-lg-flex search">
                {/* <input type="text" className="form-control" placeholder="Search products" /> */}
              </form>
            </li>
          </ul>
          <ul className="navbar-nav navbar-nav-right">
            <Dropdown alignright="true" as="li" className="nav-item d-none d-lg-block">
                {/* <Dropdown.Toggle className="nav-link btn btn-success create-new-button no-caret">
                + Create New Project
                </Dropdown.Toggle>
                <Dropdown.Menu className="navbar-dropdown preview-list create-new-dropdown-menu">
                  <h6 className="p-3 mb-0">Projects</h6>
                  <Dropdown.Divider />
                  <Dropdown.Item href="!#" onClick={evt =>evt.preventDefault()} className="preview-item">
                    <div className="preview-thumbnail">
                      <div className="preview-icon bg-dark rounded-circle">
                        <i className="mdi mdi-file-outline text-primary"></i>
                      </div>
                    </div>
                    <div className="preview-item-content">
                      <p className="preview-subject ellipsis mb-1">Software Development</p>
                    </div>
                  </Dropdown.Item>
                  <Dropdown.Divider />
                  <Dropdown.Item href="!#" onClick={evt =>evt.preventDefault()} className="preview-item">
                    <div className="preview-thumbnail">
                      <div className="preview-icon bg-dark rounded-circle">
                        <i className="mdi mdi-web text-info"></i>
                      </div>
                    </div>
                    <div className="preview-item-content">
                      <p className="preview-subject ellipsis mb-1">UI Development</p>
                    </div>
                  </Dropdown.Item>
                  <Dropdown.Divider />
                  <Dropdown.Item href="!#" onClick={evt =>evt.preventDefault()} className="preview-item">
                    <div className="preview-thumbnail">
                      <div className="preview-icon bg-dark rounded-circle">
                        <i className="mdi mdi-layers text-danger"></i>
                      </div>
                    </div>
                    <div className="preview-item-content">
                      <p className="preview-subject ellipsis mb-1">Software Testing</p>
                    </div>
                  </Dropdown.Item>
                  <Dropdown.Divider />
                  <p className="p-3 mb-0 text-center">See all projects</p>
                </Dropdown.Menu> */}
              </Dropdown>
            {/* <li className="nav-item d-none d-lg-block">
              <a className="nav-link" href="!#" onClick={event => event.preventDefault()}>
                <i className="mdi mdi-view-grid"></i>
              </a>
            </li> */}
            <Dropdown alignright="true" as="li" className="nav-item border-left" >
              {/* <Dropdown.Toggle as="a" className="nav-link count-indicator cursor-pointer">
                <i className="mdi mdi-email"></i>
                <span className="count bg-success"></span>
              </Dropdown.Toggle>
              <Dropdown.Menu className="navbar-dropdown preview-list">
                  <h6 className="p-3 mb-0">Messages</h6>
                  <Dropdown.Divider />
                  <Dropdown.Item href="!#" onClick={evt =>evt.preventDefault()} className="preview-item">
                    <div className="preview-thumbnail">
                      <div className="preview-icon bg-dark rounded-circle">
                        
                      </div>
                    </div>
                    <div className="preview-item-content">
                      <p className="preview-subject ellipsis mb-1">Mark send you a message</p>
                      <p className="text-muted mb-0"> 1 Minutes ago </p>
                    </div>
                  </Dropdown.Item>
                  <Dropdown.Divider />
                  <Dropdown.Item href="!#" onClick={evt =>evt.preventDefault()} className="preview-item">
                    <div className="preview-thumbnail">
                      <div className="preview-icon bg-dark rounded-circle">
                        
                      </div>
                    </div>
                    <div className="preview-item-content">
                      <p className="preview-subject ellipsis mb-1">Cregh send you a message</p>
                      <p className="text-muted mb-0"> 15 Minutes ago </p>
                    </div>
                  </Dropdown.Item>
                  <Dropdown.Divider />
                  <Dropdown.Item href="!#" onClick={evt =>evt.preventDefault()} className="preview-item">
                    <div className="preview-thumbnail">
                      <div className="preview-icon bg-dark rounded-circle">
                        
                      </div>
                    </div>
                    <div className="preview-item-content">
                      <p className="preview-subject ellipsis mb-1">Profile picture updated</p>
                      <p className="text-muted mb-0"> 18 Minutes ago</p>
                    </div>
                  </Dropdown.Item>
                  <Dropdown.Divider />
                  <p className="p-3 mb-0 text-center">4 new messages</p>
                </Dropdown.Menu>
            </Dropdown>
            <Dropdown alignright="true" as="li" className="nav-item border-left">
              <Dropdown.Toggle as="a" className="nav-link count-indicator cursor-pointer">
                <i className="mdi mdi-bell"></i>
                <span className="count bg-danger"></span>
              </Dropdown.Toggle>
              <Dropdown.Menu className="dropdown-menu navbar-dropdown preview-list">
                <h6 className="p-3 mb-0">Notifications</h6>
                <Dropdown.Divider />
                <Dropdown.Item className="dropdown-item preview-item" onClick={evt =>evt.preventDefault()}>
                  <div className="preview-thumbnail">
                    <div className="preview-icon bg-dark rounded-circle">
                      <i className="mdi mdi-calendar text-success"></i>
                    </div>
                  </div>
                  <div className="preview-item-content">
                    <p className="preview-subject mb-1">Event today</p>
                    <p className="text-muted ellipsis mb-0">
                    Just a reminder that you have an event today
                    </p>
                  </div>
                </Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item className="dropdown-item preview-item" onClick={evt =>evt.preventDefault()}>
                  <div className="preview-thumbnail">
                    <div className="preview-icon bg-dark rounded-circle">
                      <i className="mdi mdi-settings text-danger"></i>
                    </div>
                  </div>
                  <div className="preview-item-content">
                    <h6 className="preview-subject mb-1">Settings</h6>
                    <p className="text-muted ellipsis mb-0">
                    Update dashboard
                    </p>
                  </div>
                </Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item className="dropdown-item preview-item" onClick={evt =>evt.preventDefault()}>
                  <div className="preview-thumbnail">
                    <div className="preview-icon bg-dark rounded-circle">
                      <i className="mdi mdi-link-variant text-warning"></i>
                    </div>
                  </div>
                  <div className="preview-item-content">
                    <h6 className="preview-subject mb-1">Launch Admin</h6>
                    <p className="text-muted ellipsis mb-0">
                    New admin wow!
                    </p>
                  </div>
                </Dropdown.Item>
                <Dropdown.Divider />
                <p className="p-3 mb-0 text-center">See all notifications</p>
              </Dropdown.Menu> */}
            </Dropdown>
            <Dropdown alignright="true" as="li" className="nav-item">
              <Dropdown.Toggle as="a" className="nav-link cursor-pointer no-caret">
                <div className="navbar-profile">
                  <p className="mb-0 d-none d-sm-block navbar-profile-name">{userLogueado.email}</p>
                  <i className="mdi mdi-menu-down d-none d-sm-block"></i>
                </div>
              </Dropdown.Toggle>

              <Dropdown.Menu className="navbar-dropdown preview-list navbar-profile-dropdown-menu">
                <h6 className="p-3 mb-0">Perfil</h6>
                <Dropdown.Divider />
                <Dropdown.Item href="!#" onClick={evt =>evt.preventDefault()} className="preview-item">
                  <div className="preview-thumbnail">
                    <div className="preview-icon bg-dark rounded-circle">
                      <i className="mdi mdi-settings text-success"></i>
                    </div>
                  </div>
                  <div className="preview-item-content">
                    <Link className="nav-link" to={`Usuarios/CambiarContrasenia/${userLogueado.userID}`}>
                      <span className="menu-title">Cambiar Contraseña</span>
                    </Link>
                  </div>
                </Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item href="!#" onClick={evt =>evt.preventDefault()}  className="preview-item">
                  <div className="preview-thumbnail">
                    <div className="preview-icon bg-dark rounded-circle">
                      <i className="mdi mdi-logout text-danger"></i>
                    </div>
                  </div>
                  <div className="preview-item-content">
                    <button className='btn' onClick={()=>logOut()}>Cerrar Sesión</button>
                  </div>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </ul>
          <button className="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" >
            <span className="mdi mdi-format-line-spacing"></span>
          </button>
        </div>
      </nav>
  
    </>
    );

}

export default NavBar;

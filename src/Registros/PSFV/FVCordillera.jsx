import React from "react";
import Provisional from "../Provisional";

function FVCordillera(){
    return(
        <>
        <Provisional nombreEstacion="PFV Cordillera" categoriaEstacion="Parques Fotovoltaicos" />
        </>
    );
}
export default FVCordillera;
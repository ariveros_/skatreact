import React from "react";
import Provisional from "../Provisional";

function PSFVCañadaHonda(){
    return(
        <>
        <Provisional nombreEstacion="PSFV Cañada Honda" categoriaEstacion="Parques Fotovoltaicos" />
        </>
    );
}
export default PSFVCañadaHonda;
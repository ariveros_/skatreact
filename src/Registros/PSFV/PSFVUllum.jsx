import React from "react";
import Provisional from "../Provisional";

function PSFVUllum(){
    return(
        <>
        <Provisional nombreEstacion="PSFV Ullum" categoriaEstacion="Parques Fotovoltaicos" />
        </>
    );
}
export default PSFVUllum;
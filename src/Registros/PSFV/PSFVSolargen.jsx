import React from "react";
import Provisional from "../Provisional";

function PSFVSolargen(){
    return(
        <>
        <Provisional nombreEstacion="PSFV Solargen" categoriaEstacion="Parques Fotovoltaicos" />
        </>
    );
}
export default PSFVSolargen;
import React, { useRef, useState } from "react";
import DatePicker from "react-datepicker";
import es from 'date-fns/locale/es';
import "react-datepicker/dist/react-datepicker.css";
import setHours from "date-fns/setHours";
import setMinutes from "date-fns/setMinutes";
import addDays from "date-fns/addDays";
import "../../Components/css/maps/datepicker.css";



function Query({startDate, setStartDate, endDate, setEndDate,setSearch,porcentajeCargaTrafos,setPorcentajeCargaTrafos,porcentajeCargATRs,setPorcentajeCargATRs,tensionBarra,setTensionBarra}){
    const [masFiltros, setMasFiltros] = useState(false);
    const [warning, setWarning] = useState(false);
    //mantener los checkbox que ya quedan activos
    const checkbox = useRef();
    const limpiarFiltros = ()=>{
        setStartDate(new Date('2022-07-05T00:00:00'))
        setEndDate(new Date('2022-07-05T23:59:00'))
        setSearch(false)
        if(masFiltros === true){
            setMasFiltros(false)
        }
    }

    //resetea los valores de los checks
    const activarCancelarFiltros = ()=>{
        setMasFiltros(!masFiltros)
        if(masFiltros === true){
            setPorcentajeCargaTrafos(false)
            setPorcentajeCargATRs(false)
            setTensionBarra(false)
        }

    }
    // const onSubmit = (e) => {
    //     e.preventDefault()
    //     setSearch(true)
    // }

    const onSubmit = (e) =>{
        e.preventDefault()
            setSearch(false)
            setTimeout(()=>{
                setSearch(true)
            },100)
    }
        const handleStartDateChange = (date) => {
            setStartDate(date)
            const newEndDate = new Date(date)
            newEndDate.setHours(23);
            newEndDate.setMinutes(59);
            setEndDate(newEndDate)
            setWarning(false)
        }
        const handleEndDateChange = (date) => {
            setEndDate(date)
            if(date && date > addDays (startDate, 4)){
                setWarning(true)           
        } else{
            setWarning(false)
            }
    }
    const handelEndDateCalendarClick =(date) => {
        if (date > addDays(startDate, 4)) {
            setWarning(true)
        }else{
            setWarning(false)
        }
    }
    return(
        <>
        <div className="row">
            <div className="col-md-12 grid-margin stretch-card">
                <div className="card">
                    <div className="card-body">
                        <h4 className="card-title">Buscador</h4>
                        <hr />
                        <p className="card-description">Determine qué período de fechas desea ver</p>
                        <form>
                                <div className='row g-0'>
                                    <label className="col-sm-1 col-form-label"> Inicio</label>
                                    <div className="col-sm-3 col-md-2">
                                        <DatePicker className="form-control w-100"
                                        selected={startDate} 
                                        onChange={handleStartDateChange}
                                        showTimeSelect
                                        locale={es}
                                        timeFormat="HH:mm"
                                        timeIntervals={5}
                                        timeCaption="hora"
                                        injectTimes={[
                                        setHours(setMinutes(new Date(), 59), 23),
                                    ]}
                                        dateFormat="dd/MM/yyyy h:mm aa"
                                        />
                                    </div>
                                    <label className="col-sm-1 col-form-label ps-3"> Fin</label>
                                    <div className="col-sm-3 col-md-2">
                                        <DatePicker className="form-control w-100"
                                            selected={endDate} 
                                            onChange={handleEndDateChange}
                                            // onClickOutside={() => setWarning(false)}
                                            onSelect={handelEndDateCalendarClick}
                                            showTimeSelect
                                            timeFormat="HH:mm"
                                            locale={es}
                                            timeIntervals={5}
                                            timeCaption="hora"
                                            injectTimes={[
                                                setHours(setMinutes(new Date(), 59), 23),
                                            ]}
                                            dateFormat="dd/MM/yyyy h:mm aa"
                                            minDate={startDate}
                                            maxDate={startDate ? addDays(startDate, 3): null}
                                            
                                        />
                                        {warning && ( <p className="text-danger">La fecha fin no puede ser mayor a 3 dias de la fecha inicio</p>)}
                                    </div>
                                    <div className='form-group col-md-4 ps-2'>
                                        <div className="row g-0">
                                            <div className="col-md-2">
                                                <button type="button" className="btn btn-primary btn-icon btn-rounded" title="Buscar" onClick={(e)=>onSubmit(e)}><i className="mdi mdi-magnify btn-icon-prepend"></i></button>
                                            </div>
                                            <div className="col-md-4 ps-2 ">
                                                <button type="button" className="btn btn-outline-danger btn-icon btn-rounded" title="Borrar filtros" onClick={limpiarFiltros}><i className="mdi mdi-delete-forever btn-icon-prepend"></i></button>
                                            </div>
                                            <div className="col-md-5">
                                                {masFiltros ? 
                                                    <button type="button" className={"btn btn-inverse-warning btn-icon-text btn-rounded"} 
                                                    onClick={activarCancelarFiltros}><i className={"mdi mdi-playlist-remove btn-icon-prepend"}
                                                    ></i>{"Borrar Gráficos Extra"}</button>
                                                :
                                                    <button type="button" className={"btn btn-inverse-primary btn-icon-text btn-rounded"} 
                                                    onClick={activarCancelarFiltros}><i className={"mdi mdi-playlist-plus btn-icon-prepend"}
                                                    ></i>{"Administrar Gráficos"}</button>
                                                }
                                            </div>
                                            {/* <div className="col-md-4 ps-2 ">
                                                <button type="button" className="btn btn-inverse-danger btn-icon-text btn-rounded" title="Borrar filtros" onClick={limpiarFiltros}><i className="mdi mdi-delete-forever btn-icon-prepend"></i>Borrar Filtros</button>
                                            </div> */}
                                        </div>
                                    </div>
                                    {masFiltros && 
                                        <>
                                            <hr />
                                            <p className="card-description">Seleccione los gráficos adicionales que desea ver:</p>
                                            <div className="col-md-9">
                                                <div className="form-group px-2">
                                                <div className="row">
                                                    <div className="form-check col-md-4">
                                                        <label className="form-check-label">
                                                            <input type="checkbox" disabled className="form-check-input" checked/> <i className="input-helper"></i> Demanda Total
                                                        </label>
                                                    </div>
                                                    <div className="form-check col-md-4">
                                                        <label className="form-check-label">
                                                            <input type="checkbox" disabled className="form-check-input" checked/> <i className="input-helper"></i> Carga de Trafos
                                                        </label>
                                                    </div>
                                                    <div className="form-check col-md-4">
                                                        <label className="form-check-label">
                                                            <input type="checkbox" disabled className="form-check-input" checked/> <i className="input-helper"></i> Carga de Autotrafos
                                                        </label>
                                                    </div>
                                                    <div className="form-check col-md-4">
                                                        <label className="form-check-label">
                                                            <input type="checkbox" ref={checkbox} className="form-check-input" value={porcentajeCargaTrafos} onClick={()=>setPorcentajeCargaTrafos(!porcentajeCargaTrafos)}/> <i className="input-helper"></i> Porcentaje de carga de trafos
                                                        </label>
                                                    </div>
                                                    <div className="form-check col-md-4">
                                                        <label className="form-check-label">
                                                            <input type="checkbox" className="form-check-input" value={porcentajeCargATRs} onClick={()=>setPorcentajeCargATRs(!porcentajeCargATRs)}/> <i className="input-helper"></i> Porcentaje de carga de autotrafos
                                                        </label>
                                                    </div>
                                                    <div className="form-check col-md-4">
                                                        <label className="form-check-label">
                                                            <input type="checkbox" className="form-check-input" value={tensionBarra} onClick={()=> setTensionBarra(!tensionBarra)}/> <i className="input-helper"></i> Tensión de barras
                                                        </label>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </>}
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
        </>
    );
}
export default Query;
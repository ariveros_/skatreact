import React, {useEffect, useState} from "react";
import axios from "axios";
import url from "../../enviroment"
import { TablasEstaciones } from "../EstacionesTablas/TablasEstaciones";
import LoaderFetch from "../../Page-Components/LoaderFetch";

export const CorrienteTotalBarras = ({idEstacion,startDate,endDate})=>{

    const [data, setData] = useState()
    const [fetching, setFetching] = useState(false)
    const [error, setError] = useState()
    const [errorRequest, setErrorRequest] = useState()

    const urlFetchTabla = "/ReporteEstacion/TablaTotalCorrienteLineas"
    const isGrouped = false

    const fetchCarga = async()=>{
        const from = new Date(startDate.getTime() - startDate.getTimezoneOffset() * 60000).toISOString()
        const to = new Date(endDate.getTime() - endDate.getTimezoneOffset() * 60000).toISOString()

        try{
            setFetching(true)
            await axios.post(url+urlFetchTabla,
                {idEstacion:idEstacion, startDate: from, endDate: to},
                {withCredentials: true, responseType:'json'})
                .then((response)=>{
                    setData(response.data)
                    })
        }
        catch(error){
            if(error.response){
                if (error.response.status === 404) {
                    setErrorRequest(error.response.data.message)
                }else{
                    setError("Ha ocurrido un error interno")
                    console.log('Error: ', error.message);
                }
            }else{
                setError("Ha ocurrido un error interno")
                console.log('Error: ', error.message);
            }
        }
        finally{
            setFetching(false)
        } 
    }

    useEffect(()=>{
        if(!data ){
            fetchCarga()
        }
    },[data])

    return(
        <>
            <div className="col-md-12 grid-margin stretch-card">
                <div className="card">
                    <div className="card-body">
                        <h4 className="card-title">Corriente Total de barras</h4>
                        <br/>
                        {data && !error && 
                            data.map((tabla)=>(
                                <>
                                <hr />
                                    <TablasEstaciones
                                    key={tabla?.idTable}
                                        dataProps={tabla}
                                        isGrouped={isGrouped}
                                    />
                                </>
                            ))
                        }
                        {fetching &&
                            <div className="d-flex justify-content-center"> 
                                    <LoaderFetch />
                                </div>
                        }
                        {error && 
                        <p className="text-danger">{error}</p>}
                        {errorRequest && 
                        <p className="text-secondary">{errorRequest}</p>}
                    </div>
                </div>
            </div>
        </>
    );
}
import React, { useState, useEffect, useMemo } from "react";
import axios from "axios";
import url from "../../enviroment"
import LoaderFetch from "../../Page-Components/LoaderFetch";
import {useTable, usePagination, useGroupBy} from "react-table"

export const CargaMinMax = ({idEstacion, startDate, endDate}) => {
    const urlCargaMinMax = '/ReporteEstacion/TablaCargaMinMax'
    const [fetching, setFetching] = useState(false)
    const [error, setError] = useState()
    const [errorRequest, setErrorRequest] = useState()
    const [data, setData] = useState()

    const fetch = async () => {
        const from = new Date(startDate.getTime() - startDate.getTimezoneOffset() * 60000).toISOString()
        const to = new Date(endDate.getTime() - endDate.getTimezoneOffset() * 60000).toISOString()
        
        try{
            setFetching(true)
            await axios.post(url+urlCargaMinMax,
                {idEstacion:idEstacion, startDate: from, endDate: to},
                {withCredentials: true, responseType:'json'})
                .then((response)=>{
                    setData(response.data)
                    })
        }
        catch(error){
             if(error.response){
                if (error.response.status === 404) {
                    setErrorRequest(error.response.data.message)
                }else{
                    setError("Ha ocurrido un error interno")
                    console.log('Error: ', error.message);
                    
                }
            }else{
                setError("Ha ocurrido un error interno")
                console.log('Error: ', error.message);
            }
        }
        finally{
            setFetching(false)
        } 
    }

    useEffect(()=>{
        if(!data){
            fetch()
        }
    },[data])

    return(
        <>
            <div className="col-md-12 grid-margin stretch-card">
                <div className="card">
                    <div className="card-body">
                        <h4 className="card-title">Tabla de carga de Trafos y/o Autotrafos</h4>
                        <hr />
                        {fetching &&
                            <div className="d-flex justify-content-center"> 
                                <LoaderFetch />
                            </div>
                        }
                        {error && 
                            <p className="text-danger">{error} </p>
                        }
                        {errorRequest && 
                            <p className="text-secondary">{errorRequest} </p>
                        }
                        {data &&
                            <Tabla data={data} />
                        }
                        
                    </div>
                </div>
            </div>
        </>
    );
}

const Tabla = ({data}) => {
    const {titleTable, headersTable, subheaderTables,bodyTable} = data

    return(
        <div className="table-responsive">
            {<h6 className="text-warning"> {titleTable}</h6>}
            <table className='table table-bordered'>
                <thead>
                    <tr>
                        {headersTable.map((header)=>(
                            <th key={header.idHeader} colSpan="2" scope="col">{header.headerTitle}</th>
                        ))}
                    </tr>
                    <tr>
                        {subheaderTables.map((subheader)=>(
                            <th key={subheader.idHeader} scope="col">{subheader.headerTitle}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {bodyTable.map((row)=>(
                        <tr key={row.idHour}>
                        {row.rowValues.map((value)=>{
                            return <td>{value.valorMedida}{value.dateHour}</td>
                        })}
                        </tr>
                    ))}
                </tbody>
            </table>                
        </div>
    );
}

const Tabla2 = ({data}) => {
    const {titleTable,headersTable,subheaderTables,bodyTable} = data;

    const dataTable = useMemo(() => {
            return bodyTable.map((row) => {
                const rowData = {
                    hora : row.dateHour,
                };
                for (let counter = 0; counter < headersTable.length; counter++) {
                    const header = headersTable[counter];
                    rowData[header.headerTitle.toLowerCase().replace(/\s/g, "")] = (row.rowValues[counter].valorMedida ? row.rowValues[counter].valorMedida : row.rowValues[counter].dateHour);
                }
                // row.rowValues.forEach((value)=>{
                // const header = subheaderTables.find((h) => (h.idHeader) === value.orderValue - 1);
                // rowData[header.headerTitle.toLowerCase().replace(/\s/g, "")] = (value.valorMedida ? value.valorMedida : value.dateHour);

                // const subheader = subheaderTables.find((h) => (h.idHeader) === value.orderValue);
                // rowData[subheader.headerTitle.toLowerCase().replace(/\s/g, "")] = value.valorMedida

                // for (let index = 0; index < headersTable.subheader.length; index++) {
                //     const subheader = headersTable.subheader[index];
                //     rowData[subheader.headerTitle.toLowerCase().replace(/\s/g, "")] = value.subheaderValues[index];
                // }
            // })
            return rowData;
        });
        
    }, [bodyTable, headersTable]);

const columns = useMemo(() => {
        return headersTable.map((header) =>({
            Header: header.headerTitle,
            Footer: header.headerTitle.toLowerCase().replace(/\s/g, ""),
            columns:[
                {
                    Header: subheaderTables[0].headerTitle,
                    accessor: subheaderTables[0].headerTitle.toLowerCase().replace(/\s/g, ""),
                },
                {
                    Header: subheaderTables[1].headerTitle,
                    accessor: subheaderTables[1].headerTitle.toLowerCase().replace(/\s/g, ""),
                }
            ]
        }));
}, [headersTable, subheaderTables]);

const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    prepareRow,
    state: {pageIndex, pageSize},
    gotoPage,
    canPreviousPage,
    canNextPage,
    nextPage,
    previousPage,
    setPageSize,
} = useTable({columns,dataTable, initialState: {pageIndex: 0}, pageCount: 10}, 
    usePagination
);
const pageCount = Math.ceil(dataTable.length / pageSize);
console.log("HeaderGroups",headerGroups);
console.log("page",page);
return(
        <>
        {<h6 className="text-warning"> {titleTable}</h6>}
        <table className="table table-bordered" {...getTableProps()}>
            <thead>
            {headerGroups.map((headerGroup) =>(
            <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>
                    {column.render("Header")}
                </th>
                ))}
            </tr>
            ))}
            </thead>
            <tbody {...getTableBodyProps()}>
                    {page.map((row)=> {
                        prepareRow(row)
                        return(
                            <tr {...row.getRowProps()}>
                                {row.cells.map((cell) =>(
                                <td {...cell.getCellProps()}>{cell.render("Cell")} </td>
                    ))}
                            </tr>
                        )
                    })}
            </tbody>
        </table>
        <div>
                <button className="btn btn-secondary" onClick={() => previousPage()} disabled={!canPreviousPage}>
                    Previous
                </button>{" "}
                <button className="btn btn-primary" onClick={() => nextPage()} disabled={!canNextPage}>
                    Next
                </button>{" "}
                <span>
                    Page{" "}
                    <strong>
                        {pageIndex + 1} of {pageCount}
                    </strong>{" "}
                </span>
                <span>
                    | Go to page:{" "}
                    <input
                        type="number"
                        defaultValue={pageIndex + 1}
                        onChange={(e) => {
                            const page = e.target.value ? Number(e.target.value) - 1 : 0;
                            gotoPage(page);
                        }}
                        style={{ width: "100px" }}
                    />
                </span>{" "}
                <select
                    value={pageSize}
                    onChange={(e) => {
                        setPageSize(Number(e.target.value));
                    }}
                >
                    {[10, 20, 30, 40, 50].map((pageSize) => (
                        <option key={pageSize} value={pageSize}>
                            Show {pageSize}
                        </option>
                    ))}
                </select>
            </div>
        
        </>
    );
}

const Tabla3 = ({data}) => {
    const { headersTable, subheaderTables, bodyTable } = data;

    const columns = React.useMemo(() => {
        return headersTable.flatMap(header => {
            // Find the two subheaders associated with the current header
            const subheaders = subheaderTables.filter(subheader => subheader.headerId === header.idHeader).slice(0, 2);
            return subheaders.map(subheader => ({
                Header: subheader.headerTitle,
                accessor: (originalRow) => {
                    if(originalRow.rowValues) {
                        const value = originalRow.rowValues.find((val) => val.orderValue === subheader.idHeader);
                        return value ? value.valorMedida + ' ' + value.dateHour : '';
                    } else {
                        return '';
                    }
                },
                id: `${header.headerTitle}-${subheader.headerTitle}`,
            }));
        });
    }, [headersTable, subheaderTables]);

    const dataRows = React.useMemo(
        () =>
            bodyTable.map((row) => {
                const rowData = {};
                row.rowValues.forEach((value) => {
                    rowData[value.orderValue] = {
                        valorMedida: value.valorMedida,
                        dateHour: value.dateHour,
                    };
                });
                return rowData;
            }),
        [bodyTable]
    );

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
        state: { groupBy },
    } = useTable({ columns, dataRows }, useGroupBy);
    
    console.log('headerGroups:', headerGroups);
    console.log('rows:', rows);
    return (
        <div className="table-responsive">
            <table className="table table-bordered" {...getTableProps()}>
                <thead>
                    {headerGroups.map((headerGroup) => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map((column) => (
                                <th {...column.getHeaderProps()} colSpan={column.columns.length} scope="col">
                                    {column.render('Header')}
                                </th>
                            ))}
                        </tr>
                    ))}
                    <tr>
                        {headerGroups.map((headerGroup) =>
                            headerGroup.headers.map((column) =>
                                column.columns.map((subColumn) => (
                                    <th {...subColumn.getHeaderProps()} scope="col">
                                        {subColumn.canGroupBy ? (
                                            // If the column is groupable, show a toggle button
                                            <span {...subColumn.getGroupByToggleProps()}>
                                                {subColumn.isGrouped ? '🔽 ' : '🔼 '}
                                            </span>
                                        ) : null}
                                        {subColumn.render('Header')}
                                    </th>
                                ))
                            )
                        )}
                    </tr>
                </thead>
                <tbody {...getTableBodyProps()}>
                    {rows.map((row, rowIndex) => {
                        prepareRow(row);
                        return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map((cell) => (
                                    <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                ))}
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
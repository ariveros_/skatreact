import React,{useEffect, useState, useMemo} from "react";
import { Line } from "react-chartjs-2";
import {useTable, usePagination} from "react-table"
import axios from "axios";
import url from "../../enviroment"
import LoaderFetch from "../../Page-Components/LoaderFetch";
import SwitchTabla from "./Components/SwitchTabla";

function DemandaTotal({idEstacion,startDate,endDate}){
    const [demandaTotal, setDemandaTotal] = useState()
    const [error, setError] = useState()
    const [errorRequest, setErrorRequest] = useState()
    const [fetching, setFetching] = useState(false)
    const [graphTab, setGraphTab] = useState(false);

    const handleTab = () => {
        setGraphTab(!graphTab)
    }

    const urlGrafico = `/ReporteEstacion/DemandaTotal`;
    const demandaTot = async()=>{
        let from = new Date(startDate.getTime() - startDate.getTimezoneOffset() * 60000).toISOString()
        let to = new Date(endDate.getTime() - endDate.getTimezoneOffset() * 60000).toISOString()

        try{
            setFetching(true)
            await axios.post(url+urlGrafico,
                {idEstacion:idEstacion, startDate: from, endDate: to},
                {withCredentials: true, responseType:'json'})
                .then((response)=>{
                    setDemandaTotal(response.data)
                    })
        }
        catch(error){
            if(error.response){
                if (error.response.status === 404) {
                    setErrorRequest(error.response.data.message)
                }else{
                    setError("Ha ocurrido un error interno")
                    console.log('Error: ', error.message);
                }
            }else{
                setError("Ha ocurrido un error interno")
                console.log('Error: ', error.message);
            }
        }
        finally{
            setFetching(false)
        } 
}
    useEffect(()=>{
        if(!demandaTotal){
            demandaTot()
        }
    },[demandaTotal])

    return(
        <>
            <div className="col-md-12 grid-margin stretch-card">
                <div className="card">
                    <div className="card-body">
                        <h4 className="card-title">Demanda total [MVA]</h4>
                        <hr />
                        <SwitchTabla handleTab={handleTab}/>
                        <br/>
                        {fetching &&
                            <div className="d-flex justify-content-center"> 
                                <LoaderFetch />
                            </div>
                        }
                        {demandaTotal && 
                            <DemandaVisualizador {...{idEstacion,startDate,endDate,graphTab,demandaTotal}}/>
                        }
                        {error &&
                            <p className="text-danger">{error}</p>
                        }
                        {errorRequest &&
                            <p className="text-secondary">{errorRequest}</p>
                        }
                    </div>
                </div>
            </div>
        </>
    );
}
function DemandaVisualizador({idEstacion,startDate,endDate,graphTab,demandaTotal}){
    const [fetching, setFetching] = useState(false)
    const [error, setError] = useState()
    const [errorRequest, setErrorRequest] = useState()
    const [dataTabla, setDataTabla] = useState();
    const urlTabla = `/ReporteEstacion/TablaDemanda`;
    const datosDemandaTot = (demandaTotal) => {
        if(demandaTotal){
            const labelsCargaTR = demandaTotal.labels
            let datasets = []
            demandaTotal.datasets.forEach(dataSet => {
                const nuevoDataset = {
                    label: dataSet.label,
                    data: dataSet.data,
                    backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1,
                    fill: false,
                }
            datasets.push(nuevoDataset)
        });
            let data = {
                labels: labelsCargaTR,
                datasets: datasets
            };
            
            return data
        }
    }

    const dataDemanda = useMemo(()=>{
        return datosDemandaTot(demandaTotal)
    },[demandaTotal])

    const datosTabla = async() => {
        let from = new Date(startDate.getTime() - startDate.getTimezoneOffset() * 60000).toISOString()
        let to = new Date(endDate.getTime() - endDate.getTimezoneOffset() * 60000).toISOString()
        try{
            setFetching(true)
            await axios.post(url+urlTabla,
                {idEstacion:idEstacion, startDate: from, endDate: to},
                {withCredentials: true, responseType:'json'})
                .then((response)=>{
                    setDataTabla(response.data)
                    })
        }
        catch(error){
            if(error.response){
                if (error.response.status === 404) {
                    setErrorRequest(error.response.data.message)

                }else{
                    setError("Ha ocurrido un error interno")
                }
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            }else{
                setError("Ha ocurrido un error interno")
                console.log('Error: ', error.message);
            }
        }
        finally{
            setFetching(false)
        } 
    }

    useEffect(()=>{
        if(graphTab){
            datosTabla() //fetch a la API por la tabla
        }
    },[graphTab])
    return(
        <>
            {fetching &&
                <div className="d-flex justify-content-center"> 
                    <LoaderFetch />
                </div>
            }
            {dataDemanda &&
                <Line data={dataDemanda} options={{elements: {point: {radius: 0,hitRadius:5,hoverRadius:5}}}}  />
            }
            {dataTabla && 
                <Table dataFetchedTable={dataTabla} />
            }
            {error && 
                <p className="text-danger">{error}</p>}
            {errorRequest && 
                <p className="text-secondary">{errorRequest}</p>}
        </>
    )
}
// function Tabla({dataFetchedTable}){

//     return(
//         <>
//         {dataFetchedTable && <h6 className="text-warning"> {dataFetchedTable.titleTable}</h6>}
//         <div className="table-responsive">
//             <table className='table table-bordered'>
//                 <thead>
//                     <tr>
//                         {dataFetchedTable && dataFetchedTable.headersTable.map((header)=>(
//                             <th key={header.idHeader} scope="col">{header.headerTitle}</th>
//                         ))}
//                     </tr>
//                 </thead>
//                 <tbody>
//                     {dataFetchedTable && dataFetchedTable.bodyTable.map((row)=>(
//                         <tr key={row.idHour}>
//                             <td>{(row.dateHour)}</td>
//                             {row.rowValues.map(value =>{
//                                 return <td key={value.orderValue}>{value.valorMedida} </td>
//                             })}
//                         </tr>
//                     ))}
//                 </tbody>
//             </table>
//         </div>
//         </>
//     );
// }

const Table = ({dataFetchedTable}) => {
    const {titleTable, headersTable,bodyTable} = dataFetchedTable;

    const data = useMemo(() => {
            return bodyTable.map((row) => {
                const rowData = {
                    hora : row.dateHour,
                };
            row.rowValues.forEach((value)=>{
                const header = headersTable.find((h) => (h.idHeader) === (value.orderValue +1));
                rowData[header.headerTitle.toLowerCase().replace(/\s/g, "")] = value.valorMedida
            })
            return rowData;
        });
        
    }, [bodyTable, headersTable]);

const columns = useMemo(() => {
        return headersTable.map((header) =>({
            Header: header.headerTitle,
            accessor: header.headerTitle.toLowerCase().replace(/\s/g, ""),
        }));
}, [headersTable]);

const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    prepareRow,
    state: {pageIndex, pageSize},
    gotoPage,
    canPreviousPage,
    canNextPage,
    nextPage,
    previousPage,
    setPageSize,
} = useTable({columns, data, initialState: {pageIndex: 0}, pageCount: 10}, 
    usePagination
);
const pageCount = Math.ceil(data.length / pageSize);

    return(
        <>
        {<h6 className="text-warning"> {titleTable}</h6>}
        <table className="table table-bordered" {...getTableProps()}>
            <thead>
            {headerGroups.map((headerGroup) =>(
            <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>
                    {column.render("Header")}
                </th>
                ))}
            </tr>
            ))}
            </thead>
            <tbody {...getTableBodyProps()}>
                    {page.map((row)=> {
                        prepareRow(row)
                        return(
                            <tr {...row.getRowProps()}>
                                {row.cells.map((cell) =>(
                                <td {...cell.getCellProps()}>{cell.render("Cell")} </td>
                    ))}
                            </tr>
                        )
                    })}
            </tbody>
        </table>
        <div>
                <button className="btn btn-secondary" onClick={() => previousPage()} disabled={!canPreviousPage}>
                    Previous
                </button>{" "}
                <button className="btn btn-primary" onClick={() => nextPage()} disabled={!canNextPage}>
                    Next
                </button>{" "}
                <span>
                    Page{" "}
                    <strong>
                        {pageIndex + 1} of {pageCount}
                    </strong>{" "}
                </span>
                <span>
                    | Go to page:{" "}
                    <input
                        type="number"
                        defaultValue={pageIndex + 1}
                        onChange={(e) => {
                            const page = e.target.value ? Number(e.target.value) - 1 : 0;
                            gotoPage(page);
                        }}
                        style={{ width: "100px" }}
                    />
                </span>{" "}
                <select
                    value={pageSize}
                    onChange={(e) => {
                        setPageSize(Number(e.target.value));
                    }}
                >
                    {[10, 20, 30, 40, 50].map((pageSize) => (
                        <option key={pageSize} value={pageSize}>
                            Show {pageSize}
                        </option>
                    ))}
                </select>
            </div>
        
        </>
    );
}



export default DemandaTotal;
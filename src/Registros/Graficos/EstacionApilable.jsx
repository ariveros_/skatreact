import React,{useEffect, useState,useMemo} from "react";
import DatePicker from "react-datepicker";
import es from 'date-fns/locale/es';
import "react-datepicker/dist/react-datepicker.css";
import setHours from "date-fns/setHours";
import setMinutes from "date-fns/setMinutes";
import url from '../../enviroment';
import axios from "axios";
import { Line } from "react-chartjs-2";
import LoaderFetch from "../../Page-Components/LoaderFetch";

export default function EstacionApilable(){
    const [search, setSearch] =useState(false);
    const [buttonCompare, setButtonCompare] = useState(false)
    const [error, setError] = useState()
    const [errorRequest, setErrorRequest] = useState() 
    const [data, setData] = useState()

    return(
            <div className="col-md-12 grid-margin stretch-card">
                <div className='card'>
                    <div className='card-body'>
                        <h4 className='card-title'>Comparación de demandas</h4>
                        <hr />
                        <div className="row">
                            <div className="col-md-7">
                                <p className="text-muted">Comparar demandas entre estaciones para un período de tiempo determinado.</p>
                            </div>
                            <div className="col-md-4">
                                <button className="btn-sm btn-inverse-info btn-icon-text" onClick={()=>setButtonCompare(!buttonCompare)}>
                                    <i className="mdi mdi-compare btn-icon-prepend"></i> Comparar</button>
                            </div>
                        </div>
                        {buttonCompare &&
                        <>
                        <QueryApilable {...{setSearch,setError,setErrorRequest,data,setData}}/>
                        <hr />
                        {search && <div className="d-flex justify-content-center"> 
                            <LoaderFetch />
                        </div>}
                        {error && <p className="text-danger">{error} </p>}
                        {errorRequest && <p className="text-secondary">{error} </p>}
                        {data && <Grafico data={data} />}
                        </>}
                    </div>
                </div>
            </div>
    )
}

function QueryApilable({setSearch,setError,setErrorRequest,data,setData}){
    const timeStamp = new Date().getTime();
    const yesterdayTimeStamp = timeStamp - 24*60*60*1000;
    const yesterdayDate = new Date(yesterdayTimeStamp);

    const urlGrafico= `/ReporteEstacion/DemandaApilable`
    
    const [errorSelectMedida, setErrorSelectMedida] = useState(false)
    const [errorFecha, setErrorFecha] = useState(false);
    const [errorSelectEstacion, setErrorSelectEstacion] = useState(false)

    const [startDate, setStartDate] = useState(new Date(yesterdayDate.setHours(0,0,0,0)));
    const [endDate, setEndDate] = useState(new Date(yesterdayDate.setHours(23,59,0,0)));
    
    const [selectedMedida, setSelectedMedida] = useState(0)
    const medidasList = [
        {"id":-1,"nombre":"Elegir medida"},
        {"id": 1, "nombre": "Demanda"},
    ]
    const [estaciones, setEstaciones] = useState()

    const [primeraEstacion, setPrimeraEstacion] = useState()
    const[segundaEstacion,setSegundaEstacion] = useState()
    const [checkAgregar, setCheckAgregar] = useState(false)



    const getStation = async() => {
        const elegirEstacion= [{idEstacion: -1,nombreEstacion: "Elegir estación"}]
        const errorEstacion = [{idEstacion: 0,nombreEstacion: "Ha ocurrido un error."}]
        const response = await fetch(url+`/HistEstacionesAPI/ObtenerEstaciones?activo=true`,{
            credentials: 'include'
        })
        if(response.status === 200){
            const dataEstaciones = await response.json();
            setEstaciones(elegirEstacion.concat(dataEstaciones))
            
        }
        else{
            setEstaciones(elegirEstacion.concat(errorEstacion))
        }
    }
    const submitForm = async(e) => {
        if(selectedMedida !== -1 && primeraEstacion !== -1){
            const from = new Date(startDate.getTime() - startDate.getTimezoneOffset() * 60000).toISOString()
            const to = new Date(endDate.getTime() - endDate.getTimezoneOffset() * 60000).toISOString()
            e.preventDefault();
            try{
                setSearch(true)
                await axios.post(url+urlGrafico,
                    {startDate: from,
                    endDate: to,
                    selectedMedida: selectedMedida,
                    idEstacionPrimera: primeraEstacion,
                    idEstacionSegunda:segundaEstacion},
                {withCredentials:true})
                .then((response)=>{
                    setData(response.data)
                })
            }
            catch(error){
                if(error.response.status === 404 ){
                    setErrorRequest(error.response.data.message)
                }
                else{
                    setError("Ha ocurrido un interno en el servidor")
                }
            }
            finally{
                setSearch(false)
            }
        }
    }
    const cleanFilters = (e) => {
        e.preventDefault();
        setStartDate(new Date(yesterdayDate.setHours(0,0,0,0)));
        setEndDate(new Date(yesterdayDate.setHours(23,59,0,0)))
        setSelectedMedida(0)
        setPrimeraEstacion(0)
        setSegundaEstacion(0)
    }

    useEffect(()=>{
        if(!estaciones){
            getStation()
        }
    },[estaciones])

    useEffect(()=>{
        if(selectedMedida == -1){
            setErrorSelectMedida(true)
        }
        else{setErrorSelectMedida(false)}
    },[selectedMedida])
    
    useEffect(()=>{
        if(startDate > endDate){
            setErrorFecha(true)
        }
        else{ setErrorFecha(false)}
    },[startDate, endDate])
    useEffect(()=>{
        if(primeraEstacion == -1){
            setErrorSelectEstacion(true)
        }else{
            setErrorSelectEstacion(false)
        }
    },[primeraEstacion])

    return(
        <>
            <p className="card-description">Determine el período de fechas que desea ver</p>
            <form>
                <div className='row'>
                    <label className="col-sm-1 col-form-label">Inicio</label>
                    <div className="col-sm-3">
                        <DatePicker className="form-control w-100"
                        selected={startDate} onChange={(date) => setStartDate(date)}
                        showTimeSelect
                        locale={es}
                        timeFormat="HH:mm"
                        timeIntervals={5}
                        timeCaption="hora"
                        injectTimes={[
                        setHours(setMinutes(new Date(), 59), 23),
                    ]}
                        dateFormat="dd/MM/yyyy h:mm aa"
                        />
                    </div>
                    <label className="col-sm-1 col-form-label ">Fin</label>
                    <div className="col-sm-3">
                        <DatePicker className="form-control w-100"
                            showTimeSelect
                            timeFormat="HH:mm"
                            locale={es}
                            timeIntervals={5}
                            timeCaption="hora"
                            injectTimes={[
                                setHours(setMinutes(new Date(), 59), 23),
                            ]}
                            dateFormat="dd/MM/yyyy h:mm aa"
                            selected={endDate} onChange={(date) => setEndDate(date)}
                        />
                    </div>
                    <label className="col-md-1 col-sm-5 col-form-label">Medida</label>
                    <div className="col-md-3 col-sm-5">
                    <select className="form-control" name="medidas" value={selectedMedida} onChange={(e)=>setSelectedMedida(e.target.value)}>
                        {medidasList && medidasList.map((medida)=>(
                            <option key={medida.id} value={medida.id}>{medida.nombre}</option>
                        ))}
                    </select>
                    </div>
                    <div className="row justify-content-between">
                        <div className="col-md-9">
                            {errorFecha && <p className='text-danger'>La fecha de inicio no puede ser mayor a la fecha de finalización.</p>}
                        </div>
                        <div className="col-md-3">
                            {errorSelectMedida && <p className='text-danger'>La variable no puede ser nula.</p>}
                        </div>
                    </div>
                    <p className="card-description pt-3">Elegir estación</p>
                    <label className='col-form-label col-md-1 col-sm-2' htmlFor='selectEstacion'>E.T.</label>
                    <div className='col-md-3 col-sm-5'>
                        <select className='form-control' name='estaciones' value={primeraEstacion} onChange={(e)=>setPrimeraEstacion(e.target.value)}>
                        {estaciones && estaciones.map((estacion)=>(
                            estaciones.idEstacion == -1 ?
                            <option key={estacion.idEstacion} disabled value={estacion.idEstacion}>{estacion.nombreEstacion}</option>
                            :
                            <option key={estacion.idEstacion} value={estacion.idEstacion}>{estacion.nombreEstacion}</option>
                        ))}
                        </select>
                    </div>
                    {checkAgregar &&
                    <>
                    <label className='col-form-label col-md-1 col-sm-2' htmlFor='selectEstacion'>E.T. 2</label>
                    <div className='col-md-3 col-sm-5'>
                        <select className='form-control' name='estaciones' value={segundaEstacion} onChange={(e)=>setSegundaEstacion(e.target.value)}>
                        {estaciones && estaciones.map((estacion)=>(
                            estaciones.idEstacion == -1 ?
                            <option key={estacion.idEstacion} disabled value={estacion.idEstacion}>{estacion.nombreEstacion}</option>
                            :
                            <option key={estacion.idEstacion} value={estacion.idEstacion}>{estacion.nombreEstacion}</option>
                        ))}
                        </select>
                    </div>
                    </>
                    }
                    
                    <div className={checkAgregar ? "col-md-4" : "col-md-8"}>
                        <div className="row g-0">
                            <div className={!checkAgregar ? "col-md-8": "col-md-5"}>
                                {!checkAgregar ?
                                <button type="button" className="btn btn-inverse-success btn-icon-text btn-rounded" title='Agregar estacion' onClick={()=>setCheckAgregar(!checkAgregar)}>
                                    <i className="mdi mdi-plus"></i>Agregar estación</button>
                                :
                                <button type="button" className="btn btn-inverse-warning btn-icon-text btn-rounded" title='Deshacer agregar estacion'onClick={()=>setCheckAgregar(!checkAgregar)}>
                                <i className="mdi mdi-close"></i>Deshacer</button>
                                }
                            </div>
                            <div className={!checkAgregar ? "col-md-2" : "col-md-5"}>
                                <button type="button" className="btn btn-primary btn-icon-text btn-rounded " title='Buscar' onClick={(e)=>submitForm(e)}>
                                    <i className="mdi mdi-magnify btn-icon-prepend"></i>Buscar</button>
                            </div>
                            <div className="col-md-2">
                                <button type="button" className="btn btn-outline-danger btn-icon btn-rounded pt-1" title='Limpiar filtros' onClick={(e)=>cleanFilters(e)}>
                                    <i className="mdi mdi-delete"></i></button>
                            </div>
                        </div>
                    </div>
                    {errorSelectEstacion && <p className='text-danger'>Seleccione al menos una estación.</p>}
                </div>
            </form>
        </>
    )
}

function Grafico({data}){
    const [dataGrafico, setDataGrafico] = useState()

    const datosEstaciones = (dataprops) =>{
        const datasets = []
        let labelHours = []
        dataprops.forEach(dataEstacion =>{
            dataEstacion.datasets.forEach(dataSet =>{
                const nuevoDataset = {
                    label: dataSet.label,
                    data: dataSet.data,
                    backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1,
                    fill: false
                }
                datasets.push(nuevoDataset)
            })
            if(labelHours.length === 0){
                labelHours = dataEstacion.labels
                
            }
            
        })
    
        const dataLista = {
            labels: labelHours,
            datasets: datasets
        };
    
        return dataLista
    }
    
    const memoInfoGrafico = useMemo(()=>{
        setDataGrafico(datosEstaciones(data))
    },[data])




    return(
        <>
        <Line data={dataGrafico} options={{elements: {point: {radius: 0,hitRadius:5,hoverRadius:5}}}} />
        </> 
    );
}
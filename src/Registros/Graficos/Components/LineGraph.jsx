import React from "react";
import { Line } from "react-chartjs-2";

export const LineGraph = ({data}) =>{

    return (
        {data} &&
        <Line data={data} options={{
            elements: {point: {radius: 0,hitRadius:5,hoverRadius:5}},
            scales: {
                x: {grid: {color: 'rgba(173, 181, 189, 0.2)'}} ,
                y: {grid: {color: 'rgba(173,181,189,0.2)'}} },
        }} />
    );
}

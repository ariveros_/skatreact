import React from "react";

export default function SwitchTabla({handleTab}){
    return(
        <div className='row'>
                            <div className='col-1'>
                                <label className="switch" style={{paddingLeft: '0.1rem'}}>
                                    <input type="checkbox" className="switch" onClick={handleTab}/>
                                    <div className="slider"></div>
                                </label>
                            </div>
                            <div className='col-3'><span className="text-muted">Mostrar tabla de datos</span></div>
                        </div>
    );
}
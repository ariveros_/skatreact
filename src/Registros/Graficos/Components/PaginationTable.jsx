import React, { useState, useEffect } from "react";
import styles from "./styles.module.css";
const Pagination = ({ pageChangeHandler, totalRows, rowsPerPage}) =>{

    //Calculo del maximo de numero de paginas
    const noOfPages = Math.cell(totalRows / rowsPerPage);

    //Crea un array con el mismo largo que el numero de paginas
    const pagesArr = [...new Array(noOfPages)];

    //Variable state para mantener la pagina actual
    //Este valor es pasado al callback dado por el padre
    const [currentPage, setCurrentPage] = useState(1);

    //Cabeceras para los botones Onclick
    const onNextPage = () => setCurrentPage(currentPage + 1);
    const onPreviousPage = () => setCurrentPage(currentPage - 1);
    const onPageSelect = (pageNo) => setCurrentPage(pageNo);
    // const onFirstPage = () => setCurrentPage(1);
    // const onLastPage = () => setCurrentPage(1);

    const [canGoBack, setCanGoBack] = useState(false);
    const [canGoNext, setCanGoNext] = useState(true);

    useEffect(() => {
        pageChangeHandler(currentPage);
    },[currentPage]);

    useEffect(() =>{
        const skipFactor = (currentPage - 1) * rowsPerPage;
        //Algunas APIs requieren un skip para la paginacion, si se necesita usar esto en vez de
        //pageChangeHandler(skipFactor);
        pageChangeHandler(currentPage);
    },[currentPage])

    useEffect(() =>{
        if(noOfPages === currentPage){
            setCanGoNext(false);
        }else{
            setCanGoNext(true);
        }
        if(currentPage === 1){
            setCanGoBack(false);
        }else{
            setCanGoBack(true);
        }
    },[noOfPages, currentPage])

    return(
        <>
        {noOfPages > 1 ? (
            
                <div className={styles.pagination}>
                    <div className={styles.pagebuttons}>
                        <button
                        className={styles.pageBtn}
                        onClick={onPreviousPage}
                        disabled={!canGoBack}>
                        </button>
                        {pagesArr.map((num, index) => {
                            <button onClick={() => onPageSelect(index + 1)}
                            className={`${styles.pageBtn} ${index + 1 === currentPage ? styles.activeBtn :""}`}>
                                {index + 1}
                            </button>
                        })}
                        <button
                        className={styles.pageBtn}
                        onClick={onNextPage}
                        disabled={!canGoNext}>
                        </button>
                    </div>
                </div>
        ): null}
        </>
    )
}
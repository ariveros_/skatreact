import React, {useEffect, useState} from "react";
import axios from "axios";
import url from "../../enviroment"
import { TablasEstaciones } from "../EstacionesTablas/TablasEstaciones";
import LoaderFetch from "../../Page-Components/LoaderFetch";

export const CosenoPhi = ({idEstacion,startDate,endDate})=>{

    const [data, setData] = useState()
    const [fetching, setFetching] = useState(false)
    const [is5Min, setIs5Min] = useState(false)
    const [error, setError] = useState()
    const [errorRequest, setErrorRequest] = useState()

    const urlFetchTabla = "/ReporteEstacion/TablaCosenoPhi" 
    const isGrouped = false

    const fetchCarga = async()=>{
        const from = new Date(startDate.getTime() - startDate.getTimezoneOffset() * 60000).toISOString()
        const to = new Date(endDate.getTime() - endDate.getTimezoneOffset() * 60000).toISOString()

        try{
            setFetching(true)
            await axios.post(url+urlFetchTabla,
                {idEstacion:idEstacion, startDate: from, endDate: to, is5Min:is5Min},
                {withCredentials: true, responseType:'json'})
                .then((response)=>{
                    setData(response.data)
                    })
        }
        catch(error){
            if(error.response){
                if (error.response.status === 404) {
                    setErrorRequest(error.response.data.message)
                }else{
                    setError("Ha ocurrido un error interno")
                    console.log('Error: ', error.message);
                }
            }else{
                setError("Ha ocurrido un error interno")
                console.log('Error: ', error.message);
            }
        }
        finally{
            setFetching(false)
        } 
    }

    const handleChange = () => {
        setIs5Min(!is5Min);
    }

    useEffect(()=>{
        fetchCarga()     
    },[is5Min])

    return(
        <>
            <div className="col-md-12 grid-margin stretch-card">
                <div className="card">
                    <div className="card-body">
                        <h4 className="card-title">Coseno Phi</h4>
                        <div className="form-check col-md-6 ">
                            <label className="form-check-label">
                                <input 
                                checked={is5Min}
                                type="checkbox" className="form-check-input" value={is5Min} onChange={()=>handleChange()}/> 
                                <i className="input-helper"></i>Mostrar valores cada 5 minutos
                            </label>
                        </div>
                        {data && !error && !fetching &&
                            data.map((tabla)=>(
                                <>
                                <hr />
                                    <TablasEstaciones
                                    key={tabla?.idTable}
                                        dataProps={tabla}
                                        isGrouped={isGrouped}
                                    />
                                </>
                            ))
                        }
                        {fetching &&
                            <div className="d-flex justify-content-center"> 
                                    <LoaderFetch />
                                </div>
                        }
                        {error && 
                        <p className="text-danger">{error} </p>}
                        {errorRequest && 
                        <p className="text-secondary">{errorRequest} </p>}
                    </div>
                </div>
            </div>
        </>
    );
}
import React,{useEffect, useState} from "react";
import axios from "axios";
import url from "../../enviroment"
import LoaderFetch from "../../Page-Components/LoaderFetch";
import SwitchTabla from "./Components/SwitchTabla";
import { GraficoEstaciones } from "../EstacionesGraficos/GraficoEstaciones";

export const CargaATR = ({idEstacion,startDate,endDate})=>{
    const [data, setData] = useState()
    const [fetching, setFetching] = useState(false)
    const [graphTab, setGraphTab] = useState(false);
    const [error, setError] = useState()
    const [errorRequest, setErrorRequest] = useState()
    
    const urlFetchTabla = "/ReporteEstacion/TablaCargaATR"
    const urlFetchGrafico = "/ReporteEstacion/CargaATR"

    const handleTab = () => {
        setGraphTab(!graphTab)
    }

    const fetchCarga = async()=>{
        const from = new Date(startDate.getTime() - startDate.getTimezoneOffset() * 60000).toISOString()
        const to = new Date(endDate.getTime() - endDate.getTimezoneOffset() * 60000).toISOString()

        try{
            setFetching(true)
            await axios.post(url+urlFetchGrafico,
                {idEstacion:idEstacion, startDate: from, endDate: to},
                {withCredentials: true, responseType:'json'})
                .then((response)=>{
                    setData(response.data)
                    })
        }
        catch(error){
            if(error.response){
                if (error.response.status === 404) {
                    setErrorRequest(error.response.data.message)
                }else{
                    setError("Ha ocurrido un error interno")
                    console.log('Error: ', error.message);
                }
            }else{
                setError("Ha ocurrido un error interno")
                console.log('Error: ', error.message);
            }
        }
        finally{
            setFetching(false)
        } 
    }
    
    useEffect(()=>{
        if(!data ){
            fetchCarga()
        }
    },[data])
    
    return(
        <div className="col-md-12 grid-margin stretch-card">
            <div className="card">
                <div className="card-body">
                    <h4 className="card-title">Carga de Autotrafos</h4>
                    <SwitchTabla handleTab={handleTab}/>
                    <br/>
                    {data && !error && 
                        data.map((grafico)=>(
                            <>
                            <hr />
                                <GraficoEstaciones
                                key={grafico?.contGraficos}
                                    dataProps={grafico}
                                    graphTab={graphTab}
                                    idEstacion={idEstacion}
                                    startDate={startDate}
                                    endDate={endDate}
                                    urlFetchTabla = {urlFetchTabla}
                                    isGrouped={true}
                                />
                            </>
                        ))
                    }
                    {fetching &&
                        <div className="d-flex justify-content-center"> 
                                <LoaderFetch />
                            </div>
                    }
                    {error && <p className="text-danger">{error}</p>}
                    {errorRequest && <p className="text-secondary">{errorRequest}</p>}
                </div>
            </div>
        </div>
    );
}
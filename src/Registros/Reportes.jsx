import React,{useEffect, useState} from 'react';
// import {useNavigate} from "react-router-dom";
import DatePicker from "react-datepicker";
import es from 'date-fns/locale/es';
import {Line, Bar} from 'react-chartjs-2';
import {Chart as ChartJS} from 'chart.js/auto';
import "react-datepicker/dist/react-datepicker.css";
import setHours from "date-fns/setHours";
import setMinutes from "date-fns/setMinutes";
import "../Components/css/maps/datepicker.css";
import url from '../enviroment';

function Reportes(){
    const[showReporte, setShowReporte] = useState(false);
    const [errorGral, setErrorGral] = useState(false)

    const [data, setData] = useState()
    const [estaciones, setEstaciones] = useState()
    const [selectedEstacion, setSelectedEstacion] = useState()
    const [errorSelectEstacion, setErrorSelectEstacion] = useState(false)

    const getStation = async() => {
        const response = await fetch(url+`/HistEstacionesAPI/ObtenerEstaciones?activo=true`,{
            credentials: 'include'
        })
        if(response.status === 200){
            let dataEstaciones = await response.json();
            setEstaciones(dataEstaciones);
        }
        else{
            setErrorGral(true)
        }
    }
    const handleSubmitEstacion = () => {
        if(selectedEstacion !== undefined){
            setErrorSelectEstacion(false)
            setShowReporte(true);
        }
        else{
            setErrorSelectEstacion(true)
        }
    }

    const submitSearch = async(e) =>{
        e.preventDefault();
        let idEstacion = selectedEstacion;
        if(selectedmedida !== undefined){
        let from = new Date(startDate.getTime() - startDate.getTimezoneOffset() * 60000).toISOString()
        let to = new Date(endDate.getTime() - endDate.getTimezoneOffset() * 60000).toISOString()
        let idMedida = selectedmedida;
        

        const response = await fetch(url+`/ReporteEstacion/Registros`,{
            method: 'POST',
            headers: {'Content-type': 'application/json'},
            credentials: 'include',
            body: JSON.stringify({
                idEstacion,
                from,
                to,
                idMedida
        })
        })
        if(response.status === 200){
            let actualData = await response.json()
            setData(actualData)
            console.log(actualData)
        }
        else{
            setErrorGral(true)
            return false;
        }
    }
    else{
        setErrorGral(true);
        return false;
    }
    }

    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [selectedmedida, setSelected] = useState();

    const [medidasList, setMedidasList] = useState();
    const consultarMedidas= async() =>{

    const response = await fetch(url+`/HistTipoMedidasAPI/`,{
        credentials: 'include'
    });
    if(response.status === 200){
        let dataMedidas = await response.json();
        setMedidasList(dataMedidas);
    }
    else{
        
    }
}

const generarNuevaConsulta = (e) => {
    e.preventDefault()

    setSelectedEstacion()
    setShowReporte(false)
    setStartDate(new Date())
    setEndDate(new Date())
    setSelected()
    setData() 
}

useEffect(()=>{
    getStation();
    consultarMedidas();
},[]);

useEffect(()=>{
    if(selectedEstacion == -1){
        setErrorSelectEstacion(true)
    }
    else{setErrorSelectEstacion(false)}
},[selectedEstacion])

    return(
        <>
        <div>
            <div className='page-header'>
                <h3 className='page-title'>Reportes</h3>
            </div>
            {!data &&
                <EstacionPicker {...{estaciones,errorSelectEstacion,data,selectedEstacion,setSelectedEstacion,handleSubmitEstacion,showReporte}}/>}
            {showReporte && 
            <ReporteEstacion {...{startDate,setStartDate,endDate,setEndDate,selectedmedida,setSelected,data,medidasList,errorGral,submitSearch,generarNuevaConsulta}}/>}
            {errorGral &&
            <ErrorGral {...{setErrorGral}}/>}
        </div>
        </>
    )
}
export default Reportes;

function EstacionPicker({estaciones, errorSelectEstacion, data, selectedEstacion,setSelectedEstacion, handleSubmitEstacion}){
    return(
        <>
        <div className="col-md-12 grid-margin stretch-card">
            <div className='card'>
                <div className='card-body'>
                    <h4 className='card-title'>Estacion</h4>
                    <hr />
                    <p className="card-description">Buscar la estación que quiere consultar</p>
                        <form>
                        <div className="row">
                            <div className='form-group col-md-6'>
                                <label className='col-form-label' htmlFor='selectEstacion'>Estación</label>
                                <select className='form-control col-md-6' name='estaciones' value={selectedEstacion} onChange={(e)=>setSelectedEstacion(e.target.value)}>
                                {estaciones && estaciones.map((estacion)=>(
                                    estacion.idEstacion === 1 ?
                                    <>
                                    <option value={-1}>Elegir estación</option>
                                    <option key={estacion.idEstacion} value={estacion.idEstacion}>{estacion.nombreEstacion}</option>
                                    </>
                                    :
                                    <option key={estacion.idEstacion} value={estacion.idEstacion}>{estacion.nombreEstacion}</option>
                                ))}
                                </select>
                            </div>
                            {!errorSelectEstacion && !data &&
                                <div className='form-group col-md-6 pt-4'>
                                    <button type="button" className="btn btn-primary btn-icon btn-rounded" onClick={handleSubmitEstacion}><i className="mdi mdi-magnify btn-icon-prepend"></i></button>
                                </div>}
                            {errorSelectEstacion && <p className='text-danger'>Por favor seleccione una estación.</p>}
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </>
    );
}
function ReporteEstacion({startDate,setStartDate,endDate,setEndDate,selectedmedida,setSelected,data,medidasList,errorGral,submitSearch,generarNuevaConsulta}){
    const [errorSelectMedida, setErrorSelectMedida] = useState(false)
    const [errorFecha, setErrorFecha] = useState(false);
    useEffect(()=>{
        if(selectedmedida == -1){
            setErrorSelectMedida(true)
        }
        else{setErrorSelectMedida(false)}
    },[selectedmedida])
    
    useEffect(()=>{
        if(startDate > endDate){
            setErrorFecha(true)
        }
        else{ setErrorFecha(false)}
    },[startDate],[endDate])
    return(
        <>
        <div className='row'>
                <div className="col-md-12 grid-margin stretch-card">
                    <div className='card'>
                        <div className='card-body'>
                            <h4 className='card-title'>Reportes</h4>
                            < hr />
                            {!data ?
                                <form >
                                <div className='row'>
                                    <label className="col-sm-1 col-form-label">Fecha Inicio</label>
                                    <div className="col-sm-3">
                                        <DatePicker className="form-control w-100"
                                        selected={startDate} onChange={(date) => setStartDate(date)}
                                        showTimeSelect
                                        locale={es}
                                        timeFormat="HH:mm"
                                        timeIntervals={15}
                                        timeCaption="hora"
                                        injectTimes={[
                                            setHours(setMinutes(new Date(), 59), 23),
                                        ]}
                                        dateFormat="dd/MM/yyyy h:mm aa"
                                        />
                                    </div>
                                    <label className="col-sm-1 col-form-label ">Fecha Fin</label>
                                    <div className="col-sm-3">
                                        <DatePicker className="form-control w-100"
                                            showTimeSelect
                                            timeFormat="HH:mm"
                                            locale={es}
                                            timeIntervals={15}
                                            timeCaption="hora"
                                            injectTimes={[
                                            setHours(setMinutes(new Date(), 59), 23),
                                        ]}
                                            dateFormat="dd/MM/yyyy h:mm aa"
                                            selected={endDate} onChange={(date) => setEndDate(date)}
                                        />
                                    </div>
                                    <label className="col-sm-1 col-form-label ">Variable</label>
                                    <div className="col-sm-2">
                                        <select className='form-control' name="medidas" value={selectedmedida} onChange={(e)=> setSelected(e.target.value)}>
                                            {
                                                medidasList && medidasList.map((item)=>(
                                                    item.idTipoMedida === 1 ?
                                                    <>
                                                    <option value={-1}>Elegir variable</option>
                                                    <option key={item.idTipoMedida} value={item.idTipoMedida}>{item.medida}</option>
                                                    </>
                                                    :
                                                    <option key={item.idTipoMedida} value={item.idTipoMedida}>{item.medida}</option>
                                                ))
                                            }
                                        </select>
                                    </div>
                                    {!errorFecha && !errorSelectMedida &&
                                        <div className='col-sm-1'>
                                            <button type="button" className="btn btn-primary btn-icon btn-rounded" onClick={(e)=>submitSearch(e)}><i className="mdi mdi-magnify btn-icon-prepend"></i></button>
                                        </div>}
                                    {errorFecha && <p className='text-danger'>La fecha de inicio no puede ser mayor a la fecha de fin.</p>}
                                    {errorSelectMedida && <p className='text-danger'>Por favor seleccione una variable.</p>}
                                </div>
                            </form>
                        :
                            <div className='col-sm-4'>
                                <button type="button" className="btn btn-primary" onClick={(e)=>generarNuevaConsulta(e)}>Generar otra consulta</button>
                            </div>
                        }
                        </div>
                    </div>
                </div>
                {data && !errorGral &&
                <Tabs {...{data}} />}
            </div>
        </>
    );
}
function Tabs({data}){
    const [graphTab, setGraphTab] = useState(true);
    const handleTab = () => {
        setGraphTab(!graphTab);
    }
    return(
        <>
        <div className="col-md-12 grid-margin stretch-card">
            <div className='card'>
                <div className='card-body'>
                    <div className='btn-group'>
                        <button className={graphTab ? 'btn btn-info'  : 'btn btn-outline-secondary' } onClick={handleTab}><i className="mdi mdi-chart-line"></i>Gráfico</button>
                        <button className={!graphTab ? 'btn btn-info'  : 'btn btn-outline-secondary' } onClick={handleTab}><i className="mdi mdi mdi-table"></i>Tabla</button>
                    </div>
                </div>
                {data &&  graphTab &&
                <MedidaGrafico {...{data}} />}
            {data &&  !graphTab &&
                <MedidaTable {...{data}} />}
            </div>
        </div>
        </>
    );
}
function MedidaTable ({data}){
return(
    <>
                <div className='card-body'>
                    <h4 className='card-title'>Tabla</h4>
                    < hr />
                    <div className='table-responsive'>
                        <table className='table table-bordered'>
                            <thead>
                                <tr>
                                    <th>Hora</th>
                                    <th>Valor</th>
                                    {/* <th>Calidad</th> */}
                                </tr>
                            </thead>
                            <tbody>
                                {data && data.valores.map((valor)=>(
                                    <tr key={valor.idValorMedida}>
                                        <td>{new Date(valor.horaMedida).toLocaleString()}</td>
                                        <td>{valor.valorMedida}</td>
                                        {/* <td>{valor.calidadMedida}</td> */}
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
    </>
)}

function MedidaGrafico({data}){
    let labels = [];
    let valoresGrafico = [];
    data.valores.forEach(element => {
        labels.push(new Date(element.horaMedida).toLocaleTimeString());
        valoresGrafico.push(element.valorMedida);
    });

    let min = data.minVal;
    let max = data.maxVal;

    var label = data.descripcionMedida.toString() + " " +"[" + data.unidadMedida.toString() + "]";

    const datos = {
        labels: labels,
        datasets: [{
            label: label,
            data: valoresGrafico,
            backgroundColor: [
                'rgba(255, 206, 86, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                'rgba(255, 206, 86, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
                ],
            borderWidth: 1,
            fill: false
        },
    {
        label: "Mínimo: " + min.toString(),
    },
    {
        label: "Máximo: " + max.toString(),
    }]
    };

    return(
        <>
                        <div className='card-body'>
                            <h4 className='card-title'>Gráfico</h4>
                            <hr />
                                <Line data={datos} />
                        </div>
        </>
    );
}
function ErrorGral ({setErrorGral}){
    useEffect(()=>{
        const timer = setTimeout(()=>{
            setErrorGral(false);
        },10000);
        return() => clearTimeout(timer);
    },[])
    return (
        <>
            <div className='row'>
                <div className="col-md-12 grid-margin stretch-card">
                    <div className='card'>
                        <div className='card-body'>
                            <h4>Ha ocurrido un <mark className='bg-danger text-white'>error</mark>.</h4>
                            <p>No se ha encontrado la información que buscaba.</p>
                            <p>Si está seguro que lo que busca es correcto, por favor intente de nuevo más tarde.</p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}



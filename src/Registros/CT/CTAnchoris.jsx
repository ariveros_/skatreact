import React from "react";
import Provisional from "../Provisional";

function ETCTAnchoris(){
    return(
        <>
        <Provisional nombreEstacion="C.T. Anchoris" categoriaEstacion="Central Térmica" />
        </>
    );
}
export default ETCTAnchoris;
import React from "react";
import Provisional from "../Provisional";

function PIAguaDelToro(){
    return(
        <>
        <Provisional nombreEstacion="P.I. Agua del Toro" categoriaEstacion="Parque Interconexión" />
        </>
    );
}
export default PIAguaDelToro;
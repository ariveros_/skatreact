import React from "react";
import {EtGenericaExterna} from "./ETGenerica";

export default function ETGranMdza(){
    const idEstacion = 8; /*GRAN MENDOZA */
    const nombreEstacion = "E.T. Gran Mendoza";
    return(
        <EtGenericaExterna nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}

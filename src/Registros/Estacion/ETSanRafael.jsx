import React from "react";
import {ETGenericaDistro} from "./ETGenerica";

export default function ETSanRafael(){
    const idEstacion = 30;
    const nombreEstacion = "E.T. San Rafael";
    return(
        <ETGenericaDistro nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}


import React from "react";
import {EtGenericaExterna} from "./ETGenerica";

export default function ETLaBebida(){
    const idEstacion = 21; /*La bebida */
    const nombreEstacion = "E.T. La Bebida";
    return(
        <EtGenericaExterna nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}

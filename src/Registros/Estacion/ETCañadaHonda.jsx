import React from "react";
import {ETGenericaDistro} from "./ETGenerica";

export default function ETCañadaHonda(){

    const idEstacion = 23; /*Cañada Honda */
    const nombreEstacion = "E.T. Cañada Honda";
    return(
        <ETGenericaDistro nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}
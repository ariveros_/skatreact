import React from "react";
import {ETGenericaDistro} from "./ETGenerica";

export default function ETMontecaseros(){
    const idEstacion = 9;
    const nombreEstacion = "E.T. Montecaseros";
    return(
        <ETGenericaDistro nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}


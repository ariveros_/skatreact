import React from "react";
import {ETGenericaDistro} from "./ETGenerica";

export default function ETCdp(){
    const idEstacion = 6; /*CDP */
    const nombreEstacion = "E.T. Cruz de Piedra";
    return(
        <ETGenericaDistro nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}



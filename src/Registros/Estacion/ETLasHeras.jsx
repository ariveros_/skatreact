import React from "react";
import {EtGenericaExterna} from "./ETGenerica";

export default function ETLasHeras(){
    const idEstacion = 5; /*Las Heras */
    const nombreEstacion = "E.T. Las Heras";
    return(
        <EtGenericaExterna nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}


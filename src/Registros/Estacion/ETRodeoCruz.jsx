import React from "react";
import {EtGenericaExterna} from "./ETGenerica";

export default function ETRodeoCruz(){
    const idEstacion = 13;
    const nombreEstacion = "E.T. Rodeo de la Cruz";
    return(
        <EtGenericaExterna nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}


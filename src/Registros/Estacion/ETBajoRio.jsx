import React from "react";
import {ETGenericaDistro} from "./ETGenerica";

export default function ETBajoRio(){
    const idEstacion = 10; /*Bajo Rio Tunuyan */
    const nombreEstacion = "E.T. Bajo Río Tunuyán";
    
    return(
        <ETGenericaDistro nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}
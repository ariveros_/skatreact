import React from "react";
import {EtGenericaExterna} from "./ETGenerica";

export default function ETSarmiento(){
    const idEstacion = 20;
    const nombreEstacion = "E.T. Sarmiento";
    return(
        <EtGenericaExterna nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}


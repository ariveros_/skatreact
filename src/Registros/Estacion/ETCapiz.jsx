import React from "react";
import {ETGenericaDistro} from "./ETGenerica";

export default function ETCapiz(){

    const idEstacion = 27; /*Capiz */
    const nombreEstacion = "E.T. Capiz";
    return(
        <ETGenericaDistro nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}
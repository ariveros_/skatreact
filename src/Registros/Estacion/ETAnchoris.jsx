import React from "react";
import {ETGenericaDistro} from "./ETGenerica";

export default function ETAnchoris(){
    const idEstacion = 3; /* Anchoris */
    const nombreEstacion = "E.T. Anchoris";
    return(
        <ETGenericaDistro nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}





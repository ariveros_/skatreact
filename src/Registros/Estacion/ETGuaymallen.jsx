import React from "react";
import {EtGenericaExterna} from "./ETGenerica";

export default function ETGuaymallen(){

    const idEstacion = 12; /*Guaymallen */
    const nombreEstacion = "E.T. Guaymallén";
    return(
        <EtGenericaExterna nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}
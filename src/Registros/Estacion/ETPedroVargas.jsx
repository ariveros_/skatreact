import React from "react";
import {ETGenericaDistro} from "./ETGenerica";

export default function ETPedroVargas(){
    const idEstacion = 28;
    const nombreEstacion = "E.T. Pedro Vargas";
    return(
        <ETGenericaDistro nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}


import React from "react";
import {ETGenericaDistro} from "./ETGenerica";

export default function ETLdc(){
    const idEstacion = 7;
    const nombreEstacion = "E.T. Luján de Cuyo";
    return(
        <ETGenericaDistro nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}


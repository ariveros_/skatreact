import React,{useEffect, useState} from "react";
import { Link} from 'react-router-dom';

import Query from "../Graficos/QueryReport";
import DemandaTotal from "../Graficos/DemandaTotal";

import {TemperaturaAmbiente} from "../Graficos/TemperaturaAmbiente";
import {CargaTR} from "../Graficos/CargaTrafos";
import { CargaATR } from "../Graficos/CargaATR";
import { PorcentajeCargaTR } from "../Graficos/PorcentajeCargaTR";
import { PorcentajeCargaATR } from "../Graficos/PorcentajeCargaATR";
import { TensionBarras } from "../Graficos/TensionBarras";
import { CargaMinMax } from "../Graficos/CargaMinMax";
import { PotenciaBarras } from "../Graficos/PotenciaBarras";
import { PotenciaTotalBarras } from "../Graficos/PotenciaTotalBarras";
import { CorrienteBarras } from "../Graficos/CorrienteBarras";
import { CorrienteTotalBarras } from "../Graficos/CorrienteTotalBarras";
import { AmperesLineas } from "../Graficos/AmperesLineas";
import { CosenoPhi } from "../Graficos/CosenoPhi";

export const ETGenericaDistro = ({nombreEstacion, idEstacion})=>{
    const [startDate, setStartDate] = useState(new Date('2022-07-05T00:00:00'));
    const [endDate, setEndDate] = useState(new Date('2022-07-05T23:59:00'));
    const [search, setSearch] =useState(false);
    const [porcentajeCargaTrafos,setPorcentajeCargaTrafos] = useState(false);
    const [porcentajeCargATRs,setPorcentajeCargATRs] = useState(false);
    const [tensionBarra,setTensionBarra] = useState(false);

    const [tempAmb, setTempAmb] = useState(false);
    useEffect(()=>{
        if(idEstacion===8 || idEstacion===2){
            setTempAmb(true)
        }
    },[tempAmb,idEstacion])

    return(
        <>
        <div className="page-header">
            <h3 className="page-title">{nombreEstacion}</h3>
            <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item">
                        <Link to={`/#`}>Estaciones Transformadoras</Link>
                    </li>
                    <li className="breadcrumb-item active" aria-current='page'>{nombreEstacion}</li>
                </ol>
            </nav>
        </div>
        
        <Query {...{startDate, setStartDate, endDate, setEndDate,setSearch,porcentajeCargaTrafos,setPorcentajeCargaTrafos,porcentajeCargATRs,setPorcentajeCargATRs,tensionBarra,setTensionBarra}}/>
        {search &&
            <div className="row">
                <DemandaTotal {...{idEstacion,startDate,endDate}}/>
                {tempAmb &&  <TemperaturaAmbiente {...{idEstacion,startDate,endDate}}/>}
                <CargaMinMax {...{idEstacion,startDate,endDate}}/>
                <CargaTR {...{idEstacion,startDate,endDate}}/>
                <CargaATR {...{idEstacion,startDate,endDate}}/>
                <PotenciaBarras {...{idEstacion,startDate,endDate}}/>
                <PotenciaTotalBarras {...{idEstacion,startDate,endDate}}/>
                <CorrienteBarras {...{idEstacion,startDate,endDate}}/>
                <CorrienteTotalBarras {...{idEstacion,startDate,endDate}}/>
                <AmperesLineas {...{idEstacion,startDate,endDate,nombreEstacion}}/>
                <CosenoPhi {...{idEstacion,startDate,endDate}}/>
                {porcentajeCargaTrafos && <PorcentajeCargaTR {...{idEstacion,startDate,endDate}} />}
                {porcentajeCargATRs && <PorcentajeCargaATR {...{idEstacion,startDate,endDate}} />}
                {tensionBarra && <TensionBarras {...{idEstacion,startDate,endDate}} />}
            </div>}
        </>
    );
}
export const EtGenericaExterna = ({nombreEstacion, idEstacion})=>{
    const [startDate, setStartDate] = useState(new Date('2022-07-05T00:00:00'));
    const [endDate, setEndDate] = useState(new Date('2022-07-05T23:59:00'));
    const [search, setSearch] =useState(false);
    const [porcentajeCargaTrafos,setPorcentajeCargaTrafos] = useState(false);
    const [porcentajeCargATRs,setPorcentajeCargATRs] = useState(false);
    const [tensionBarra,setTensionBarra] = useState(false);

    const [tempAmb, setTempAmb] = useState(false);
    useEffect(()=>{
        if(idEstacion===8 || idEstacion===2){
            setTempAmb(true)
        }
    },[tempAmb,idEstacion])

    return(
        <>
        <div className="page-header">
            <h3 className="page-title">{nombreEstacion}</h3>
            <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item">
                        <Link to={`/#`}>Estaciones Transformadoras</Link>
                    </li>
                    <li className="breadcrumb-item active" aria-current='page'>{nombreEstacion}</li>
                </ol>
            </nav>
        </div>
        
        <Query {...{startDate, setStartDate, endDate, setEndDate,setSearch,porcentajeCargaTrafos,setPorcentajeCargaTrafos,porcentajeCargATRs,setPorcentajeCargATRs,tensionBarra,setTensionBarra}}/>
        {search &&
            <div className="row">
            {tempAmb &&  <TemperaturaAmbiente {...{idEstacion,startDate,endDate}}/>}
            <CargaTR {...{idEstacion,startDate,endDate}}/>
            <CargaATR {...{idEstacion,startDate,endDate}}/>
            {porcentajeCargaTrafos && <PorcentajeCargaTR {...{idEstacion,startDate,endDate}} />}
            {porcentajeCargATRs && <PorcentajeCargaATR {...{idEstacion,startDate,endDate}} />}
            {tensionBarra && <TensionBarras {...{idEstacion,startDate,endDate}} />}
        </div>}
        </>
    );
}
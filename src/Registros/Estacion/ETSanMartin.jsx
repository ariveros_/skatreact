import React from "react";
import {EtGenericaExterna} from "./ETGenerica";

export default function ETSanMartin(){
    const idEstacion = 4;
    const nombreEstacion = "E.T. San Martín";
    return(
        <EtGenericaExterna nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}


import React from "react";
import {EtGenericaExterna} from "./ETGenerica";

export default function ETRioDiamante(){
    const idEstacion = 29;
    const nombreEstacion = "E.T. Rio Diamante";
    return(
        <EtGenericaExterna nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}


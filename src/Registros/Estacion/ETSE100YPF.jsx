import React from "react";
import {EtGenericaExterna} from "./ETGenerica";

export default function ETSE100YPF(){
    const idEstacion = 11;
    const nombreEstacion = "E.T. SE100 YPF";
    return(
        <EtGenericaExterna nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}


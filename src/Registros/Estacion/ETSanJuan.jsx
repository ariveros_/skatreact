import React from "react";
import {ETGenericaDistro} from "./ETGenerica";

export default function ETSanJuan(){
    const idEstacion = 2;
    const nombreEstacion = "E.T. San Juan";
    return(
        <ETGenericaDistro nombreEstacion={nombreEstacion} idEstacion={idEstacion} />
    );
}


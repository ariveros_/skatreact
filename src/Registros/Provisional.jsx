import React,{useState} from "react";
import { Link } from "react-router-dom";
import Query from "./Graficos/QueryReport";

function Provisional({nombreEstacion, categoriaEstacion}){
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [search, setSearch] =useState(false);
    
    return (
        <>
            <div className="page-header">
                <h3 className="page-title">{nombreEstacion}</h3>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item">
                            <Link to={`/#`}>{categoriaEstacion}</Link>
                        </li>
                        <li className="breadcrumb-item active" aria-current='page'>{nombreEstacion}</li>
                    </ol>
                </nav>
            </div>
            <Query {...{startDate, setStartDate, endDate, setEndDate,setSearch}}/>
            <div className="row">
                <div className="col-md-12 grid-margin stretch-card">
                    <div className="card">
                        <div className="card-body">
                            <h4 className="card-title">Vista provicional</h4>
                            <hr />
                            <p className="text-muted">Proximamente.</p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
export default Provisional;
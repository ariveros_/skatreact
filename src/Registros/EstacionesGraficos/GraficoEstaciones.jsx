import React,{useMemo, useState,useEffect} from "react";
import { Line } from "react-chartjs-2";
import {processGraphInfo} from "./processGraphInfo";
import {useTable, usePagination} from "react-table"
import axios from "axios";
import url from "../../enviroment";
import '../../Components/css/style.css';
import LoaderFetch from "../../Page-Components/LoaderFetch";
import { LineGraph } from "../Graficos/Components/LineGraph";

export const GraficoEstaciones = ({dataProps,graphTab,idEstacion,startDate,endDate,urlFetchTabla,isGrouped}) =>{
    const [graphicLabel,setGraphicLabel] = useState("")
    const [fetching, setFetching] = useState(false)
    const [error, setError] = useState()
    const [dataFetchedTable, setDataFetchedTable] = useState()

    const data = useMemo(()=>{
        return processGraphInfo(dataProps)
    },[dataProps])

    const fetchTabla = async()=>{
        const from = new Date(startDate.getTime() - startDate.getTimezoneOffset() * 60000).toISOString()
        const to = new Date(endDate.getTime() - endDate.getTimezoneOffset() * 60000).toISOString()

        const arrollamiento = (dataProps.labelGrafico.split(" "))
        const tension = arrollamiento[0]

        setFetching(true)
        await axios.post(url+urlFetchTabla,
        {idEstacion:idEstacion, startDate: from, endDate: to, tension: tension},
        {withCredentials: true, responseType:'json'})
        .then((response)=>{
            setDataFetchedTable(response.data)
            })
        .catch((error)=>{
            if(error.response.status === 404 ){
                setError(error.response.data.message)
            }
            else{
                setError("Ha ocurrido un interno en el servidor")
                console.log(error.response.data)
            }
            })
        .finally(()=>{
            setFetching(false)
        })
    }

    useEffect(()=>{
        setGraphicLabel(dataProps?.labelGrafico)
    },[dataProps?.labelGrafico])

    useEffect(()=>{
        if(graphTab){
            fetchTabla()
        }
    },[graphTab])

    return(
        <>
            
            {graphicLabel && <h6>{graphicLabel}</h6>}
            <br />
            {data  &&
                <LineGraph data={data}/>
            }
            <br />
            {fetching &&
                <div className="d-flex justify-content-center"> 
                    <LoaderFetch />
                </div>
            } 
            {dataFetchedTable && isGrouped &&
                <TableGrouped 
                dataFetchedTable={dataFetchedTable}/>
            }
            {dataFetchedTable && !isGrouped &&
                <TableSimple 
                dataFetchedTable={dataFetchedTable}/>
            }
            {error && 
                <p className="text-danger">{error} </p>}
        </>
    );
}

const TableGrouped = ({dataFetchedTable}) => {
    const {titleTable,headersTable,bodyTable} = dataFetchedTable;
const columns = useMemo(
    () => [
        {
        Header: 'Hora',
        accessor: 'dateHour', // Clave de la propiedad que representa esta columna
        },
      // Mapea los encabezados de la tabla excepto el primero
        ...headersTable.slice(1).map(header => ({
        Header: header.headerTitle,
        // Si tiene subheaders, muestra los subheaders como columnas
        columns: header.subheaders
            ? header.subheaders.map(subheader => ({
                Header: subheader.headerTitle,
                accessor: `${header.idHeader}${subheader.idHeader}`, // Usa una clave única para este subheader
            }))
            : [],
        })),
    ],
    [headersTable]
);
const data = useMemo(
    () =>
      // Mapea los datos del cuerpo de la tabla
        bodyTable.map(row => {
        const rowData = {
            dateHour: row.dateHour,
        };

        row.rowValues.forEach(value => {
                const header = headersTable.find(header => header.idHeader === value.idHeader);
                  if (header && header.idHeader !== 1) { // Si no es el primer header
                        if (header.subheaders) {
                            const subheader = header.subheaders.find(subheader => subheader.idHeader === value.idSubheader)
                            if (subheader) {
                            const columnName = `${header.idHeader}${subheader.idHeader}`;
                            rowData[columnName] = value.valorMedida;
                            }
                        }
                    }
            }
        );
        return rowData;
        }),
    [headersTable, bodyTable]
);

const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    prepareRow,
    state: {pageIndex, pageSize},
    gotoPage,
    canPreviousPage,
    canNextPage,
    nextPage,
    previousPage,
    setPageSize,
} = useTable({columns, data, initialState: {pageIndex: 0}, pageCount: 10}, 
    usePagination
);
const pageCount = Math.ceil(data.length / pageSize);

    return(
        <>
        {<h6 className="text-warning"> {titleTable}</h6>}
        <div className="table-responsive">
            <table className="table table-bordered" {...getTableProps()}>
                <thead>
                {headerGroups.map((headerGroup) =>(
                <tr {...headerGroup.getHeaderGroupProps()}>
                    {headerGroup.headers.map((column) => (
                    <th {...column.getHeaderProps()}>
                        {column.render("Header")}
                    </th>
                    ))}
                </tr>
                ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                        {page.map((row)=> {
                            prepareRow(row)
                            return(
                                <tr {...row.getRowProps()}>
                                    {row.cells.map((cell) =>(
                                    <td {...cell.getCellProps()}>{cell.render("Cell")} </td>
                        ))}
                                </tr>
                            )
                        })}
                </tbody>
            </table>
            <div>
                    <button className="btn btn-secondary" onClick={() => previousPage()} disabled={!canPreviousPage}>
                        Previous
                    </button>{" "}
                    <button className="btn btn-primary" onClick={() => nextPage()} disabled={!canNextPage}>
                        Next
                    </button>{" "}
                    <span>
                        Page{" "}
                        <strong>
                            {pageIndex + 1} of {pageCount}
                        </strong>{" "}
                    </span>
                    <span>
                        | Go to page:{" "}
                        <input
                            type="number"
                            defaultValue={pageIndex + 1}
                            onChange={(e) => {
                                const page = e.target.value ? Number(e.target.value) - 1 : 0;
                                gotoPage(page);
                            }}
                            style={{ width: "100px" }}
                        />
                    </span>{" "}
                    <select
                        value={pageSize}
                        onChange={(e) => {
                            setPageSize(Number(e.target.value));
                        }}
                    >
                        {[10, 20, 30, 40, 50].map((pageSize) => (
                            <option key={pageSize} value={pageSize}>
                                Show {pageSize}
                            </option>
                        ))}
                    </select>
                </div>
        </div>
        </>
    );
}

const TableSimple = ({dataFetchedTable}) => {
    const {titleTable,headersTable,bodyTable} = dataFetchedTable;

    const data = useMemo(() => {
            return bodyTable.map((row) => {
                const rowData = {
                    hora : row.dateHour,
                };
            row.rowValues.forEach((value)=>{
                const header = headersTable.find((h) => (h.idHeader) === value.orderValue + 1);
                rowData[header.headerTitle.toLowerCase().replace(/\s/g, "")] = value.valorMedida
            })
            return rowData;
        });
        
    }, [bodyTable, headersTable]);

const columns = useMemo(() => {
        return headersTable.map((header) =>({
            Header: header.headerTitle,
            accessor: header.headerTitle.toLowerCase().replace(/\s/g, ""),
        }));
}, [headersTable]);

const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    prepareRow,
    state: {pageIndex, pageSize},
    gotoPage,
    canPreviousPage,
    canNextPage,
    nextPage,
    previousPage,
    setPageSize,
} = useTable({columns, data, initialState: {pageIndex: 0}, pageCount: 10}, 
    usePagination
);
const pageCount = Math.ceil(data.length / pageSize);

    return(
        <>
        {<h6 className="text-warning"> {titleTable}</h6>}
        <div className="table-responsive">
            <table className="table table-bordered" {...getTableProps()}>
                <thead>
                {headerGroups.map((headerGroup) =>(
                <tr {...headerGroup.getHeaderGroupProps()}>
                    {headerGroup.headers.map((column) => (
                    <th {...column.getHeaderProps()}>
                        {column.render("Header")}
                    </th>
                    ))}
                </tr>
                ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                        {page.map((row)=> {
                            prepareRow(row)
                            return(
                                <tr {...row.getRowProps()}>
                                    {row.cells.map((cell) =>(
                                    <td {...cell.getCellProps()}>{cell.render("Cell")} </td>
                        ))}
                                </tr>
                            )
                        })}
                </tbody>
            </table>
            <div>
                    <button className="btn btn-secondary" onClick={() => previousPage()} disabled={!canPreviousPage}>
                        Previous
                    </button>{" "}
                    <button className="btn btn-primary" onClick={() => nextPage()} disabled={!canNextPage}>
                        Next
                    </button>{" "}
                    <span>
                        Page{" "}
                        <strong>
                            {pageIndex + 1} of {pageCount}
                        </strong>{" "}
                    </span>
                    <span>
                        | Go to page:{" "}
                        <input
                            type="number"
                            defaultValue={pageIndex + 1}
                            onChange={(e) => {
                                const page = e.target.value ? Number(e.target.value) - 1 : 0;
                                gotoPage(page);
                            }}
                            style={{ width: "100px" }}
                        />
                    </span>{" "}
                    <select
                        value={pageSize}
                        onChange={(e) => {
                            setPageSize(Number(e.target.value));
                        }}
                    >
                        {[10, 20, 30, 40, 50].map((pageSize) => (
                            <option key={pageSize} value={pageSize}>
                                Show {pageSize}
                            </option>
                        ))}
                    </select>
                </div>
        </div>    
        </>
    );
}
import React,{useEffect, useState} from "react";
import axios from "axios";
import url from "../../enviroment";
import {GraficoEstaciones} from "./GraficoEstaciones";
import LoaderFetch from '../../Page-Components/LoaderFetch'
import SwitchTabla from "../Graficos/Components/SwitchTabla";

function GraficoXTensiones({idEstacion,startDate,endDate, urlFetch,nombreGrafico}){
    const [data, setData] = useState()
    const [fetching, setFetching] = useState(false)
    const [graphTab, setGraphTab] = useState(false);
    const [error, setError] = useState(false)

    const handleTab = () => {
        setGraphTab(!graphTab)
    }

    const fetchCarga = async()=>{
        const from = new Date(startDate.getTime() - startDate.getTimezoneOffset() * 60000).toISOString()
        const to = new Date(endDate.getTime() - endDate.getTimezoneOffset() * 60000).toISOString()

        setFetching(true)
        await axios.post(url+urlFetch,
        {idEstacion:idEstacion, startDate: from, endDate: to},
        {withCredentials: true, responseType:'json'})
        .then((response)=>{
            setError(false)
            setData(response.data)
            })
        .catch((error)=>{
            setError(true)
            console.log('Error general', error.message)
        }).finally(()=>{
            setFetching(false)
        })
    }
    useEffect(()=>{
        if(!data ){
            fetchCarga()
        }
    },[data])
    return(
        <div className="col-md-12 grid-margin stretch-card">
            <div className="card">
                <div className="card-body">
                    <h4 className="card-title">{nombreGrafico}</h4>
                    <SwitchTabla handleTab={handleTab}/>
                    <br/>
                    {data && !error && 
                        data.map((grafico)=>(
                            <>
                            <hr />
                                <GraficoEstaciones
                                key={grafico?.contGraficos}
                                    dataProps={grafico}
                                    graphTab={graphTab}
                                    idEstacion={idEstacion}
                                    startDate={startDate}
                                    endDate={endDate}
                                    urlFetchTabla= "/ReporteEstacion/TablaCargaTrafos"
                                />
                            </>
                        ))
                    }
                    {fetching &&
                        <div className="d-flex justify-content-center"> 
                                <LoaderFetch />
                            </div>
                    }
                    {error && 
                    <p className="text-danger">No se ha podido mostrar la información </p>}
                </div>
            </div>
        </div>
    );
}
export default GraficoXTensiones;




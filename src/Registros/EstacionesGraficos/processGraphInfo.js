
export const processGraphInfo = (dataprops) =>{

    const labelsGrafico = dataprops.labels
    const datasets = []
    dataprops.datasets.forEach(dataSet => {
        const nuevoDataset = {
            label: dataSet.label,
            data: dataSet.data,
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1,
            fill: false
        }
        datasets.push(nuevoDataset)
    });
    if(dataprops.minValue){
    const minDataset = {
        label: "Mín: "+ Number(dataprops.minValue).toFixed(2).toString(),
        data: dataprops.minValue,
        backgroundColor: [
            'rgba(73, 80, 87, 0.2)'
        ],
        borderColor: [
        'rgba(0, 210, 64, 1)'
        ],
        borderWidth: 1,
        fill: false
    }
    datasets.push(minDataset)
    }
    if(dataprops.maxValue){
    const maxDataset = {
        label: "Máx: "+Number(dataprops.maxValue).toFixed(2).toString(),
        data: dataprops.maxValue,
        backgroundColor: [
        'rgba(73, 80, 87, 0.2)'
        ],
        borderColor: [
        'rgba(255, 66, 74, 1)'
        ],
        borderWidth: 1,
        fill: false
    }
    datasets.push(maxDataset)
}
    const dataLista = {
        labels: labelsGrafico,
        datasets: datasets
    };
    
    const data = dataLista
    
    return data
}
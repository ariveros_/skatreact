import React from "react";
import { Link } from "react-router-dom";
import { Line } from "react-chartjs-2";

function CHNih1(){
    return(
        <>
        <div className="page-header">
            <h3 className="page-title">Nihuil 1</h3>
            <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item">
                        <Link to={`/#`}>Centrales Hidroeléctricas</Link>
                    </li>
                    <li className="breadcrumb-item active" aria-current='page'>Nihuil-1</li>
                </ol>
            </nav>
        </div>
        <hr />
        <div className="row">
            <CH1Data1/>
            <CH1Data2/>
        </div>
        </>
    );
}
export default CHNih1;

function CH1Data1(){

    const dataPI = {
        labels: ["1:00","2:00","3:00","4:00",
    "5:00","6:00","7:00","7:30","8:00","8:30",
    "9:00","9:30","10:00","10:30","11:00","11:30",
    "12:00","12:30","13:00","13:30","14:00","14:30",
    "15:00","15:30","16:00","16:30","17:00","17:30",
    "18:00","18:30","19:00","19:30","20:00","20:30",
    "21:00","21:30","22:00","23:00","24:00"],
datasets:[
    {
    label:"Barra A",
    data:[138.2,137.5,136.7,136.7,137.1,137.0,137.0,136.9,137.1,136.7,137.0,136.9,136.7,136.6,136.8,134.3,
        134.7,135.0,135.0,135.1,135.1,135.0,135.1,135.1,135.3,135.3,135.1,135.2,135.1,135.3,135.2,135.1,
        136.0,136.7,137.0,136.7,136.8,137.2,137.2],
    backgroundColor: [
    'rgba(255, 99, 132, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 206, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(255, 159, 64, 0.2)'
    ],
    borderColor: [
    'rgba(255,99,132,1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)'
    ],
    borderWidth: 1,
    fill: false
    },{
        label:"Barra B",
        data:[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,],
        backgroundColor: ['rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'],
        borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
        borderWidth: 1,
        fill: false
    }
]
    }

    return(
        <div className="col-md-6 grid-margin strech-card">
            <div className="card">
                <div className="card-body">
                    <h4 className="card-title">Tensiones de Barra 132kv</h4>
                    <Line data={dataPI} options={{elements: {point: {radius: 0,hitRadius:5,hoverRadius:5}}}}></Line>
                </div>
            </div>
        </div>
    )
}

function CH1Data2(){

    const dataPI = {
        labels: ["1:00","2:00","3:00","4:00",
    "5:00","6:00","7:00","7:30","8:00","8:30",
    "9:00","9:30","10:00","10:30","11:00","11:30",
    "12:00","12:30","13:00","13:30","14:00","14:30",
    "15:00","15:30","16:00","16:30","17:00","17:30",
    "18:00","18:30","19:00","19:30","20:00","20:30",
    "21:00","21:30","22:00","23:00","24:00"],
datasets:[
    {
    label:"E.T Malargüe 132kv",
    data:[13.2,12.4,12.0,12.3,12.4,12.2,11.9,12.0,12.6,12.8,12.9,12.9,12.8,13.0,13.2
        ,13.1,13.2,13.2,12.7,12.6,12.6,12.7,12.5,12.8,12.7,12.2,12.6,12.6,13.0,13.1,
        13.1,13.1,12.9,13.1,13.4,13.8,14.0,14.0,13.4],
    backgroundColor: [
    'rgba(255, 99, 132, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 206, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(255, 159, 64, 0.2)'
    ],
    borderColor: [
    'rgba(255,99,132,1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)'
    ],
    borderWidth: 1,
    fill: false
    }
]
    }

    return(
        <div className="col-md-6 grid-margin strech-card">
            <div className="card">
                <div className="card-body">
                    <h4 className="card-title">Demandas en 132kv</h4>
                    <Line data={dataPI} options={{elements: {point: {radius: 0,hitRadius:5,hoverRadius:5}}}}></Line>
                </div>
            </div>
        </div>
    )
}
import React from "react";
import Provisional from "../Provisional";

function CHPiedraNegra(){
    return(
        <>
        <Provisional nombreEstacion="CH Piedra Negra" categoriaEstacion="Central Hidroélectrica" />
        </>
    );
}
export default CHPiedraNegra;
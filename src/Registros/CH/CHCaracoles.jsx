import React from "react";
import Provisional from "../Provisional";

function CHCaracoles(){
    return(
        <>
        <Provisional nombreEstacion="CH Los Caracoles" categoriaEstacion="Central Hidroélectrica" />
        </>
    );
}
export default CHCaracoles;
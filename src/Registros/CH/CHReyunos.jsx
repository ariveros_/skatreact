import React from "react";
import Provisional from "../Provisional";

function CHReyunos(){
    return(
        <>
        <Provisional nombreEstacion="CH Reyunos" categoriaEstacion="Central Hidroélectrica" />
        </>
    );
}
export default CHReyunos;
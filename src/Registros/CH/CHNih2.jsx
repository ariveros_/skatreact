import React from "react";
import { Link } from "react-router-dom";
import { Line } from "react-chartjs-2";

function CHNih2(){
    return(
        <>
        <div className="page-header">
            <h3 className="page-title">Nihuil-2</h3>
            <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item">
                        <Link to={`/#`}>Centrales Hidroeléctricas</Link>
                    </li>
                    <li className="breadcrumb-item active" aria-current='page'>Nihuil 2</li>
                </ol>
            </nav>
        </div>
        <hr />
        <div className="row">
            <CH2Data1/>
            <CH2Data2/>
        </div>
        </>
    );
}
export default CHNih2;

function CH2Data1(){

    const dataPI = {
        labels: ["1:00","2:00","3:00","4:00",
    "5:00","6:00","7:00","7:30","8:00","8:30",
    "9:00","9:30","10:00","10:30","11:00","11:30",
    "12:00","12:30","13:00","13:30","14:00","14:30",
    "15:00","15:30","16:00","16:30","17:00","17:30",
    "18:00","18:30","19:00","19:30","20:00","20:30",
    "21:00","21:30","22:00","23:00","24:00"],
datasets:[
    {
    label:"Barra A",
    data:[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,],
    backgroundColor: [
    'rgba(255, 99, 132, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 206, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(255, 159, 64, 0.2)'
    ],
    borderColor: [
    'rgba(255,99,132,1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)'
    ],
    borderWidth: 1,
    fill: false
    },{
        label:"Barra B",
        data:[137.2,136.8,133.0,135.9,136.3,136.2,133.3,136.3,136.8,136.1,136.2,136.1,136.0,
            135.9,135.8,133.7,134.5,134.2,134.9,134.6,134.9,134.7,134.7,134.7,135.1,134.8,
            134.9,134.8,134.4,135.3,135.2,135.0,134.9,135.8,136.1,136.0,136.3,136.4,136.6],
        backgroundColor: ['rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'],
        borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
        borderWidth: 1,
        fill: false
    }
]
    }

    return(
        <div className="col-md-6 grid-margin strech-card">
            <div className="card">
                <div className="card-body">
                    <h4 className="card-title">Tensiones de Barra 132kv</h4>
                    <Line data={dataPI} options={{elements: {point: {radius: 0,hitRadius:5,hoverRadius:5}}}}></Line>
                </div>
            </div>
        </div>
    )
}

function CH2Data2(){

    const dataPI = {
        labels: ["1:00","2:00","3:00","4:00",
    "5:00","6:00","7:00","7:30","8:00","8:30",
    "9:00","9:30","10:00","10:30","11:00","11:30",
    "12:00","12:30","13:00","13:30","14:00","14:30",
    "15:00","15:30","16:00","16:30","17:00","17:30",
    "18:00","18:30","19:00","19:30","20:00","20:30",
    "21:00","21:30","22:00","23:00","24:00"],
datasets:[
    {
    label:"Autotrafo 220kv",
    data:[222.9,221.8,221.2,220.9,222.0,221.7,222.1,222.4,223.6,222.3,222.4,222.4,222.1,222.0,222.0,218.3,
        219.9,219.2,220.8,220.1,220.2,219.9,219.9,219.9,221.1,220.5,220.7,220.5,219.5,221.2,220.7,220.2,
        220.1,221.1,221.8,221.4,222.1,222.3,222.3],
    backgroundColor: [
    'rgba(255, 99, 132, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 206, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(255, 159, 64, 0.2)'
    ],
    borderColor: [
    'rgba(255,99,132,1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)'
    ],
    borderWidth: 1,
    fill: false
    }
]
    }

    return(
        <div className="col-md-6 grid-margin strech-card">
            <div className="card">
                <div className="card-body">
                    <h4 className="card-title">Tensión de 220kv</h4>
                    <Line data={dataPI} options={{elements: {point: {radius: 0,hitRadius:5,hoverRadius:5}}}}></Line>
                </div>
            </div>
        </div>
    )
}
import React from "react";
import Provisional from "../Provisional";

function CHElTigre(){
    return(
        <>
        <Provisional nombreEstacion="CH El Tigre" categoriaEstacion="Central Hidroélectrica" />
        </>
    );
}
export default CHElTigre;
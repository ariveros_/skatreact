import React from "react";
import Provisional from "../Provisional";

function CHCacheuta(){
    return(
        <>
        <Provisional nombreEstacion="CH Cacheuta" categoriaEstacion="Central Hidroélectrica" />
        </>
    );
}
export default CHCacheuta;
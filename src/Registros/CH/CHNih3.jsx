import React from "react";
import { Link } from "react-router-dom";
import { Line } from "react-chartjs-2";

function CHNih3(){
    return(
        <>
        <div className="page-header">
            <h3 className="page-title">Nihuil-3</h3>
            <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item">
                        <Link to={`/#`}>Centrales Hidroeléctricas</Link>
                    </li>
                    <li className="breadcrumb-item active" aria-current='page'>Nihuil 3</li>
                </ol>
            </nav>
        </div>
        <hr />
        <div className="row">
            <CH3Data1/>
            <CH3Data2/>
        </div>
        </>
    );
}
export default CHNih3;

function CH3Data1(){

    const dataPI = {
        labels: ["1:00","2:00","3:00","4:00",
    "5:00","6:00","7:00","7:30","8:00","8:30",
    "9:00","9:30","10:00","10:30","11:00","11:30",
    "12:00","12:30","13:00","13:30","14:00","14:30",
    "15:00","15:30","16:00","16:30","17:00","17:30",
    "18:00","18:30","19:00","19:30","20:00","20:30",
    "21:00","21:30","22:00","23:00","24:00"],
datasets:[
    {
    label:"Barra A",
    data:[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
        0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,],
    backgroundColor: [
    'rgba(255, 99, 132, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 206, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(255, 159, 64, 0.2)'
    ],
    borderColor: [
    'rgba(255,99,132,1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)'
    ],
    borderWidth: 1,
    fill: false
    },{
        label:"Barra B",
        data:[137.0,136.1,135.5,135.6,136.1,136.1,135.9,135.7,136.1,135.5,135.7,135.7,135.5,
            135.5,135.6,133.2,133.7,133.9,134.0,134.0,134.2,134.0,134.2,134.2,134.3,134.4,
            134.2,134.2,133.8,134.4,134.6,134.3,134.5,135.3,135.7,135.5,135.8,136.0,136.0],
        backgroundColor: ['rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'],
        borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
        borderWidth: 1,
        fill: false
    }
]
    }

    return(
        <div className="col-md-6 grid-margin strech-card">
            <div className="card">
                <div className="card-body">
                    <h4 className="card-title">Tensiones de Barra 132kv</h4>
                    <Line data={dataPI} options={{elements: {point: {radius: 0,hitRadius:5,hoverRadius:5}}}}></Line>
                </div>
            </div>
        </div>
    )
}

function CH3Data2(){

    const dataPI = {
        labels: ["1:00","2:00","3:00","4:00",
    "5:00","6:00","7:00","7:30","8:00","8:30",
    "9:00","9:30","10:00","10:30","11:00","11:30",
    "12:00","12:30","13:00","13:30","14:00","14:30",
    "15:00","15:30","16:00","16:30","17:00","17:30",
    "18:00","18:30","19:00","19:30","20:00","20:30",
    "21:00","21:30","22:00","23:00","24:00"],
datasets:[
    {
    label:"E.T Alvear 132kv",
    data:[17.5,16.1,15.4,15.0,15.1,14.5,13.4,14.8,15.7,16.5,16.5,17.2,17.6,17.8,
        18.0,19.0,19.6,20.6,21.1,21.4,20.9,20.5,20.5,20.3,20.6,21.1,22.3,23.0,
        23.1,22.9,22.4,22.0,21.4,21.2,22.5,23.3,23.5,22.7,21.1],
    backgroundColor: [
    'rgba(255, 99, 132, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 206, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(255, 159, 64, 0.2)'
    ],
    borderColor: [
    'rgba(255,99,132,1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)'
    ],
    borderWidth: 1,
    fill: false
    }
]
    }

    return(
        <div className="col-md-6 grid-margin strech-card">
            <div className="card">
                <div className="card-body">
                    <h4 className="card-title">Demanda E.T Alvear</h4>
                    <Line data={dataPI} options={{elements: {point: {radius: 0,hitRadius:5,hoverRadius:5}}}}></Line>
                </div>
            </div>
        </div>
    )
}
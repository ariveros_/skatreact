import React from "react";
import Provisional from "../Provisional";

function CHQuebrUllum(){
    return(
        <>
        <Provisional nombreEstacion="CH Quebrada de Ullum" categoriaEstacion="Central Hidroélectrica" />
        </>
    );
}
export default CHQuebrUllum;
import React,{useState, useEffect} from "react";
import { useParams, useNavigate, Link } from "react-router-dom";
import '../../Components/css/style.css';
import url from "../../enviroment"

function AdministrarRoles(){
//Acá debería poder cambiarse el rol que tiene el usuario
const {id} = useParams();
//Consultamos el rol
const [data, setData] = useState({});
// const [errorSelectMedida, setErrorSelectMedida] = useState(false)


// useEffect(()=>{
//     console.log('selected: '+selected)

//     if(selected !== undefined){
//         if(selected === -1){
//             setErrorSelectMedida(true)
//         }
//         else{setErrorSelectMedida(false)}
//     }
// },[])

//para navegar
const navigate = useNavigate();
let error = useNavigate();
const consultarRolPersona = async() =>{
    const response = await fetch (url+`/RolsAPI/ConsultarRoles?id=${id}`,{
        credentials: 'include'
    })

    if(response.status === 200){
        let actualData = await response.json()
        setData(actualData)
    }
    else{
        error(`/Error/${response.status}`)
    }

}

//Consultamos los roles posibles
const [roles, setRoles] = useState();
const consultarTodosRoles = async() =>{

    const response = await fetch(url+`/RolsAPI/Roles`,{
        credentials: 'include'
    });
    if(response.status === 200){
        let dataRoles = await response.json();
        setRoles(dataRoles);
    }
    else{
        error(`/Error/${response.status}`)
    }
}

useEffect(()=>{
    consultarRolPersona();
    consultarTodosRoles();
},[]);


//Para elegir valor del select
const [selected, setSelected] = useState({});
const guardarRol = async() =>{
    let idNuevoRol = selected;
    let idUsuario = data.idUserRol;

    if(idNuevoRol !== undefined && idNuevoRol !== -1){
        const response = await fetch(url+`/RolsAPI/ModificarRoles`,{
            method: 'PUT',
            headers: {'Content-type': 'application/json'},
            credentials: 'include',
            body: JSON.stringify({
                idNuevoRol,
                idUsuario
            })
        });
        if(response.status === 200){navigate("/Admin/Roles");}
        else{error(`/Error/${response.status}`)}
    }

}

return(
    <div className="card  col-md-12 grid-margin">
        <div className="card-body">
            <h4 className="card-description">Administrar Roles</h4>
            <hr/>
            <div className="row">
                <div className='form-group col-md-6'>
                        <label htmlFor='selectRol'>Persona</label>
                        <select className='form-control'>
                            <option>{data.email}</option>
                        </select>
                </div>
                <div className='form-group col-md-6'>
                        <label htmlFor='selectRol'>Rol</label>
                        <select className='form-control' name="roles"  onChange={(e)=> setSelected({... e.target.value})}>
                            {
                                roles && roles.map((item)=>(
                                    item.idRol === 1 ?
                                    <>
                                    <option key={-1} value={-1}disabled selected>Elegir rol</option>
                                    <option key={item.idRol} value={item.idRol}>{item.nombreRol}</option> 
                                    </>
                                    :
                                    <option key={item.idRol} value={item.idRol}>{item.nombreRol}</option> 
                                ))
                            }
                        </select>
                        
                        <p className="card-description text-light pt-3">Rol actual: <span className="text-info">{data.nombreRol}</span></p>
                </div>
            </div>
        </div>
        <div>
            <div className="ms-3">
                <Link to='/Admin/Roles'><button className="btn btn-outline-primary btn-fw">Volver</button></Link>
                <button className="btn btn-primary btn-fw m-3" onClick={guardarRol}>Guardar</button>
            </div>
        </div>
    </div>

);
}
export default AdministrarRoles;
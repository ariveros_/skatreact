import React,{useState, useEffect} from "react";
import { Link, useNavigate } from "react-router-dom";
import '../../Components/vendors/mdi/css/materialdesignicons.min.css';
import url from "../../enviroment"

function Roles(){
//Esta es la página que debería listar todos los usuarios con su respectivo rol
const [checkbox, setCheckbox] = useState(false);
const handleChange = () => {
    setCheckbox(!checkbox);
}
//Roles persona
const[data, setData] =useState([]);

let error = useNavigate();

//Fetch activos
async function fetchUserRoles(){
    const response = await fetch (url+`/RolsAPI/ObtenerPersonasRol?activo=`+true,{
        credentials: 'include'
    })
    if(response.status === 200){
        console.log(response.status)
    let actualData = await response.json()
    setData(actualData)
    setBuscados(actualData) //para busqueda
    }
    else{
        error(`/Error/${response.status}`)
    }

} 
//Fetch inactivos/eliminados
async function fetchUserRolesEliminados(){
    const response = await fetch (url+`/RolsAPI/ObtenerPersonasRol?activo=`+false,{
        credentials: 'include'
    })
    if(response.status === 200){
        console.log(response.status)
    let actualData = await response.json()
    setData(actualData)
    setBuscados(actualData) //para busqueda
    }
    else{
        error(`/Error/${response.status}`)
    }
} 
//Para que se ejecute cuando se monte
// useEffect(() =>{ 
//     fetchUserRoles();
//  },[])

//Para interceptar el cambio del checkbox
useEffect(()=>{
    if(checkbox){
    fetchUserRolesEliminados();
}
else{
    fetchUserRoles();
}
},[checkbox])

 //Funcion para completar la tabla
function Row() {
    if(!checkbox){
        return(
            (data.length > 0) ?
                    data.map((item) => (
                        <tr key={item.IdUserRol}>
                            <td>{item.email}</td>
                            <td>{item.nombreRol}</td>
                            <td>
                            <Link to={`/Admin/Roles/AdministrarRoles/${item.IdRol}`}><button className='btn btn-icon-text btn-info' ><i className='mdi mdi-grease-pencil'></i></button></Link>
                            <button className='btn btn-icon-text btn-danger' onClick={()=>eliminarRol(item)}><i className='mdi mdi-delete'></i></button>
                            </td>
                        </tr>
                    )) 
                    : <tr><td>Lista vacía: No se encontraron roles activos</td></tr>
        );
    }
    else{
        return(
            data.length > 0 ?
                    data.map((item) => (
                        <tr key={item.IdUserRol}>
                            <td>{item.email}</td>
                            <td>{item.nombreRol}</td>
                            <td><button className='btn btn-icon-text btn-success' onClick={()=>recuperarRol(item)}><i className='mdi mdi-reply'></i></button>
                            </td>
                        </tr>
                    )) 
                    :  <tr>
                        <td></td>
                        <td></td>
                        <td>Lista vacía: No se encontraron roles eliminados</td>
                        </tr>
        );
    }
}
  //Eliminar rol
async function eliminarRol(rol){
    let IdUserRol = rol.IdUserRol;
    
        const response = await fetch (url+`/RolsAPI/BajaRoles`,{
            method: 'PUT',
            credentials: 'include',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                IdUserRol,
            })
        }).then((response)=> handleChange())
        .catch((error)=> console.log("Errorcito: "+error))
        
    } 
    //Recuperar rol
    async function recuperarRol(rol){
        let IdUserRol = rol.IdUserRol;

        const response = await fetch (url+`/RolsAPI/RecuperarRoles`,{
            method: 'PUT',
            credentials: 'include',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                IdUserRol,
            })
        }).then((response)=> handleChange())
        .catch((error)=> console.log("Errorcito: "+error))
        
    } 

    //busqueda
    const [buscados, setBuscados] = useState();
    const [search, setSearch] = useState();

    const handleSearch = (e) => {
        setSearch(e.target.value)
        filtrarBusqueda(e.target.value)
    }
    const filtrarBusqueda = (terminoBusqueda) => {
        var resultadosBusqueda = buscados.filter((element)=>{
            if(element.nombreRol.toString().toLowerCase().includes(terminoBusqueda.toLowerCase())
            || element.email.toString().toLowerCase().includes(terminoBusqueda.toLowerCase())
            ){
                return element;
            }
        })
        setData(resultadosBusqueda)
    }

return(
    <div className="card">
    <div className="card-body">
        <div className="row">
        <h4 className="card-description col-md-10" >Roles</h4> 
        <Link to={'/Admin/Roles/AdministrarRoles/Crear'} className="col-md-1">
            <button className="btn btn-sm btn-rounded btn-inverse-success" ><i className="mdi mdi-plus"></i></button></Link>
        </div>
        < hr />
        <div className='row'>
                    <div className="form-check col-md-6 ">
                        <label className="form-check-label">
                            <input type="checkbox" className="form-check-input" checked={checkbox} value={checkbox} onChange={handleChange}/> 
                            <i className="input-helper"></i>Mostrar sólo eliminados
                        </label>
                    </div>
                    <div className="form-check col-md-4">
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text bg-primary text-white"><i className='mdi mdi-magnify'></i></span>
                            </div>
                            <input placeholder="buscar" type="text" className="form-control form-control"
                            value={search}
                            onChange={(e)=>handleSearch(e)} />
                        </div>
                    </div>
                </div>
        <div className="table-responsive">
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">Personas</th>
                        <th scope="col">Rol</th>
                        <th scope="col"></th>
                        
                    </tr>
                </thead>
                <tbody>
                <Row />
                </tbody>
            </table>
        </div>
    </div>
    </div>
);
}

export default Roles;


import React,{useState, useEffect} from "react";
import { useNavigate, Link } from "react-router-dom";
import '../../Components/css/style.css';
import url from "../../enviroment"


function CreateRolUser(){


//para navegar
const navigate = useNavigate();
let error = useNavigate();
//Consultamos todos los usuarios
const [data, setData] = useState();
const consultarPersonas = async() =>{
    const response = await fetch (url+`/UsersAPI/`,{
        credentials: 'include'
    })
    
    if(response.status === 200){
        let actualData = await response.json()
        setData(actualData)
    }
    else{
        error(`/Error/${response.status}`)
    }
}

//Consultamos los roles posibles
const [roles, setRoles] = useState();
const consultarTodosRoles = async() =>{

    const response = await fetch(url+`/RolsAPI/Roles`,{
        credentials: 'include'
    });
    
    if(response.status === 200){
        let dataRoles = await response.json();
        setRoles(dataRoles);
    }
    else{
        error(`/Error/${response.status}`)
    }
}

useEffect(()=>{
    consultarPersonas();
    consultarTodosRoles();
},[]);

useEffect(()=>{
    console.log("Data: ",data);
},[data])
useEffect(()=>{
    console.log("Roles: ",roles);
},[roles])

//Para elegir valor del select
const [selectedUser, setSelectedUser] = useState();
const [selectedRol, setSelectedRol] = useState();
const guardarRol = async() =>{
    let idRol = selectedRol;
    let idUsuario = selectedUser;
    
    const response = await fetch(url+`/RolsAPI/AltaRoles`,{
        method: 'POST',
        headers: {'Content-type': 'application/json'},
        credentials: 'include',
        body: JSON.stringify({
            idRol,
            idUsuario
        })
    });
    navigate("/Admin/Roles");

}

return(
    <div className="card  col-md-12 grid-margin">
        <div className="card-body">
            <h4 className="card-description">Administrar Roles</h4>
            <hr/>
            <div className="row">
                <div className='form-group col-md-6'>
                        <label htmlFor='selectRol'>Persona</label>
                        <select className='form-control' name="users" onChange={(e)=> setSelectedUser(e.target.value)}>
                            {
                                data && data.map((item)=>(
                                    <option key={item.idUser} value={item.idUser}>{item.email}</option>
                                ))
                            }
                        </select>
                </div>
                <div className='form-group col-md-6'>
                        <label htmlFor='selectRol'>Rol</label>
                        <select className='form-control' name="roles" onChange={(e)=> setSelectedRol(e.target.value)}>
                            {
                                roles && roles.map((item)=>(
                                    item.idRol === 1 ? <><option value={-1} selected>Elegir un rol</option>
                                    <option key={item.idRol} value={item.idRol}>{item.nombreRol}</option>
                                    </> :
                                    <option key={item.idRol} value={item.idRol}>{item.nombreRol}</option>
                                ))
                                
                            }
                            {selectedRol === -1 && <p>Algooo</p>}
                        </select>
                            
                </div>
            </div>
        </div>
        <div className=' col-md-5 align-items-centre'>
            <Link to='/Admin/Roles'><button className="btn btn-primary">Volver</button></Link>
            <button className="btn btn-success" onClick={guardarRol}>Guardar</button>
        </div>
    </div>

);
}
export default CreateRolUser;
import React, {  useState, useEffect } from 'react';
import {  Navigate, Link, useParams } from 'react-router-dom';
import '../../Components/vendors/mdi/css/materialdesignicons.min.css';
import url from "../../enviroment"

function AdministrarUser(){
const {id} = useParams();

//navegar
const [redirect, setRedirect] = useState(false);
//Usuario
const [data, setData] = useState([]);
//Cambios 
const [email, setEmail] = useState('');
const [password, setPassword] = useState('');

const [shownPass, setShownPass] = useState(false); //Mostrar o no pass
const [changePass, setChangePass] = useState(false); //Checkbox

const togglePass = (e) => {
    e.preventDefault();
    setShownPass(!shownPass);
}
const handleChange = () => {
    setChangePass(!changePass);
}

const consultarUser = async() =>{
    const response = await fetch (url+`/UsersAPI/ConsultarUser?id=${id}`,{
        credentials: 'include'
    })
    let actualData = await response.json();
    setData(actualData)
}

useEffect(()=>{
    consultarUser();
},[]);



const submit = async(e) => {
    e.preventDefault();
    let userID = id;
    await fetch(url+`/UsersAPI/ModificarUser`, {
        method: 'POST',
        headers: {'Content-type': 'application/json'},
        credentials: 'include',
        body: JSON.stringify({
            userID,
            email,
            password
        })
    });
    setRedirect(true);
}
if(redirect){
    return <Navigate to ="/Admin/Usuarios" />
}

const cambiarContraseña = () =>{
if(changePass){
    return(
        <div className='from-group'>
                            <label className="control-label col-md-5 pt-2">Contraseña</label>
                            <div className="input-group">
                                <input className="form-control text-box single-line" 
                                    type={shownPass ? "text" : "password"} name="password" placeholder="Ingrese contraseña"
                                    onChange={(e)=>setPassword(e.target.value)}
                                    />
                                    <div className="input-group-append col-md-2">
                                        <button className="btn btn-sm btn-icon" onClick={(e)=>togglePass(e)}>
                                            {shownPass ? <i className="mdi mdi-eye-off"></i> : <i className="mdi mdi-eye"></i>}
                                        </button> 
                                    </div>
                            </div> 
                            </div>      
    );
}
}

return (
    <div className="card">
            <div className="card-body">
                <h4 className="card-description">Modificar usuario</h4>
                < hr />
                <div className="form-horizontal col-md-6">
    
                        <form>
                            <div className='form-group'>
                            <label className="control-label col-md-5 pt-2">Correo electrónico</label>
                            <div className="col-md-11">
                                <input className="form-control text-box single-line" 
                                typeof="email" name="email" placeholder={data.email}
                                onChange={(e)=>setEmail(e.target.value)}
                                />
                            </div>
                            </div>
                                <div className="form-check col-md-6 ">
                                    <label className="form-check-label">
                                    <input type="checkbox" className="form-check-input" value={changePass} onChange={handleChange}/> 
                                    <i className="input-helper"></i> ¿Cambiar contraseña?
                                    </label>
                                </div>
                                {cambiarContraseña()}
                            <div className='col-md-5 align-items-centre pt-2'>
                                <button className="btn btn-success" onClick={submit}>Guardar</button>
                            </div>
                        </form>
    
                    <div className='col-md-5 align-items-centre pt-5'>
                        <Link to='/Admin/Usuarios'><button className="btn btn-primary">Volver</button></Link>
                    </div>
                </div>
            </div>
        </div>
    
    );

}

export default AdministrarUser;
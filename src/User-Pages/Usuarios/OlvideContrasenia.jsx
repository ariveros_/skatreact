import React,{useState} from "react";
import { useNavigate } from "react-router-dom";
import Logo from '../../Components/images/logoSKATmini.svg'
import url from "../../enviroment"

function OlvideContrasenia(){
    const [email, setEmail] = useState();
    const [token, setToken] = useState('');
    const [codigoEnviado, setCodigoEnviado] = useState(false)

    let recuperarPass = useNavigate();

    //Errores
    const [errorMail, setErrorMail] =useState(false)
    const [errorToken, setErrorToken] =useState(false)
    const [errorServidor, setErrorServidor] = useState(false)
    const [errorBack, setErrorBack] = useState(false)

    const enviarEmail =  async(e) => {
        e.preventDefault()
        try{
            const response = await fetch(url+`/AuthAPI/RecuperarPassword`,{
                method: 'POST',
                headers: {'Content-type': 'application/json'},
                credentials: "include",
                body: JSON.stringify({
                    email
                })
            })
            if(response.status === 200){
                setCodigoEnviado(true);
            }
        }catch(error){
            if(error.response){
                if (error.response.status === 404) {
                    setErrorMail(true)
                }else{
                    setErrorServidor(true)
                }
            }
            else if(error.request){
                setErrorServidor(true)
                console.log('Error en la solicitud:', error.message);
            }else{
                setErrorServidor(true)
                console.log('Error general: ', error.message);
            }
        }
    }

    const comprobarCodigo = async(e) => {
        e.preventDefault()
        try{
            const response = await fetch(url+`/AuthAPI/ValidarToken`,{
                method: 'POST',
                headers: {'Content-type': 'application/json'},
                credentials: 'include',
                body: JSON.stringify({
                        token
                    })
            })
            if(response.status === 200){
                recuperarPass(`/ResetPass/${token}`)
            }
        }catch(error){
            if(error.response){
                if (error.response.status === 404) {
                    setErrorToken(true)
                }else{
                    setErrorBack(true)
                }
            }
            else if(error.request){
                setErrorBack(true)
                console.log('Error en la solicitud:', error.message);
            }else{
                setErrorBack(true)
                console.log('Error general: ', error.message);
            }
        }   
    }

    return(
        <>
        <div className="container-scroller">
                <div className="container-fluid page-body-wrapper full-page-wrapper">
                    <div className="row w-100 m-0">
                        <div className="content-wrapper full-page-wrapper d-flex align-items-center auth register-half-bg">
                                <div className="card col-lg-4 mx-auto  d-flex align-items-center justify-content-between">
                                    <div className="brand-logo logo-login pt-4">
                                        <img src={Logo} alt='Skat logo'></img>
                                    </div>
                                    <div className="card-body px-5 py-3">
                                        <h3 className="card-title text-left mb-3">Recuperar contraseña</h3>
                                        <form onSubmit={enviarEmail}>     
                                            <div className='form-group'>
                                            <label htmlFor='email'>Correo electrónico:</label>
                                            <br />
                                            <input
                                                type="email"
                                                className='form-control p_input'
                                                name='email'
                                                onChange={e => setEmail(e.target.value)}
                                                />
                                            {errorMail && <p className="text-danger">El correo electrónico no existe</p>}
                                            {errorServidor && <p className="text-danger">Ha ocurrido un error. Inténtelo de nuevo más tarde.</p>}
                                            <br />
                                            <button className='btn btn-primary btn-block enter-btn btn-sm'>Enviar código</button>
                                            {/* {codigoEnviado ?<button className='btn btn-inverse-primary btn-block enter-btn btn-sm'>Reenviar código</button> 
                                            : <button className='btn btn-primary btn-block enter-btn btn-sm'>Enviar código</button>} */}
                                            
                                            
                                            </div>
                                        </form>
                                        {codigoEnviado && 
                                        <form>
                                            <label>Ingresar código recibido por correo: </label>
                                            <input 
                                            type="text"
                                            className='form-control p_input'
                                            name="codigo"
                                            onChange={e => setToken(e.target.value)}
                                            />
                                            {errorToken && <p className="text-danger">El código ingresado no es correcto</p>}
                                            {errorBack && <p className="text-danger">Ha ocurrido un error. Inténtelo de nuevo más tarde.</p>}
                                            <br />
                                            <button className="btn btn-inverse-success enter-btn btn-sm" onClick={comprobarCodigo}>Verificar código</button>
                                        </form>}
                                    </div>
                                </div>
                        </div>
                    </div>     
                </div>
        </div>
        </>
    );
}
export default OlvideContrasenia;
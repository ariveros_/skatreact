import React, {  useState } from 'react';
import {  Navigate, Link, useNavigate } from 'react-router-dom';
import '../../Components/vendors/mdi/css/materialdesignicons.min.css';
import url from "../../enviroment"

function RegistrarUsuario(){
//navegar
const [redirect, setRedirect] = useState(false);
let error = useNavigate();
//Usuario
const [email, setEmail] = useState('');
const [password, setPassword] = useState('');
const [shownPass, setShownPass] = useState(false);

const togglePass = (e) => {
    e.preventDefault();
    setShownPass(!shownPass);
}

const submit = async(e) => {
    e.preventDefault();

    const response= await fetch(url+`/UsersAPI/AltaUsuario`, {
        method: 'POST',
        headers: {'Content-type': 'application/json'},
        credentials: 'include',
        body: JSON.stringify({
            email,
            password
        })
    });
    if(response.status === 200){
        setRedirect(true);
    }
    else{
        error(`/Error/${response.status}`)
    }
}
if(redirect){
    return <Navigate to ="/Admin/Usuarios" />
}


return (
<div className="card">
        <div className="card-body">
            <h4 className="card-description">Crear usuario</h4>
            < hr />
            <div className="form-horizontal col-md-6">
                    <form>
                        <div className='form-group'>
                        <label className="control-label col-md-5 pt-2">Correo electrónico</label>
                        <div className="col-md-11">
                            <input className="form-control text-box single-line" 
                            type="email" name="email" placeholder="Ingrese su correo electrónico"
                            onChange={(e)=>setEmail(e.target.value)}
                            />
                        </div>
                        </div>
                        <div className='from-group'>
                        <label className="control-label col-md-5 pt-2">Contraseña</label>
                        <div className="input-group">
                            <input className="form-control text-box single-line" 
                                type={shownPass ? "text" : "password"} name="password" placeholder="Ingrese contraseña"
                                onChange={(e)=>setPassword(e.target.value)}
                                />
                                <div className="input-group-append col-md-2">
                                    <button className="btn btn-sm btn-icon" onClick={(e)=>togglePass(e)}>
                                        {shownPass ? <i className="mdi mdi-eye-off"></i> : <i className="mdi mdi-eye"></i>}
                                    </button> 
                                </div>
                        </div>
                        </div>                            
                        <div className='col-md-5 align-items-centre pt-2'>
                            <button className="btn btn-success" onClick={submit}>Registrar</button>
                        </div>
                    </form>

                <div className='col-md-5 align-items-centre pt-5'>
                    <Link to='/Admin/Usuarios'><button className="btn btn-primary">Volver</button></Link>
                </div>
            </div>
        </div>
    </div>

);
}
export default RegistrarUsuario;
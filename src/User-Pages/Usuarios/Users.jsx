import React,{useState, useEffect} from "react";
import { Link, useNavigate } from "react-router-dom";
import url from "../../enviroment";

function Users(){

    //Para manejar el checkbox de Users Activos
    const [checkbox, setCheckbox] = useState(false);
    
    const handleChange = () => {
        setCheckbox(!checkbox);
    }
    let error = useNavigate();
    
    //Users
    const[data, setData] =useState([]);
    //Fetch activos
    async function fetchUsers(){
        const response = await fetch (url+`/UsersAPI/ObtenerUsuarios?activo=`+true,{
            credentials: 'include'
        })
        if(response.status === 200){
            let actualData = await response.json()
            setData(actualData)
            setBuscados(actualData) //para busqueda
        }
        else{
            error(`/Error/${response.status}`)
        }
    } 
    //Fetch inactivos/eliminados
    async function fetchUsersEliminados(){
        const response = await fetch (url+`/UsersAPI/ObtenerUsuarios?activo=`+false,{
            credentials: 'include'
        })
        if(response.status === 200){
            let actualData = await response.json()
            setData(actualData)
            setBuscados(actualData) //para busqueda
        }
        else{
            error(`/Error/${response.status}`)
        }
    } 
    
    //Para interceptar el cambio del checkbox
    useEffect(()=>{
        if(checkbox){
            fetchUsersEliminados();
        }
        else{
            fetchUsers();
        }
    },[checkbox])
    
    
     //Funcion para completar la tabla
    function Row() {
        if(!checkbox){
            return(
                data.length > 0 ?
                        data.map((item) => (
                            <tr key={item.userID}>
                                <td>{item.email}</td>
                                <td>
                                <Link to={`/Admin/Usuarios/Editar/${item.userID}`}>
                                    <button className='btn btn-icon-text btn-info'><i className='mdi mdi-grease-pencil'></i></button>
                                    </Link>
                                <button className='btn btn-icon-text btn-danger' onClick={()=>eliminarUser(item.userID)}><i className='mdi mdi-delete'></i></button>
                                </td>
                            </tr>
                        )) : <tr><td>Lista vacía: No se encontraron usuarios activos</td></tr>
            );
        }
        else{
            return(
                data.length > 0 ?
                        data.map((item) => (
                            <tr key={item.userID}>
                                <td>{item.email}</td>
                                <td><button className='btn btn-icon-text btn-success' onClick={()=>recuperarUser(item.userID)}><i className='mdi mdi-reply'></i></button>
                                </td>
                            </tr>
                        )) : <tr><td>Lista vacía: No se encontraron usuarios eliminados</td></tr>
            );
        }
    }
    
     //Eliminar rol
        async function eliminarUser(userID){
            const response = await fetch (url+`/UsersAPI/BajaUser`,{
                method: 'PUT',
                credentials: 'include',
                headers: {'Content-type': 'application/json'},
                body: JSON.stringify({userID})
            });
            return response.json();
        } 
     //Recuperar rol
    async function recuperarUser(userID){
        const response = await fetch (url+`/UsersAPI/RecuperarUser`,{
            method: 'PUT',
            credentials: 'include',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({userID})
        }).catch(error => console.log(error));
        return response.json();
    } 

    //busqueda
    const [buscados, setBuscados] = useState();
    const [search, setSearch] = useState();

    const handleSearch = (e) => {
        setSearch(e.target.value)
        filtrarBusqueda(e.target.value)
    }
    const filtrarBusqueda = (terminoBusqueda) => {
        var resultadosBusqueda = buscados.filter((element)=>{
            if(element.email.toString().toLowerCase().includes(terminoBusqueda.toLowerCase())){
                return element;
            }
        })
        setData(resultadosBusqueda)
    }
    return(
    
        
        <div className="card">
            <div className="card-body">
                <div className="row">
                    <h4 className="card-description col-md-10" >Usuarios</h4> 
                    <Link to='/Admin/Usuarios/Registrar' className="col-md-1">
                        <button className="btn btn-sm btn-rounded btn-inverse-success"><i className="mdi mdi-plus"></i></button>
                    </Link>
                </div>
                < hr />
                <div className='row'>
                    <div className="form-check col-md-6 ">
                        <label className="form-check-label">
                            <input type="checkbox" className="form-check-input" value={checkbox} onChange={handleChange}/> 
                            <i className="input-helper"></i>Mostrar sólo eliminados
                        </label>
                    </div>
                    <div className="form-check col-md-4">
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text bg-primary text-white"><i className='mdi mdi-magnify'></i></span>
                            </div>
                            <input placeholder="buscar" type="text" className="form-control form-control"
                            value={search}
                            onChange={(e)=>handleSearch(e)} />
                        </div>
                    </div>
                </div>
                <div className='table-responsive'>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Personas</th>
                            <th scope="col"></th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <Row />
                    </tbody>
                </table>
                </div>
            </div>
    
        </div>
    );
    
}

export default Users; 
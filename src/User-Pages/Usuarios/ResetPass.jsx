import React,{useEffect, useState} from "react";
import {Link, useParams } from "react-router-dom";
import Logo from '../../Components/images/logoSKATmini.svg'
import url from "../../enviroment"

function ResetPass(){
    let {token} = useParams();
    const[passChanged, setPassChanged] =useState(false);
    

    return(
    <>
    <div className="container-scroller">
            <div className="container-fluid page-body-wrapper full-page-wrapper">
                <div className="row w-100 m-0">
                    <div className="content-wrapper full-page-wrapper d-flex align-items-center auth resetpass-bg">
                            <div className="card col-lg-4 mx-auto  d-flex align-items-center justify-content-between">
                                <div className="brand-logo logo-login pt-4">
                                    <img src={Logo} alt='Skat logo'></img>
                                </div>
                                <div className="card-body px-5 py-3 col-xl-12 col-sm-12">
                                    {passChanged ? <PasswordChanged /> : <PasswordReset {...{token,setPassChanged}} />}
                                </div>
                            </div>
                    </div>
                </div>     
            </div>
    </div>
    </>
);
}
export default ResetPass;

function PasswordReset ({token, setPassChanged}){
    const [password, setPassword] = useState();
    const [passwordConfirm, setPasswordConfirm] = useState();
    const [errorPassNoCoincide, setErrorPassNoCoincide] = useState(false);
    const [errorPassUsada, setErrorPassUsada] = useState(false);
    const [errorUsuarioNotFound, setErrorUsuarioNotFound] = useState(false);
    const [errorGeneral, setErrorGeneral] = useState(false);

    useEffect(()=>{
        checkPass()
    },[password,passwordConfirm])

    const submit = async(e) => {
        e.preventDefault()
        try{
            const response = await fetch(url+`/UsersAPI/RecuperarContraseña`,{
                method: 'POST',
                headers: {'Content-type': 'application/json'},
                credentials: 'include',
                body: JSON.stringify({
                        token,
                        password
                    })
            })
            if(response.status === 200){
                setPassChanged(true)
            }
        }catch(error){
            if(error.response){
                if (error.response.status === 400) {
                    setErrorPassUsada(true)
                }
                else if (error.response.status === 404) {
                    setErrorUsuarioNotFound(true)
                }
            }
            else if(error.request){
                setErrorGeneral(true)
                console.log('Error en la solicitud:', error.message);
            }else{
                setErrorGeneral(true)
                console.log('Error general: ', error.message);
            }
        }
        
    }

    const checkPass = () =>{
        setErrorPassNoCoincide(password !== passwordConfirm)
    }
    return(
        <>
            <h3 className="card-title text-left mb-3">Cambiar Contraseña</h3>
            <form onSubmit={submit}>     
                <div className='form-group'>
                    <label htmlFor='password'>Contraseña:</label>
                        <br />
                        <input
                            type="password"
                            className='form-control p_input'
                            name='password'
                            autoComplete="off"
                            placeholder="Nueva contraseña"
                            onChange={e => setPassword(e.target.value)}
                            />
                    <br />
                    <label htmlFor="password">Confirmar contraseña: </label>
                        <br />
                        <input
                            type="password"
                            className='form-control p_input'
                            name='password'
                            placeholder="Repita contraseña"
                            autoComplete="off"
                            onChange={e => setPasswordConfirm(e.target.value)}
                            />
                    {!errorPassNoCoincide && !errorPassUsada && !errorGeneral && !errorUsuarioNotFound &&
                        <br/>}
                    {errorPassNoCoincide && 
                    <p className="text-danger">
                        <span className="p-1"><i className="mdi mdi-close-circle-outline icon-sm text-danger" ></i> </span>
                        Las contraseñas no coinciden</p>}
                    {errorPassUsada && 
                    <p className="text-danger">
                        <span className="p-1"><i className="mdi mdi-close-circle-outline icon-sm text-danger" ></i></span> 
                        La contraseña no puede ser igual a una usada anteriormente. Por favor, intente de nuevo.</p>}
                    {errorUsuarioNotFound && 
                        <p className="text-danger">
                        <span className="p-1"><i className="mdi mdi-close-circle-outline icon-sm text-danger" ></i></span> 
                        El usuario no ha sido encontrado.</p>}
                    {errorGeneral && 
                        <p className="text-danger">
                        <span className="p-1"><i className="mdi mdi-close-circle-outline icon-sm text-danger" ></i></span> 
                        Ha ocurrido un error. Inténtelo de nuevo más tarde.</p>}
                    <div className="d-grid">
                        <button className='btn btn-primary btn-lg w-100' onClick={()=>submit()}>Cambiar contraseña</button>
                        <Link to={'/Home'}><button className="btn btn-outline-primary w-100 btn-lg  mt-2">Volver al Inicio</button></Link>
                    </div>
                    
                </div>
            </form>
        </>
    );
}
function PasswordChanged(){
return(
    <>
    
    <div className="row d-flex justify-content-center">
    <span className="col-sm-1 col-xl-1"><i className="mdi mdi-check-circle-outline icon-md text-success d-flex  align-self-start"></i></span>
    <h3 className="col-sm-11 card-title text-left  mt-2 text-success">¡Contraseña actualizada!</h3>
    </div>
    <blockquote className="blockquote blockquote-success">
    <div className="d-flex justify-content-center">
        <p className="card-text text-muted">La contraseña ha sido actualizada exitosamente.</p>
    </div>
    </blockquote>
    <div className="d-grid">
        <Link to={'/Login'}><button className="btn btn-primary btn-lg w-100 enter-btn ">Iniciar sesión</button></Link>
    </div>
    </>
);
}
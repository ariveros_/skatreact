import React, {  useState, useEffect } from 'react';
import {  Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import '../../Components/vendors/mdi/css/materialdesignicons.min.css';
import url from "../../enviroment"

function CambiarContraseña(){
    const {id} = useParams();

    //navegar
    const [redirect, setRedirect] = useState(false);
    let error = useNavigate();
    //Usuario
    const [data, setData] = useState([]);
    //Cambios 
    const [newPass, setNewPass] = useState('');
    //Errores
    const [mensajeError, setMensajeError] = useState(false);
    //Mostrar contraseña o no
    const [shownPass, setShownPass] = useState(false); //Mostrar o no pass
    
    const togglePass = (e) => {
        e.preventDefault();
        setShownPass(!shownPass);
    }

    
    const consultarUser = async() =>{
        const response = await fetch (url+`/UsersAPI/ConsultarUser?id=${id}`,{
            credentials: 'include'
        })
        let actualData = await response.json();
        setData(actualData)
    }
    
    useEffect(()=>{
        consultarUser();
    },[]);
    
    
    
    const submit = async(e) => {
        e.preventDefault();
        let idUser = id;
        const response = await fetch(url+`/UsersAPI/CambiarContraseña`, {
            method: 'POST',
            headers: {'Content-type': 'application/json'},
            credentials: 'include',
            body: JSON.stringify({
                idUser,
                newPass
            })
        });
        if(response.status === 200){
            setRedirect(true);
        }
        else if(response.status === 400){
            setMensajeError(true);
        }
        else{
            error(`/Error/${response.status}`)
        }
        
    }
    if(redirect){
        return <Navigate to ="/Home" />
    }
    
    
    return (
        <div className="card">
                <div className="card-body">
                    <h4 className="card-description">Cambiar Contraseña</h4>
                    < hr />
                    <div className="form-horizontal col-md-6">
                        <form>
                            <div className='form-group'>
                            <label className="control-label col-md-5 pt-2">Correo electrónico</label>
                            <div className="col-md-11">
                                <input className="form-control text-box single-line" 
                                typeof="email" name="email" placeholder={data.email}
                                disabled={true}
                                />
                            </div>
                            </div>
                            <div className='from-group'>
                                <label className="control-label col-md-5 pt-2">Contraseña nueva</label>
                                <div className="input-group">
                                    <input className="form-control text-box single-line" 
                                    type={shownPass ? "text" : "password"} name="password" placeholder="Ingrese contraseña"
                                    onChange={(e)=>setNewPass(e.target.value)}
                                    />
                                    <div className="input-group-append col-md-2">
                                        <button className="btn btn-sm btn-icon" onClick={(e)=>togglePass(e)}>
                                            {shownPass ? <i className="mdi mdi-eye-off"></i> : <i className="mdi mdi-eye"></i>}
                                        </button> 
                                    </div>
                            </div> 
                            {mensajeError && <p className='text-danger'>Esa contraseña ya ha sido utilizada antes. Por favor, elija una nueva</p>}
                            </div>                                      
                            <div className='col-md-5 align-items-centre pt-2'>
                                <button className="btn btn-success" onClick={submit}>Guardar</button>
                            </div>
                        </form>
        
                        <div className='col-md-5 align-items-centre pt-5'>
                            <Link to='/Home'><button className="btn btn-primary">Volver</button></Link>
                        </div>
                    </div>
                </div>
            </div>
        
        );


}
export default CambiarContraseña;
import React,{useState, useEffect} from "react";
import { useParams, useNavigate, Link } from "react-router-dom";
import '../../Components/css/style.css';
import url from "../../enviroment"

function AdministrarPermisos(){
//Acá debería poder cambiarse el rol que tiene el usuario
const {id} = useParams();

//para navegar
const navigate = useNavigate();


//Consultamos el permiso
const [permiso, setPermiso] = useState({});
const consultarPermiso = async() =>{

    const response = await fetch(url+`/PermisosAPI/ConsultarPermiso?id=${id}`,{
        credentials: 'include'
    });
    let dataPermiso = await response.json();
    setPermiso(dataPermiso);
}

useEffect(()=>{
    consultarPermiso();
},[]);

useEffect(()=>{
    console.log(permiso);
},[permiso]);

const [permisoMenuNombre, setPermisoMN] = useState('');
const guardarPermiso = async() =>{
    let permisoMenuID = id;


    const response = await fetch(url+`/PermisosAPI/ModificarPermiso`,{
        method: 'PUT',
        headers: {'Content-type': 'application/json'},
        credentials: 'include',
        body: JSON.stringify({
            permisoMenuID,
            permisoMenuNombre
        })
    });
    navigate("/Admin/Permisos");

}

return(
    <div className="card  col-md-12 grid-margin">
        <div className="card-body">
            <h4 className="card-description">Administrar Permisos</h4>
            <hr/>
            <div className="row">
                <div className='form-group col-md-6'>
                        <label htmlFor='selectRol'>Permiso</label>
                        <input className="form-control" 
                        type={"text"} placeholder={permiso.permisoMenuNombre}
                        onChange={(e)=> setPermisoMN(e.target.value)}
                        />
                </div>
            </div>
        </div>
        <div className='btn-group col-md-5 align-items-centre'>
            <Link to='/Admin/Permisos'><button className="btn btn-primary">Volver</button></Link>
            <button className="btn btn-success" onClick={guardarPermiso}>Guardar</button>
        </div>
    </div>

);

}
export default AdministrarPermisos;
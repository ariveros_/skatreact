import React, {  useState } from 'react';
import {  Navigate, Link } from 'react-router-dom';
import '../../Components/vendors/mdi/css/materialdesignicons.min.css';
import url from "../../enviroment"

function CrearPermiso(){
    //navegar
    const [redirect, setRedirect] = useState(false);
    //Rol
    const [permisoMenuNombre, setPermisoMN] = useState('');

    const submit = async(e) =>{
        e.preventDefault();

        await fetch(url+`/PermisosAPI/AltaPermiso`, {
            method: 'POST',
            headers: {'Content-type': 'application/json'},
            credentials: 'include',
            body: JSON.stringify({
                permisoMenuNombre
            })
        });
        setRedirect(true);
    }
    if(redirect){
        return <Navigate to ="/Admin/Permisos" />
    }
    

    return(
        <div className="card">
        <div className="card-body">
            <h4 className="card-description">Administrar Permisos</h4>
            < hr />
            <div className="form-horizontal">
                <div className="form-group col-md-6">
                    <form onSubmit={submit}>
                        <label className="control-label col-md-5">Nombre del permiso</label>
                        <div className="col-md-10">
                            <input className="form-control text-box single-line" 
                            typeof="text" name="nombreRol" placeholder="Inserte nombre del nuevo permiso"
                            onChange={(e)=>setPermisoMN(e.target.value)} 
                            />
                        </div>
                        <div className='col-md-5 align-items-centre'>
                            <button className="btn btn-success">Guardar</button>
                        </div>
                    </form>
                </div>
                <div className='col-md-5 align-items-centre pt-5'>
                    <Link to='/Admin/Permisos'><button className="btn btn-primary">Volver</button></Link>
                </div>
            </div>
        </div>
    </div>
);

}
export default CrearPermiso;
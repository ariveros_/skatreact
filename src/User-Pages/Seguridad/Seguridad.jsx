import React, {useEffect, useState} from "react";
import { Link, useNavigate} from "react-router-dom";
import axios from "axios";
//importar estilos pero mepa que va a tener que ser con context
import '../../Components/vendors/mdi/css/materialdesignicons.min.css';
import url from "../../enviroment"

function Seguridad(){
//Esta es la pagina que debería mostrar todos los roles, con un botoncito para editarlos o eliminarlos
//hacer get a todos los roles para mostrarlos como lista

    //Para manejar el checkbox de Roles Eliminados
    const [checkbox, setCheckbox] = useState(false);

    const handleChange = () => {
        setCheckbox(!checkbox);
    }
    
    let error = useNavigate();

    //Rol
    const[data, setData] =useState([]);
    //Fetch activos
    async function fetchRoles(){
        const response = await fetch (url+`/RolsAPI/ObtenerRol?activo=`+true,{
            credentials: 'include'
        })
        if(response.status === 200){
            let actualData = await response.json()
            setData(actualData)
            setBuscados(actualData) //para busqueda
        }
        else{
            error(`/Error/${response.status}`)
        }
    } 
    //Fetch inactivos/eliminados
    async function fetchRolesEliminados(){
        const response = await fetch (url+`/RolsAPI/ObtenerRol?activo=`+false,{
            credentials: 'include'
        })
        if(response.status === 200){
            let actualData = await response.json();
            setData(actualData)
            setBuscados(actualData) //para busqueda
        }
        else{
            error(`/Error/${response.status}`)
        }
    } 

    //Para interceptar el cambio del checkbox
    useEffect(()=>{
        if(checkbox){
            fetchRolesEliminados();
        }
        else{
            fetchRoles();
        }
    },[checkbox])
    

     //Funcion para completar la tabla
    function Row() {
        if(!checkbox){
            return(
                data.length > 0 ?
                        data.map((item) => (
                            <tr key={item.roleID}>
                                <td>{item.nombreRol}</td>
                                <td>{item.roleID}</td>
                                <td>
                                <Link to={`/Admin/Seguridad/Editar/${item.roleID}`}><button className='btn btn-icon-text btn-info'><i className='mdi mdi-grease-pencil'></i></button></Link>
                                <button className='btn btn-icon-text btn-danger' onClick={()=>eliminarRol(item.roleID)}><i className='mdi mdi-delete'></i></button>
                                </td>
                            </tr>
                        )) : <tr><td>Lista vacía: No se encontraron roles activos</td></tr>
            );
        }
        else{
            return(
                data.length > 0 ?
                        data.map((item) => (
                            <tr key={item.roleID}>
                                <td>{item.nombreRol}</td>
                                <td>{item.roleID}</td>
                                <td><button className='btn btn-icon-text btn-success' onClick={()=>recuperarRol(item.roleID)}><i className='mdi mdi-reply'></i></button>
                                </td>
                            </tr>
                        )) : <tr><td>Lista vacía: No se encontraron roles eliminados</td></tr>
            );
        }
    }

     //Eliminar rol
    const eliminarRol = async(roleID) => {
            await axios.put(url+`/RolsAPI/BajaRol`,{roleID: roleID},
            {withCredentials: true, responseType: "json"})
            .then((response)=> handleChange())
            .catch((error)=> console.log("Errorcito: "+error))
        } 
     //Recuperar rol
    const recuperarRol = async(id) => {
        await axios.put(url+`/RolsAPI/RecuperarRol`,{roleID: id},
        {withCredentials: true, responseType: "json"})
        .then((response)=> handleChange())
        .catch((error)=> console.log("Errorcito: "+error))
    }


    //busqueda
    const [buscados, setBuscados] = useState();
    const [search, setSearch] = useState();

    const handleSearch = (e) => {
        setSearch(e.target.value)
        filtrarBusqueda(e.target.value)
    }
    const filtrarBusqueda = (terminoBusqueda) => {
        var resultadosBusqueda = buscados.filter((element)=>{
            if(element.nombreRol.toString().toLowerCase().includes(terminoBusqueda.toLowerCase())){
                return element;
            }
        })
        setData(resultadosBusqueda)
    }

    return(

        
        <div className="card">
            <div className="card-body">
                <div className="row">
                    <h4 className="card-description col-md-10" >Seguridad</h4> 
                    <Link to='/Admin/Seguridad/CrearRol' className="col-md-1">
                        <button className="btn btn-sm btn-rounded btn-inverse-success"><i className="mdi mdi-plus"></i></button>
                    </Link>
                </div>
                < hr />
                <div className='row'>
                    <div className="form-check col-md-6 ">
                        <label className="form-check-label">
                            <input 
                            checked={checkbox}
                            type="checkbox" className="form-check-input" value={checkbox} onChange={()=>handleChange()}/> 
                            <i className="input-helper"></i>Mostrar sólo eliminados
                        </label>
                    </div>
                    <div className="form-check col-md-4">
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text bg-primary text-white"><i className='mdi mdi-magnify'></i></span>
                            </div>
                            <input placeholder="buscar" type="text" className="form-control form-control"
                            value={search}
                            onChange={(e)=>handleSearch(e)} />
                        </div>
                    </div>
                </div>
                <div className='table-responsive'>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Nombre del Rol</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <Row />
                    </tbody>
                </table>
                </div>
            </div>

        </div>
    );
}

export default Seguridad;
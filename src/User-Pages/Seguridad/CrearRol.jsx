import React, {  useState } from 'react';
import {  Navigate, Link, useNavigate } from 'react-router-dom';
import '../../Components/vendors/mdi/css/materialdesignicons.min.css';
import url from "../../enviroment"

function CrearRol(){
    //navegar
    const [redirect, setRedirect] = useState(false);
    let error = useNavigate();
    //Rol
    const [nombreRol, setNombreRol] = useState('');
    const submit = async(e) =>{
        e.preventDefault();

        const response = await fetch(url+`/RolsAPI/AltaRol`, {
            method: 'POST',
            headers: {'Content-type': 'application/json'},
            credentials: 'include',
            body: JSON.stringify({
                nombreRol
            })
        });
        if(response.status === 200){
            setRedirect(true);
        }
        else{
            error(`/Error/${response.status}`)
        }
    }
    if(redirect){
        return <Navigate to ="/Admin/Seguridad" />
    }
    

    return(
        <div className="card">
        <div className="card-body">
            <h4 className="card-description">Administrar Permisos</h4>
            < hr />
            <div className="form-horizontal">
                <div className="form-group col-md-6">
                    <form onSubmit={submit}>
                        <label className="control-label col-md-5">Nombre del rol</label>
                        <div className="col-md-10">
                            <input className="form-control text-box single-line" 
                            typeof="text" name="nombreRol" placeholder="Inserte nombre del rol"
                            onChange={(e)=>setNombreRol(e.target.value)} 
                            />
                        </div>
                        <div className='col-md-5 align-items-centre'>
                            <button className="btn btn-success">Guardar</button>
                        </div>
                    </form>
                </div>
                <div className='col-md-5 align-items-centre pt-5'>
                    <Link to='/Admin/Seguridad'><button className="btn btn-primary">Volver</button></Link>
                </div>
            </div>
        </div>
    </div>
);
}
export default CrearRol;
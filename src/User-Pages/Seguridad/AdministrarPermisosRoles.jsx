import React, { useEffect, useState } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import '../../Components/vendors/mdi/css/materialdesignicons.min.css';
import Modal from 'react-bootstrap/Modal'
import { ModalBody, ModalFooter, ModalHeader } from "react-bootstrap";
import url from "../../enviroment"


function AdministrarPermisosRoles(){
//Esta es la pagina que por rol te lista todos los permisos y te permite agregar uno nuevo
const {id} = useParams();

    //Modal
    const[modalAgregar, setModalAgregar] = useState(false);
    const abrirModalAgregar = () => setModalAgregar(true);
    //para buscar permisos
    const [permisos, setPermisos] = useState('');

    const[errorAct, setErrorAct] = useState(false)
    const[errorMenu, setErrorMenu] = useState(false)
    const [okNombre, setOkNombre] = useState(false)
    const [okMenus, setOkMenus] = useState(false)
    
    const cerrarModalAgregar = () => 
    {
        setModalAgregar(false);
        setPermisosAA([]);
    }
    

    let error = useNavigate();
    const [data, setData] = useState({});

const consultarRol = async() =>{
    const response = await fetch (url+`/RolsAPI/ConsultarRol?rolID=${id}`,{
        credentials: 'include'
    })
    if(response.status === 200){
        let actualData = await response.json()
        setData(actualData)
    }
    else{
        error(`/Error/${response.status}`)
    }
}
useEffect(()=>{
    consultarRol();
},[]);


useEffect(()=>{ 
    console.log("data:",data);
},[data])



const todosPermisos = async () => {
    
    const response = await fetch (url+`/RolsAPI/GetOtrosMenus?idRol=${id}`,{
        credentials: 'include'
    })
    let actualPermisos = await response.json();
    if(response.status === 200){
        setPermisos(actualPermisos);
        abrirModalAgregar()
    }
    
}
const handleServiceAddPermiso = () => {
    todosPermisos(); //get de listas de permisos
}
const [permisosAAgregar, setPermisosAA] = useState([]);

// let permisosAAgregar = [];//como estaba antes
useEffect(()=>
console.log(permisosAAgregar),[permisosAAgregar])

const agregarPermisoRol = (permisoNuevo) =>{
    console.log(Array.isArray(permisosAAgregar))
    setPermisosAA(oldPermisos =>[...oldPermisos, permisoNuevo])
}

const agregarPermisos = async(menus) => {
    let idRol = id;
    const response = await fetch(url+`/RolsAPI/AgregarMenu`,{
        method: 'PUT',
        headers: {'Content-type': 'application/json'},
        credentials: 'include',
        body: JSON.stringify({
            idRol,
            menus
        })
    });
    if(response.status === 200){
        cerrarModalAgregar()
        setErrorMenu(false)
        setOkMenus(true)
    }
    else{
        setOkMenus(false)
        setErrorMenu(true)
    }
}
useEffect(()=>{
    consultarRol();
},[data])

const eliminarPermiso = async(idMenu) => {
    let idRol = id;
    const response = await fetch(url+`/RolsAPI/EliminarMenu`,{
        method: 'PUT',
        headers: {'Content-type': 'application/json'},
        credentials: 'include',
        body: JSON.stringify({
            idRol,
            idMenu
        })
    })
    if(response.status === 200){
        setErrorMenu(false)
        setOkMenus(true)
    }
    else{
        setOkMenus(false)
        setErrorMenu(true)
    }
    
}
const [nuevoNombreRol, setNombreRol] = useState('');
const actualizarRol = async() => {

    const response = await fetch(url+`/RolsAPI/ModificarRol`,{
        method: 'PUT',
        headers: {'Content-type': 'application/json'},
        credentials: 'include',
        body: JSON.stringify({
            nuevoNombreRol,
            data
        })
    })
    if(response.status === 200){
        setErrorAct(false)
        setOkNombre(true)
    }
    else{
        setErrorAct(true)
        setOkNombre(false)
    }
}

return(
    <div className="card">
        <div className="card-body">
            <h4 className="card-description">Administrar Permisos del Rol</h4>
            < hr />
            <div className='row'>
            <div className='form-group col-md-5'>
                <label htmlFor='selectRol'>Cambiar nombre del Rol</label>
                <input className='form-control' placeholder={data.nombreRol}
                onChange={(e)=> setNombreRol(e.target.value)}/>
            </div>
            <div className='col-md-5 pt-xl-4'>
                <button className="btn btn-inverse-primary" onClick={actualizarRol}>Actualizar nombre</button>
            </div>
            {okNombre && <p className='text-light ps-xl-4'><span className='icon-sm text-success'><i className='mdi mdi-check'></i></span> Nombre actualizado exitosamente</p>}
            {errorAct &&  <p className='text-light ps-xl-4'><span className='icon-sm text-danger'><i className='mdi mdi-close'></i></span> Algo salió mal</p>}
            </div>
            <hr />
            <div className='table-responsive pt-xl-2'>
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">Permisos del rol</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        
                    </tr>
                </thead>
                <tbody>
                    {data.menuVMs && data.menuVMs.map((item)=>
                        <tr key={item.idMenu}>
                            <td>{item.nombreMenu}</td>
                            <td>
                                <button className='btn btn-icon-text btn-inverse-danger' onClick={()=>eliminarPermiso(item.idMenu)}><i className='mdi mdi-delete'></i></button>
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
            <div className='ps-xl-3 '>
            <button className="btn btn-inverse-primary btn-icon-text btn-sm" onClick={handleServiceAddPermiso}><i className='btn-icon-prepend mdi mdi-plus'></i>agregar menús</button>
            {okMenus && <p className='pt-xl-2 text-light '><span className='icon-sm text-success'><i className='mdi mdi-check'></i></span> Permisos actualizados exitosamente</p>}
            {errorMenu &&  <p className='text-light pt-xl-2'><span className='icon-sm text-danger'><i className='mdi mdi-close'></i></span> Algo salió mal</p>}
            </div>
            <div className='align-items-centre pt-xl-3'>
                <Link to='/Admin/Seguridad'><button className="btn btn-outline-warning">Volver y descartar cambios</button></Link>
            </div>
            </div>
            
    
    </div>
    {/* ----- MODALS -----  */}
    <Modal show={modalAgregar} onHide={cerrarModalAgregar}>
    <ModalHeader>Agregar nuevo menú al rol</ModalHeader>
    <ModalBody>
    <div className="form-horizontal">
    <div className="form-group">
        <label className="control-label col-md-5">Lista de permisos existentes</label>
        <div className='table-responsive'>
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">Menús</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {permisos && permisos.map((item) =>
                    <tr key={item.idMenu}>
                        <td>{item.nombreMenu}</td>
                        <td><button className="btn btn-sm btn-rounded btn-inverse-success" onClick={()=>agregarPermisoRol(item.idMenu)}>
                            {permisosAAgregar.includes(item.idMenu) ? <i className='btn-icon mdi mdi-check'></i>
                            :  <i className='btn-icon mdi mdi-plus'></i>}
                            </button>
                            </td>
                    </tr>
                    )}
                </tbody>
            </table>
            </div>
    </div>
    </div>
    </ModalBody>
    <ModalFooter>
        <button className="btn btn-danger" onClick={cerrarModalAgregar}>Cancelar cambios</button>
        <button className="btn btn-primary" onClick={()=>agregarPermisos(permisosAAgregar)}>Guardar</button>
    </ModalFooter>
</Modal>

    </div>
);
}
export default AdministrarPermisosRoles;
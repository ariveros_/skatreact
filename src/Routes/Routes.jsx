// import React from 'react';
import React, {Suspense, useEffect, useState} from 'react';
import {BrowserRouter, Outlet, Route, Routes,Navigate} from 'react-router-dom';
//Contexts
import {LoginContext} from '../Contexts/LoginContext'
import url from '../enviroment';
//Layouts
import '../Components/css/style.css';
import FullPageLayout from '../Layouts/FullPageLayout';
import PartialPageLayout from '../Layouts/PartialPageLayout';
//Pages
import Login from '../Pages/Login';
import Spinner from '../Page-Components/Spinner';
import LandingHome from '../Pages/LandingHome';
import Home from '../Pages/Home';
import Contacto from '../Pages/Contacto';
//Errors-pages
import Error from '../Error-Pages/Error';
//User-pages (Configuracion)
import Users from '../User-Pages/Usuarios/Users';
import RegistrarUsuario from '../User-Pages/Usuarios/RegistrarUsuario';
import AdministrarUser from '../User-Pages/Usuarios/AdministrarUser';
import CambiarContraseña from '../User-Pages/Usuarios/CambiarContraseña';
import Seguridad from '../User-Pages/Seguridad/Seguridad';
import AdministrarPermisosRoles from '../User-Pages/Seguridad/AdministrarPermisosRoles'; 
import CrearRol from '../User-Pages/Seguridad/CrearRol';
import Roles from '../User-Pages/Roles/Roles';
import AdministrarRoles from '../User-Pages/Roles/AdministrarRoles';
import CreateRolUser from '../User-Pages/Roles/CreateRolUser';
import Permisos from '../User-Pages/Permisos/Permisos';
import CrearPermiso from '../User-Pages/Permisos/CrearPermiso';
import EditarPermiso from '../User-Pages/Permisos/AdmnistrarPermisos';
//Olvide contraseña
import OlvideContrasenia from '../User-Pages/Usuarios/OlvideContrasenia';
import ResetPass from '../User-Pages/Usuarios/ResetPass';
//reportes
import Reportes from '../Registros/Reportes';
//estaciones
import ETAnchoris from '../Registros/Estacion/ETAnchoris'
import ETBajoRio from '../Registros/Estacion/ETBajoRio';
import ETCañadaHonda from '../Registros/Estacion/ETCañadaHonda';
import ETCapiz from '../Registros/Estacion/ETCapiz';
import ETCdp from '../Registros/Estacion/ETCdp';
import ETGranMdza from '../Registros/Estacion/ETGranMdza'
import ETGuaymallen from '../Registros/Estacion/ETGuaymallen';
import ETLaBebida from '../Registros/Estacion/ETLaBebida';
import ETLasHeras from '../Registros/Estacion/ETLasHeras';
import ETLDC from '../Registros/Estacion/ETLDC';
import ETMontecaseros from '../Registros/Estacion/ETMontecaseros';
import ETPedroVargas from '../Registros/Estacion/ETPedroVargas';
import ETRioDiamante from '../Registros/Estacion/ETRioDiamante';
import ETRodeoCruz from '../Registros/Estacion/ETRodeoCruz';
import ETSE100YPF from '../Registros/Estacion/ETSE100YPF';
import ETSanJuan from '../Registros/Estacion/ETSanJuan';
import ETSanMartin from '../Registros/Estacion/ETSanMartin';
import ETSanRafael from '../Registros/Estacion/ETSanRafael';
import ETSarmiento from '../Registros/Estacion/ETSarmiento';

//CH
import CHNih1 from '../Registros/CH/CHNih1';
import CHNih2 from '../Registros/CH/CHNih2';
import CHNih3 from '../Registros/CH/CHNih3';



//PI
import PIPIP from '../Registros/PI/PIPIP';
const ProtectedRoute = ({user, redirectPath = "/"}) => {
  if(!user) {
    return <Navigate to={redirectPath} replace />;
  }
  return <Outlet />;
};
function App() {
  const [logueado, setLogueado] = useState(false)
  const [userLogueado, setUserLogueado] = useState(null)
  let pathLanding = '/'; 
  //const userExists = localStorage.getItem('userExists');
  // useEffect(()=>{
  //   if(userExists === "true"){
  //     setLogueado(true)
  //   }
  //   else{
  //     setLogueado(false)
  //     localStorage.removeItem('userExists')
  //   }
  // },[])
  // useEffect(()=>{
  //   if(logueado){
  //     authUser()
  //   }
  // },[logueado])
  async function checkAuthStatus() {
    const response = await fetch(url+`/AuthAPI/usuario`,{
        credentials: "include"
    });
    if (response.status === 200) {
      let actualData = await response.json();
      setUserLogueado(actualData);
      setLogueado(true);
      localStorage.setItem('userExists', 'true');
      console.log("seguis logueado")
    }
    else {
      const user = localStorage.getItem('userExists');
      if(user){
        localStorage.removeItem('userExists')
        console.log("no seguis logueado")
        setUserLogueado({})
        setLogueado(false);
        window.location.replace(pathLanding)
        
      }
      else{
        console.log("No inicio sesion por ahora")
      }
      
    }
  }

  useEffect(()=>{
    checkAuthStatus();
  
  const intervalId = setInterval(() => {
    checkAuthStatus();
    // console.log("Se llamo a intervalo")
  }, 60000);
  return() => clearInterval(intervalId);
},[])
    //Funcion para devolver el auth del user
  //   async function authUser(){
  //     const response = await fetch(url+`/AuthAPI/usuario`,{
  //         credentials: "include"
  //     })
  //     if(response.status === 200){
  //         let actualData = await response.json()
  //         setUserLogueado(actualData)
  //     }
  //     else{
  //         console.log(response.statusText)
  //     }
  // }
  return (
    <LoginContext.Provider value={{logueado, setLogueado, userLogueado, setUserLogueado}}>
    <Suspense fallback={<Spinner />}>
    <BrowserRouter>
    <Routes>
      <Route element={<FullPageLayout />}>
        <Route path='/Login' element={<Login/>} />
        <Route path='/Error/:id' element={<Error />} />
        <Route path='/RecuperarContrasenia' element={ <OlvideContrasenia /> } />
        <Route path='/ResetPass/:token' element={ <ResetPass /> } />
        
      </Route>
      <Route path='/' element={<PartialPageLayout />}>
        <Route index element={<LandingHome />} />
        <Route element={<ProtectedRoute user={userLogueado} />}>
          <Route path='Home' element={<Home />} />
          <Route path='/Reportes/ET/Anchoris' element={<ETAnchoris />}/>
          <Route path='/Reportes/ET/BRT' element={<ETBajoRio />}/>
          <Route path='/Reportes/ET/CaniadaHonda' element={<ETCañadaHonda />}/>
          <Route path='/Reportes/ET/Capiz' element={<ETCapiz />}/>
          <Route path='Reportes/ET/CDP' element={<ETCdp />} />
          <Route path='/Reportes/ET/GMdz' element={<ETGranMdza />}/>
          <Route path='/Reportes/ET/Guaymallen' element={<ETGuaymallen />}/>
          <Route path='/Reportes/ET/LaBebida' element={<ETLaBebida />}/>
          <Route path='/Reportes/ET/LasHeras' element={<ETLasHeras />}/>
          <Route path='/Reportes/ET/Lujan' element={<ETLDC />}/>
          <Route path='/Reportes/ET/Montecaseros' element={<ETMontecaseros />}/>
          <Route path='/Reportes/ET/PedroVargas' element={<ETPedroVargas />}/>
          <Route path='/Reportes/ET/RioDiamante' element={<ETRioDiamante />}/>
          <Route path='/Reportes/ET/RodeoCruz' element={<ETRodeoCruz />}/>
          <Route path='/Reportes/ET/SanJuan' element={<ETSanJuan />}/>
          <Route path='/Reportes/ET/SanMartin' element={<ETSanMartin />}/>
          <Route path='/Reportes/ET/SanRafel' element={<ETSanRafael />}/>
          <Route path='/Reportes/ET/Sarmiento' element={<ETSarmiento />}/>
          <Route path='/Reportes/ET/SE100YPF' element={<ETSE100YPF />}/>

          <Route path='/Reportes/CH/Nihuil1' element={<CHNih1/>}/>
          <Route path='/Reportes/CH/Nihuil2' element={<CHNih2/>}/>
          <Route path='/Reportes/CH/Nihuil3' element={<CHNih3/>}/>

          <Route path='Admin/Usuarios' element={<Users />} />
          <Route path='Admin/Usuarios/Registrar'  element={ <RegistrarUsuario />} />
          <Route path='Admin/Usuarios/Editar/:id' element={<AdministrarUser />} />
          <Route path='Usuarios/CambiarContrasenia/:id' element={<CambiarContraseña />} />
          <Route path='Admin/Seguridad' element={<Seguridad />} />
          <Route path='Admin/Seguridad/Editar/:id' element={<AdministrarPermisosRoles />} /> 
          <Route path='Admin/Seguridad/CrearRol' element={<CrearRol />} /> 
          <Route path='Admin/Roles' element={<Roles />} />
          <Route path='Admin/Roles/AdministrarRoles/:id' element={<AdministrarRoles />} />
          <Route path='Admin/Roles/AdministrarRoles/Crear' element={<CreateRolUser />} />
        </Route>
          <Route path='Contacto' element={<Contacto /> }/>
      </Route>
    </Routes>
    </BrowserRouter>
    </Suspense>
    </LoginContext.Provider>
  );
}

export default App;

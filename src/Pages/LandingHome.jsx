import React,{useEffect, useState} from 'react';
import '../Components/css/style.css';
import {Line, Bar} from 'react-chartjs-2';
import {Chart as ChartJS} from 'chart.js/auto'
import arMill from '@react-jvectormap/argentina/dist/arMill';
import { VectorMap } from '@react-jvectormap/core';
import DatePicker from "react-datepicker";
import es from 'date-fns/locale/es';
import "react-datepicker/dist/react-datepicker.css";
import setHours from "date-fns/setHours";
import setMinutes from "date-fns/setMinutes";
import { TemperaturaMdzaBar } from '../Pages/HomeComponents/TemperaturaGraph';
import {Tiempo} from './LandingComponents/TiempoTable';

//prueba para grafico de san juan
import { SanJuanDemandaGeneracion } from './LandingComponents/SanJuanLandingGraph';
import { MendozaDemandaGeneracion } from './LandingComponents/MendozaLandingGraph';

function LandingHome(){
    const timeStamp = new Date().getTime();
    const yesterdayTimeStamp = timeStamp - 24*60*60*1000;
    const yesterdayDate = new Date(yesterdayTimeStamp);
    const [startDate, setStartDate] = useState(new Date(yesterdayDate.setHours(0,0,0,0)));
    const [endDate, setEndDate] = useState(new Date(yesterdayDate.setHours(23,59,0,0)));

    return(
    <div>
        <div className='page-header'>
            <h3 className='page-title'>Inicio</h3>
        </div>
        <div className='row'>
            <div className="col-md-12 grid-margin stretch-card">
                <Query {...{startDate,setStartDate,endDate,setEndDate}}/>
            </div>
        </div>

        <div className='row'>
            <div className='col-12'>
                <div className='row'>
                    <div className="col-md-6 grid-margin stretch-card">
                        <MendozaDemandaGeneracion />
                    </div>
                    
                    <div className="col-md-6 grid-margin stretch-card">
                        <SanJuanDemandaGeneracion />
                    </div>
                </div>
            </div>
        </div>

        

        <div className='row'>
            <div className='col-12'>
                <div className='row'>
                    <TablaDemandas />
                    <TablaTemp />
                </div>
            </div>
        </div>
        <div className='row'>
            <div className='col-12'>
                <div className='row'>
                <div className="col-md-6 grid-margin stretch-card">
                        <TemperaturaMdzaBar />
                    </div>
                    <div className="col-md-6 grid-margin stretch-card">
                        <div className='card'>
                            <div className='card-body'>
                                <h4 className='card-title'>Máximas demandas históricas</h4>
                                <Tabla />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className='row'>
            <div className='col-12'>
                <div className='row'>
                <div className="col-md-4 grid-margin stretch-card">
                    <Map/>
                </div>
                <div className="col-md-8 grid-margin stretch-card">
                
                </div>
                    
                    {/* <div className='col-8 grid-margin stretch-card'>
                        <Tiempo />
                    </div>  */}
                </div>
            </div>
        </div>
    </div>

    )
}
export default LandingHome;

function Query({startDate,setStartDate,endDate,setEndDate}){
    const [errorFecha, setErrorFecha] = useState(false);
    const timeStamp = new Date().getTime();
    const yesterdayTimeStamp = timeStamp - 24*60*60*1000;
    const yesterdayDate = new Date(yesterdayTimeStamp);


    // let yesterdayStart = new Date()
    //     yesterdayStart.setDate(yesterdayStart.getDate()-yesterdayStart.getDay())
    //     yesterdayStart.setHours(0,0,0,0)

    // let yesterdayEnd = new Date()
    //     yesterdayEnd.setDate(yesterdayEnd.getDate()-yesterdayEnd.getDay())
    //     yesterdayEnd.setHours(23,59,0,0)

    // let todaysDate = new Date(Date.now())
    // let arreglado1 = new Date(todaysDate.getTime() - todaysDate.getTimezoneOffset() * 60000).toISOString()
    // let arreglado2 = new Date(yesterdayStart.getTime() - yesterdayStart.getTimezoneOffset() * 60000).toISOString()
    // let arreglado3 = new Date(yesterdayEnd.getTime() - yesterdayEnd.getTimezoneOffset() * 60000).toISOString()

    const limpiarFiltros = (e) => {
        e.preventDefault()
        setStartDate(new Date(yesterdayDate.setHours(0,0,0,0)))
        setEndDate(new Date(yesterdayDate.setHours(23,59,0,0)))
    }
    // const generarNuevaConsulta = (e) => {
    //     e.preventDefault()

    //     setSelectedEstacion()
    //     setShowReporte(false)
    //     setStartDate(new Date())
    //     setEndDate(new Date())
    //     setSelected()
    //     setData()
    // }
        

    useEffect(()=>{
        if(startDate > endDate){
            setErrorFecha(true)
        }
        else{ setErrorFecha(false)}
    })

    return(
        <div className='card'>
                        <div className='card-body'>
                        <form>
                            <div className='row'>
                                <label className="col-sm-1 col-form-label">Fecha Inicio</label>
                                <div className="col-sm-3">
                                    <DatePicker className="form-control w-100"
                                    selected={startDate} onChange={(date) => setStartDate(date)}
                                    showTimeSelect
                                    locale={es}
                                    timeFormat="HH:mm"
                                    timeIntervals={5}
                                    timeCaption="hora"
                                    injectTimes={[
                                    setHours(setMinutes(new Date(), 59), 23),
                                ]}
                                    dateFormat="dd/MM/yyyy h:mm aa"
                                    />
                                </div>
                                <label className="col-sm-1 col-form-label ">Fecha Fin</label>
                                <div className="col-sm-3">
                                    <DatePicker className="form-control w-100"
                                        showTimeSelect
                                        timeFormat="HH:mm"
                                        locale={es}
                                        timeIntervals={5}
                                        timeCaption="hora"
                                        injectTimes={[
                                            setHours(setMinutes(new Date(), 59), 23),
                                        ]}
                                        dateFormat="dd/MM/yyyy h:mm aa"
                                        selected={endDate} onChange={(date) => setEndDate(date)}
                                    />
                                </div>
                                <div className='col-sm-4'>
                                    <button type="button" className="btn btn-group btn-primary btn-icon-text btn-rounded" title='Buscar'><i className="mdi mdi-magnify btn-icon-prepend"></i>Buscar</button>
                                    <button type="button" className='btn btn-group btn-outline-primary btn-icon-text btn-rounded' title='Limpiar filtros, antes de hacer una nueva consulta' onClick={(e)=>limpiarFiltros(e)}><i className='mdi mdi mdi-delete btn-icon-prepend'></i><span className='btn-icon-text'>Limpiar filtros</span></button>
                                    {errorFecha && <p className='text-danger'>La fecha de inicio no puede ser mayor a la fecha de fin.</p>}
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
    )
}
function Tabs(){
    const [graphTab, setGraphTab] = useState(true);
    const handleTab = () => {
        setGraphTab(!graphTab);
    }
    return(
        <>
        <div className='row pt-1'>
            <div className='col-2'>
                Demanda
            </div>
            <div className='col-1' style={{paddingLeft:'.1rem'}}>
                <label className="switch">
                    <input type="checkbox" className="switch " onClick={handleTab}/>
                    <div className="slider"></div>
                </label>
            </div>
            <div className='col-2 '>Generación</div>
        </div>
        <div >
            <hr />
            {graphTab &&
            <Demanda />}
            {!graphTab &&
            <Generación />}
        </div>
        </>
    );
}

//agregado para separar mendoza y san juan





function Demanda(){

    const labels = ['00','04','08','12','16','20']

    const dataDemanda = {
        labels: labels,
        datasets: [{
            label: "Mendoza",
            data: [10, 19, 16, 25, 20, 30],
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1,
            fill: false
        },
        {
            label: "San Juan",
            data: [5, 9, 13, 15, 12, 13],
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1,
            fill: false
        }
    ]
    };
    return(
        <>
            <h4 className='card-title'>Demanda [MW]</h4>
            <Line data={dataDemanda} options={{elements: {point: {radius: 0,hitRadius:5,hoverRadius:5}}}} />
        </>
    );
}

function Generación(){

    const dataGeneracion = {
        labels:['00','04','08','12','16','20'],
        datasets:[{
            label: "Mendoza",
            data: [10, 19, 16, 25, 20, 30],
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1,
            fill: 1
            },

            {label: "San Juan",
            data: [5, 9, 13, 15, 12, 13],
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1,
            fill: 'origin'
    }]}
    return(
        <>
            <h4 className='card-title'>Generación [MW]</h4>
            <Line data={dataGeneracion} options={{elements: {point: {radius: 0,hitRadius:5,hoverRadius:5}}}}
            />
        </>
    );
}

function Tabla(){

    return(
        <>
        <div className="table-responsive">
            <table className='table table-bordered'>
                <thead>
                    <tr>
                        <th>Mendoza</th>
                        <th>San Juan</th>
                        <th>Cuyo</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>13/01/2022</td>
                        <td>13/01/2022</td>
                        <td>13/01/2022</td>
                    </tr>
                    <tr>
                        <td>1231</td>
                        <td>583</td>
                        <td>1771</td>
                    </tr>
                </tbody>
            </table>
        </div>
        </>
    );
}
function TablaTemp(){
return(
    <div className='col-md-4 grid-margin stretch-card'>
                            <div className='card'>
                                <div className='card-body'>
                                    <h4 className='card-title'>Temperatura ambiente (°C)</h4>
                                    <div className="table-responsive">
                                        <table className='table table-bordered'>
                                            <thead>
                                                <tr>
                                                    <th colSpan={2}>Mendoza</th>
                                                    <th colSpan={2}>San Juan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Máx </td>
                                                    <td>Min</td>
                                                    <td>Máx</td>
                                                    <td>Min</td>
                                                </tr>
                                                <tr>
                                                    <td>16,9</td>
                                                    <td>0,1</td>
                                                    <td>19,3</td>
                                                    <td>6,6</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
);
}
function TablaDemandas(){
    return(
        <div className="col-md-8 grid-margin stretch-card">
                            <div className='card'>
                                <div className='card-body'>
                                    <h4 className='card-title'>Demandas (MW)</h4>
                                    <div className="table-responsive">
                                        <table className='table table-bordered'>
                                            <thead>
                                                <tr>
                                                    <th colSpan={2}>Mendoza</th>
                                                    <th colSpan={2}>San Juan</th>
                                                    <th colSpan={2}>Cuyo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Máxima </td>
                                                    <td>Mínima</td>
                                                    <td>Máxima</td>
                                                    <td>Mínima</td>
                                                    <td>Máxima</td>
                                                    <td>Mínima</td>
                                                </tr>
                                                <tr>
                                                    <td>21:00</td>
                                                    <td>04:00</td>
                                                    <td>21:30</td>
                                                    <td>05:00</td>
                                                    <td>21:00</td>
                                                    <td>04:00</td>
                                                </tr>
                                                <tr>
                                                    <td>880</td>
                                                    <td>607</td>
                                                    <td>369</td>
                                                    <td>229</td>
                                                    <td>1247</td>
                                                    <td>839</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
    );
}
function Map(){
    const mapData = [
        {name: 'AR-M', value: 500},
        {name: 'AR-J', value: 212.33}
    ]

    const getAllData2 = () => {
        mapData.forEach((region) =>{
            return region.value
        })
    }

    const getdata = (key)=> {
        var countryData = [];
        mapData.forEach((region) =>{
            countryData[region.name] = region.value;
        })
        return countryData[key]
        }
        const getalldata = () =>{
        var countryData = [];
        mapData.forEach((region) =>{
            countryData[region.name] = region.value;
        })
        return countryData;
    }
        const regionName = (e, el,valor) => {
            if(getdata(valor)){
                    const frase = el.html() +  '<br> Demanda: ' + `${getdata(valor)}`;
                    el.html(`${frase}`);
            }
        };
    return(
        <div className='card'>
                            <div className='card-body'>
                                <h4 className='card-title'>Mapa</h4>
                                <div className='vector-map' style={{height:"450px"}}>
                                    <VectorMap
                                        map={arMill}
                                        backgroundColor="transparent"
                                        panOnDrag={true}
                                        containerClassName="dashboard-vector-map"
                                        focusOn= { {
                                            x: 0.5,
                                            y: 0.5,
                                            scale: 1,
                                            animate: true
                                            }}
                                        onRegionTipShow={regionName}
                                        series={{
                                            regions: [
                                                {
                                                    values: getalldata(), //can be directly served //with api response or any data
                                                    scale: ['#C8EEFF', '#0071A4'], //color range
                                                    normalizeFunction: "polynomial"
                                                }
                                            ]
                                            }}
                                    />
                                </div>
                            </div>
                        </div>
    );
}
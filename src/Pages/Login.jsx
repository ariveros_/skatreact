import React, {useEffect, useContext } from 'react';
import axios from 'axios'
import '../Components/css/style.css'
import { useState } from "react";
import { Navigate} from 'react-router-dom';
import Logo from '../Components/images/logoSKATmini.svg';
import {LoginContext} from '../Contexts/LoginContext'
import url from '../enviroment';


function Login(){
    //from Context
    const {logueado,setLogueado} = useContext(LoginContext)
    //States
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [redirect, setRedirect] = useState(false);
    const [errorBack, setErrorBack] = useState(false);
    const [credentialError, setCredentialError] = useState(false);

    //Envio del form
    const submit = async(e) => {
        e.preventDefault();
        try {
            await axios.post(url + `/AuthAPI/login`, {
                email: email,
                password: password
            }, { withCredentials: true, timeout:10000});

            setErrorBack(false);
            setCredentialError(false);
            setLogueado(true);
            localStorage.setItem('userExists', true);
            setRedirect(true);
        } catch (error) {
            if (error.response) {
                // El error es un error de respuesta del servidor
                if (error.response.status === 400) {
                    setCredentialError(true);
                } else {
                    setErrorBack(true);
                }
            } else if (error.request) {
                // El error es un error de solicitud, posiblemente relacionado con CORS o problemas de certificado
                console.log('Error en la solicitud:', error.message);
                setErrorBack(true);
            } else {
                // Otro tipo de error
                console.log('Error:', error.message);
                setErrorBack(true);
            }
        }
    };
    if(redirect){
        return <Navigate to ="/Home" />
    }
    
    return(
        <>
        <div className="container-scroller">
                <div className="container-fluid page-body-wrapper full-page-wrapper">
                    <div className="row w-100 m-0">
                        <div className="content-wrapper full-page-wrapper d-flex align-items-center auth login-bg">
                                <div className="card col-lg-4 mx-auto  d-flex align-items-center justify-content-between">
                                    <div className="brand-logo logo-login pt-4">
                                        <img src={Logo} alt='Skat logo'></img>
                                    </div>
                                    <div className="card-body px-5 py-3">
                                        <h3 className="card-title text-left mb-3">Iniciar sesión</h3>
                                        <form onSubmit={submit}>     
                                            <div className='form-group'>
                                            <label htmlFor='email'>Correo electrónico:</label>
                                            <input
                                                type="email"
                                                className='form-control p_input'
                                                name='email'
                                                onChange={e => setEmail(e.target.value)}
                                                />
                                            <br />
                                            <label htmlFor='password'>Contraseña:</label>
                                            <input
                                                type="password"
                                                className='form-control p_input'
                                                name='password'
                                                autoComplete="on"
                                                onChange={e => setPassword(e.target.value)}
                                                />
                                            {credentialError && <p className='text-danger'>Las credenciales son incorrectas</p>}
                                            {errorBack && <p className='text-danger'>Ha ocurrido un error intentando iniciar sesión</p>}
                                            <br />
                                            <div className='d-grip'>
                                                <button className='btn btn-primary btn-lg w-100'>Iniciar Sesión</button>
                                            </div>
                                            
                                            <div className='row justify-content-end g-0 pt-2'>
                                                <a href="/RecuperarContrasenia" className="col-md-5 auth-link text-light"> ¿Olvidó contraseña?</a>
                                            </div>
                                            <hr/>
                                            <p className="sign-up text-secondary">¿Nuevo? <a href='/Contacto' className='text-primary'>Contáctese</a> con el administrador del sistema </p>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                        </div>
                    </div>     
                </div>
        </div>
        </>
    );
}

export default Login;
import React from "react";
import { LineGraph } from "../../Registros/Graficos/Components/LineGraph";

export function TemperaturaMdzaBar(){
    //TEMPERATURA MENDOZA
    const labels = [];
    for (let index = 1; index <= 24; index++) {
        labels.push(index.toString())
    }
    //00
    let temp00 = "4.494;----;4.494;----;4.445;----;4.445;----;4.396;----;4.396;----;4.347;----;4.347;----;4.347;----;4.347;----;4.347;----;4.396;----;"
    const temp00Arr = temp00.split(';')
    const tmp00 = []
    for(let i=0; i< temp00Arr.length -1 ; i++){
        if(i % 2 === 0){tmp00.push(parseFloat(temp00Arr[i]))}
    }
    const tprom00 = tmp00.reduce((a,b)=>a+b,0) / tmp00.length
    //01
    let temp01 = "4.347;----;4.396;----;4.347;----;4.347;----;4.347;----;4.347;----;4.396;----;4.347;----;4.347;----;4.347;----;4.299;----;4.299;----;"
    const temp01Arr = temp01.split(';')
    const tmp01 = []
    for(let i=0; i< temp01Arr.length -1 ; i++){
        if(i%2 === 0){tmp01.push(parseFloat(temp01Arr[i]))}
    }
    const tprom01 = tmp01.reduce((a,b)=>a+b,0) / tmp01.length
    //02
    let temp02 = "4.299;----;4.250;----;4.201;----;4.152;----;4.054;----;3.957;----;3.908;----;3.908;----;3.859;----;3.859;----;3.810;----;3.810;----;"
    const temp02Arr = temp02.split(';')
    const tmp02 = []
    for(let i=0; i< temp02Arr.length -1; i++){
        if(i%2 === 0){tmp02.push(parseFloat(temp02Arr[i]))}
    }
    const tprom02 = tmp02.reduce((a,b)=>a+b,0) / tmp02.length
    //03
    let temp03 = "3.713;----;3.761;----;3.713;----;3.664;----;3.664;----;3.664;----;3.615;----;3.566;----;3.566;----;3.566;----;3.517;----;3.566;----;"
    const temp03Arr = temp03.split(';')
    const tmp03 = []
    for(let i=0; i< temp03Arr.length-1 ; i++){
        if(i%2 === 0){tmp03.push(parseFloat(temp03Arr[i]))}}
    const tprom03 = tmp03.reduce((a,b)=>a+b,0) / tmp03.length
    //04
    let temp04 = "3.517;----;3.517;----;3.517;----;3.517;----;3.517;----;3.566;----;3.566;----;3.566;----;3.517;----;3.517;----;3.517;----;3.468;----;"
    const temp04Arr = temp04.split(';')
    const tmp04 = []
    for(let i=0; i< temp04Arr.length-1; i++){
        if(i%2 === 0){tmp04.push(parseFloat(temp04Arr[i]))}}
    const tprom04 = tmp04.reduce((a,b)=>a+b,0) / tmp04.length
    //05
    let temp05 = "3.420;----;3.371;----;3.371;----;3.371;----;3.371;----;3.322;----;3.371;----;3.371;----;3.371;----;3.371;----;3.322;----;3.322;----;"
    const temp05Arr = temp05.split(';')
    const tmp05 = []
    for(let i=0; i< temp05Arr.length-1 ; i++){
        if(i%2 === 0){tmp05.push(parseFloat(temp05Arr[i]))}}
    const tprom05 = tmp05.reduce((a,b)=>a+b,0) / tmp05.length
    //06
    let temp06 = "3.322;----;3.322;----;3.273;----;3.322;----;3.273;----;3.273;----;3.273;----;3.273;----;3.273;----;3.322;----;3.322;----;3.322;----;"
    const temp06Arr = temp06.split(';')
    const tmp06 = []
    for(let i=0; i< temp06Arr.length -1; i++){
        if(i%2 === 0){tmp06.push(parseFloat(temp06Arr[i]))}}
    const tprom06 = tmp06.reduce((a,b)=>a+b,0) / tmp06.length
    //07
    let temp07 = "3.371;----;3.371;----;3.371;----;3.371;----;3.420;----;3.371;----;3.420;----;3.420;----;3.420;----;3.420;----;3.371;----;3.273;----;"
    const temp07Arr = temp07.split(';')
    const tmp07 = []
    for(let i=0; i< temp07Arr.length -1; i++){
        if(i%2 === 0){tmp07.push(parseFloat(temp07Arr[i]))}}
    const tprom07 = tmp07.reduce((a,b)=>a+b,0) / tmp07.length
    //08
    let temp08 = "3.224;----;3.127;----;3.029;----;2.980;----;2.980;----;2.931;----;2.980;----;2.931;----;2.980;----;2.980;----;2.980;----;2.980;----;"
    const temp08Arr = temp08.split(';')
    const tmp08 = []
    for(let i=0; i< temp08Arr.length -1; i++){
        if(i%2 === 0){tmp08.push(parseFloat(temp08Arr[i]))}}
    const tprom08 = tmp08.reduce((a,b)=>a+b,0) / tmp08.length
    //09
    let temp09 = "2.980;----;2.834;----;2.785;----;2.687;----;2.638;----;2.638;----;2.638;----;2.638;----;2.687;----;2.736;----;2.785;----;2.882;----;"
    const temp09Arr = temp09.split(';')
    const tmp09 = []
    for(let i=0; i< temp09Arr.length -1; i++){
        if(i%2 === 0){tmp09.push(parseFloat(temp09Arr[i]))}}
    const tprom09 = tmp09.reduce((a,b)=>a+b,0) / tmp09.length
    //10
    let temp10 = "2.931;----;2.931;----;2.980;----;2.980;----;3.078;----;3.078;----;3.175;----;3.175;----;3.175;----;3.224;----;3.273;----;3.371;----;"
    const temp10Arr = temp10.split(';')
    const tmp10 = []
    for(let i=0; i< temp10Arr.length -1; i++){
        if(i%2 === 0){tmp10.push(parseFloat(temp10Arr[i]))}}
    const tprom10 = tmp10.reduce((a,b)=>a+b,0) / tmp10.length
    //11
    let temp11 = "3.322;----;3.420;----;3.371;----;3.371;----;3.371;----;3.420;----;3.517;----;3.566;----;3.517;----;3.664;----;3.713;----;3.713;----;"
    const temp11Arr = temp11.split(';')
    const tmp11 = []
    for(let i=0; i< temp11Arr.length -1; i++){
        if(i%2 === 0){tmp11.push(parseFloat(temp11Arr[i]))}}
    const tprom11 = tmp11.reduce((a,b)=>a+b,0) / tmp11.length
    //12
    let temp12 = "3.810;----;3.761;----;3.908;----;3.908;----;3.810;----;3.810;----;3.957;----;3.908;----;3.908;----;3.957;----;4.103;----;4.103;----;"
    const temp12Arr = temp12.split(';')
    const tmp12 = []
    for(let i=0; i< temp12Arr.length -1; i++){
        if(i%2 === 0){tmp12.push(parseFloat(temp12Arr[i]))}}
    const tprom12 = tmp12.reduce((a,b)=>a+b,0) / tmp12.length
    //13
    let temp13 = "4.103;----;4.299;----;4.396;----;4.640;----;4.884;----;4.933;----;4.884;----;5.129;----;5.031;----;5.031;----;5.031;----;5.129;----;"
    const temp13Arr = temp13.split(';')
    const tmp13 = []
    for(let i=0; i< temp13Arr.length -1; i++){
        if(i%2 === 0){tmp13.push(parseFloat(temp13Arr[i]))}}
    const tprom13 = tmp13.reduce((a,b)=>a+b,0) / tmp13.length
    //14
    let temp14 = "4.884;----;4.836;----;4.787;----;4.592;----;4.640;----;4.640;----;4.787;----;4.884;----;4.836;----;4.738;----;4.884;----;4.836;----;"
    const temp14Arr = temp14.split(';')
    const tmp14 = []
    for(let i=0; i< temp14Arr.length -1; i++){
        if(i%2 === 0){tmp14.push(parseFloat(temp14Arr[i]))}}
    const tprom14 = tmp14.reduce((a,b)=>a+b,0) / tmp14.length
    //15
    let temp15 = "4.884;----;4.836;----;4.884;----;5.031;----;5.031;----;4.933;----;4.933;----;4.933;----;5.031;----;5.031;----;5.080;----;5.129;----;"
    const temp15Arr = temp15.split(';')
    const tmp15 = []
    for(let i=0; i< temp15Arr.length -1; i++){
        if(i%2 === 0){tmp15.push(parseFloat(temp15Arr[i]))}}
    const tprom15 = tmp15.reduce((a,b)=>a+b,0) / tmp15.length
    //16
    let temp16 = "5.129;----;5.226;----;5.177;----;5.226;----;5.275;----;5.226;----;5.226;----;5.226;----;5.226;----;5.226;----;5.177;----;5.177;----;"
    const temp16Arr = temp16.split(';')
    const tmp16 = []
    for(let i=0; i< temp16Arr.length -1; i++){
        if(i%2 === 0){tmp16.push(parseFloat(temp16Arr[i]))}}
    const tprom16 = tmp16.reduce((a,b)=>a+b,0) / tmp16.length
    //17
    let temp17 ="5.226;----;5.177;----;5.129;----;5.031;----;5.031;----;5.080;----;5.031;----;4.884;----;4.836;----;4.738;----;4.738;----;4.738;----;"
    const temp17Arr = temp17.split(';')
    const tmp17 = []
    for(let i=0; i< temp17Arr.length -1; i++){
        if(i%2 === 0){tmp17.push(parseFloat(temp17Arr[i]))}}
    const tprom17 = tmp17.reduce((a,b)=>a+b,0) / tmp17.length
    //18
    let temp18 = "4.640;----;4.592;----;4.592;----;4.592;----;4.543;----;4.445;----;4.445;----;4.347;----;4.347;----;4.347;----;4.299;----;4.347;----;"
    const temp18Arr = temp18.split(';')
    const tmp18 = []
    for(let i=0; i< temp18Arr.length -1; i++){
        if(i%2 === 0){tmp18.push(parseFloat(temp18Arr[i]))}}
    const tprom18 = tmp18.reduce((a,b)=>a+b,0) / tmp18.length
    //19
    let temp19 = "4.201;----;4.201;----;4.250;----;4.201;----;4.201;----;4.201;----;4.152;----;4.201;----;4.152;----;4.152;----;4.103;----;4.152;----;"
    const temp19Arr = temp19.split(';')
    const tmp19 = []
    for(let i=0; i< temp19Arr.length -1; i++){
        if(i%2 === 0){tmp19.push(parseFloat(temp19Arr[i]))}}
    const tprom19 = tmp19.reduce((a,b)=>a+b,0) / tmp19.length
    //20
    let temp20 = "4.152;----;4.152;----;4.103;----;4.103;----;4.103;----;4.103;----;4.103;----;4.103;----;4.103;----;4.152;----;4.103;----;4.103;----;"
    const temp20Arr = temp20.split(';')
    const tmp20 = []
    for(let i=0; i< temp20Arr.length -1; i++){
        if(i%2 === 0){tmp20.push(parseFloat(temp20Arr[i]))}}
    const tprom20 = tmp20.reduce((a,b)=>a+b,0) / tmp20.length
    //21
    let temp21 = "4.152;----;4.103;----;4.054;----;4.006;----;3.957;----;3.957;----;3.957;----;3.908;----;3.859;----;3.908;----;3.810;----;3.859;----;"
    const temp21Arr = temp21.split(';')
    const tmp21 = []
    for(let i=0; i< temp21Arr.length -1; i++){
        if(i%2 === 0){tmp21.push(parseFloat(temp21Arr[i]))}}
    const tprom21 = tmp21.reduce((a,b)=>a+b,0) / tmp21.length
    //22
    let temp22 = "3.810;----;3.810;----;3.810;----;3.810;----;3.810;----;3.810;----;3.810;----;3.761;----;3.810;----;3.859;----;3.761;----;3.713;----;"
    const temp22Arr = temp22.split(';')
    const tmp22 = []
    for(let i=0; i< temp22Arr.length -1; i++){
        if(i%2 === 0){tmp22.push(parseFloat(temp22Arr[i]))}}
    const tprom22 = tmp22.reduce((a,b)=>a+b,0) / tmp22.length
    //23
    let temp23 = "3.713;----;3.713;----;3.761;----;3.713;----;3.713;----;3.761;----;3.761;----;3.761;----;3.761;----;3.713;----;3.713;----;3.664;----;"
    const temp23Arr = temp23.split(';')
    const tmp23 = []
    for(let i=0; i< temp23Arr.length -1; i++){
        if(i%2 === 0){tmp23.push(parseFloat(temp23Arr[i]))}}
    const tprom23 = tmp23.reduce((a,b)=>a+b,0) / tmp23.length

    //array de temperaturas
    let tempDay = [tprom00,tprom01,tprom01,tprom02,tprom03,tprom04,tprom05,tprom06,tprom07,tprom08,tprom09,tprom10,tprom11,tprom12,
    tprom13,tprom14,tprom15,tprom16,tprom17,tprom18,tprom19,tprom20,tprom21,tprom22,tprom23]


    const data2 = {
        labels: labels,
        datasets: [{
            label: "212047 - GRAN MENDOZA TEMPERATURA",
            data: tempDay,
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1,
            fill: false
        }]
    }
    return(
        <>
            <div className='card'>
                <div className='card-body'>
                    <h4 className='card-title'>Temperatura Mendoza</h4>
                    {data2 && <LineGraph data={data2} />}
                </div>
            </div>
        </>
    );
}
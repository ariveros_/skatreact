import { Table } from "react-bootstrap";

export function MaximoIntercambio(){

    const datosMaximoIntercambio ={
        "DatosMaximo":{
            "HorasRiesgo" : "22,0",
            "ReciboMW" : "-714",
            "SuministroMw" : "-372"
        }
    }

    const datosEnergiasMaximasH = {
        "SanJuan":{
            "Nombre" : "SanJuan",
            "Valor" : "10,175",
            "Fecha" : "22/01/16"
        },
        "Mendoza":{
            "Nombre" : "Mendoza",
            "Valor" : "25,539",
            "Fecha" : "16/01/14"
        },
    
        "Cuyo" :{
            "Nombre" : "Cuyo",
            "Valor" : "32,839",
            "Fecha" : "22/01/14"
        },
        "EMA":{
            "Fecha" : "31/01/2009",
            "Valor" : "0,821"
        }
}
    


    return(
        <div className="card">
            <div className="card-body">
                <h4 className="card-title">Máximo Intercambio en 500 KV</h4>
                <div className="row">
                    <div className="col-xl-12 p-1 col-sm-12 col-md-6">
                        <DatosMI {...datosMaximoIntercambio}/>
                                        {/* <Table striped bordered hover variant="dark" responsive>
                                            <thead>
                                                <tr>
                                                    <th colSpan={3}>Máximo Intercabio en 500 KV</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Horas en riesgo -40%</td>
                                                    <td>Recibo (MW)</td>
                                                    <td>Suministro (MW)</td>
                                                </tr>
                                                <tr>
                                                    <td>22,0</td>
                                                    <td>-714</td>
                                                    <td>-372</td>
                                                </tr>
                                            </tbody>
                                        </Table> */}
                    </div>
                    
                    <h4 className="card-title pt-3">EMA y Energías Máximas Historicas</h4>
                    <div className="col-xl-12 p-1 col-sm-12 col-md-6">
                        <DatosEMA {...datosEnergiasMaximasH}/>
                                            {/* <Table striped bordered hover variant="dark" responsive>
                                            <thead>
                                                <tr>
                                                    <th>Fabrica EMA</th>
                                                    <th colSpan={3}>Energías Máximas Historicas</th>
                                                </tr>
                                                <tr>
                                                    <th>31/01/2009</th>
                                                    <th>San Juan</th>
                                                    <th>Mendoza</th>
                                                    <th>Cuyo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>0,821</td>
                                                    <td>10,175 <br/> 22/01/16</td>
                                                    <td>25,539 <br/> 16/01/14</td>
                                                    <td>32,839 <br/> 22/01/14</td>
                                                </tr>
                                            </tbody>
                                            </Table> */}
                    </div>
                </div>
            </div>
        </div>
    )
}

function DatosMI(datosMaximoIntercambio){

    return (
        <Table striped bordered hover variant="dark">
                                <thead>
                                    {/* <tr>
                                        <th colSpan={3}>Máximo Intercabio en 500 KV</th>
                                    </tr> */}
                                    <tr>
                                        <td>Horas en riesgo -40%</td>
                                        <td>Recibo (MW)</td>
                                        <td>Suministro (MW)</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{datosMaximoIntercambio.DatosMaximo.HorasRiesgo}</td>
                                        <td>{datosMaximoIntercambio.DatosMaximo.ReciboMW}</td>
                                        <td>{datosMaximoIntercambio.DatosMaximo.SuministroMw}</td>
                                    </tr>
                                </tbody>
                            </Table>
    )
    
}

function DatosEMA(datosEnergiasMaximasH){

    return(
        <Table striped bordered hover variant="dark">
                                <thead>
                                    {/* <tr>
                                        <th>Fabrica EMA</th>
                                        <th colSpan={3}>Energías Máximas Historicas</th>
                                    </tr> */}
                                    <tr>
                                        <th>{datosEnergiasMaximasH.EMA.Fecha}</th>
                                        <th>{datosEnergiasMaximasH.SanJuan.Nombre}</th>
                                        <th>{datosEnergiasMaximasH.Mendoza.Nombre}</th>
                                        <th>{datosEnergiasMaximasH.Cuyo.Nombre}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{datosEnergiasMaximasH.EMA.Valor}</td>
                                        <td>{datosEnergiasMaximasH.SanJuan.Valor} <br/> {datosEnergiasMaximasH.SanJuan.Fecha}</td>
                                        <td>{datosEnergiasMaximasH.Mendoza.Valor} <br/> {datosEnergiasMaximasH.Mendoza.Fecha}</td>
                                        <td>{datosEnergiasMaximasH.Cuyo.Valor} <br/> {datosEnergiasMaximasH.Cuyo.Fecha}</td>
                                    </tr>
                                </tbody>
                                </Table>
    )
}
import { Stack, Table } from "react-bootstrap";

export function DemandaMWHome(){

    const datosDemandaMW = 
        {
            "Mendoza": {
                "MaximaHora": "21:00",
                "MinimaHora" : "4:00",
                "MaximoValor" : "880",
                "MinimoValor" : "607",
                "TemperaturaMax" : "16,9",
                "TemperaturaMin" : "0,1",
                "Nombre" : "Mendoza"
            },
            "SanJuan": {
                "MaximaHora" : "21:30",
                "MinimaHora" : "5:00",
                "MaximoValor" : "369",
                "MinimoValor" : "229",
                "TemperaturaMax" : "19,3",
                "TemperaturaMin" : "6,6",
                "Nombre" : "San Juan"
            },
            "Cuyo": {
                "MaximaHora" : "21:00",
                "MinimaHora" : "4:00",
                "MaximoValor" : "1247",
                "MinimoValor" : "839",
                "Nombre" : "Cuyo"
            }
    }


    return(
        <div className="card">
            <div className="card-body">
                <h4 className="card-title">Demanda MW </h4>
                <div className="row">
                    <div className="col-xl-6 p-1 col-sm-12 col-md-6">
                        <Datos {...datosDemandaMW.Mendoza}/>
                    </div>
                    <div className="col-xl-6 p-1 col-sm-12 col-md-6">
                        <Datos {...datosDemandaMW.SanJuan}/>
                    </div>
                    
                    <div className="col-xl-12 p-1 col-sm-12 col-md-12 pt-3">
                        <Datos {...datosDemandaMW.Cuyo}/>
                    </div>
                </div>
            </div>
        </div>
    )
}

// function Mendoza(mendoza) {
//     return(
//         <Table striped bordered hover variant="dark" responsive>
//                         <thead>
//                             <tr>
//                                 <th colSpan={2} className="text-center">Mendoza</th>
//                             </tr>
//                                 <tr >
//                                     <td>Máxima ({mendoza.MaximaHora}hs)</td>
//                                     <td>Minima ({mendoza.MinimaHora}hs)</td>
//                                 </tr> 
//                         </thead>
//                         <tbody>
                                    
//                                     <tr>
//                                         <td>{mendoza.MaximoValor}</td>
//                                         <td>{mendoza.MinimoValor}</td>
//                                     </tr>
//                             <tr>
//                                 <td colSpan={2}>Temperatura Ambiente C°</td>
//                             </tr>
//                             <tr>
//                                 <td>Min: {mendoza.TemperaturaMax}</td>
//                                 <td>Max: {mendoza.TemperaturaMin}</td>
//                             </tr>
//                         </tbody>
//                     </Table>
//     )
// }

// function SanJuan(sanJuan) {
//     return(
//         <Table striped bordered hover variant="dark" responsive>
//                         <thead>
//                             <tr>
//                                 <th colSpan={2} className="text-center">San Juan</th>
//                             </tr>
//                                 <tr>
//                                     <td>Máxima ({sanJuan.MaximaHora}hs)</td>
//                                     <td>Minima ({sanJuan.MinimaHora}hs)</td>
//                                 </tr> 
//                         </thead>
//                         <tbody>
                                    
//                                     <tr>
//                                         <td>{sanJuan.MaximoValor}</td>
//                                         <td>{sanJuan.MinimoValor}</td>
//                                     </tr>
//                             <tr>
//                                 <td colSpan={2}>Temperatura Ambiente C°</td>
//                             </tr>
//                             <tr>
//                                 <td>Min: {sanJuan.TemperaturaMax}</td>
//                                 <td>Max: {sanJuan.TemperaturaMin}</td>
//                             </tr>
//                         </tbody>
//                     </Table>
//     )
// }

// function Cuyo(cuyo) {
//     return(
//         <Table striped bordered hover variant="dark" responsive>
//                         <thead>
//                             <tr>
//                                 <th colSpan={2} className="text-center">Cuyo</th>
//                             </tr>
//                                 <tr>
//                                     <td>Máxima ({cuyo.MaximaHora}hs)</td>
//                                     <td>Minima ({cuyo.MinimaHora}hs)</td>
//                                 </tr> 
//                         </thead>
//                         <tbody>
                                    
//                                     <tr>
//                                         <td>{cuyo.MaximoValor}</td>
//                                         <td>{cuyo.MinimoValor}</td>
//                                     </tr>
//                         </tbody>
//                     </Table>
//     )
// }

function Datos(datos){
    return(
        <Table striped bordered hover variant="dark" responsive>
                        <thead>
                            <tr>
                                <th colSpan={2} className="text-center">{datos.Nombre}</th>
                            </tr>
                                <tr >
                                    <td>Máxima ({datos.MaximaHora}hs)</td>
                                    <td>Minima ({datos.MinimaHora}hs)</td>
                                </tr> 
                        </thead>
                        <tbody>
                                    
                                    <tr>
                                        <td>{datos.MaximoValor}</td>
                                        <td>{datos.MinimoValor}</td>
                                    </tr>
                            <tr>
                                <td colSpan={2}>Temperatura Ambiente C°</td>
                            </tr>
                            <tr>
                                <td>Min: {datos.TemperaturaMax}</td>
                                <td>Max: {datos.TemperaturaMin}</td>
                            </tr>
                        </tbody>
                    </Table>
    )
}
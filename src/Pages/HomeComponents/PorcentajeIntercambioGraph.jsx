import { Bar } from "react-chartjs-2";

export function PIntercambio(){

    const dataPI = {
        labels: ["1:00","2:00","3:00","4:00",
    "5:00","6:00","7:00","8:00","9:00","10:00",
"11:00","12:00","13:00","14:00","15:00","16:00",
"17:00","17:30","18:00","18:30","19:00","19:15",
"19:30","19:45","20:00","20:15","20:30","20:45",
"21:00","21:15","21:30","21:45","22:00","22:15",
"22:30","22:45","23:00","23:30","24:00"],
datasets:[
    {
    label:"Red",
    data:[-61.0,-65.3,-64.8,-62.9,-64.1,-63.8,-64.1,-59.3,-58.0,-49.6,-44.4,-44.8,-44.5,-41.1,-39.3,-38.0,-43.8,-48.5,-53.0,-50.8,-49.6,
    -52.1,-45.8,-46.1,-47.1,-46.1,-46.4,-47.3,-48.0,-47.9,-47.2,-47.6,-47.1,-47.3,-46.8,-46.8,-53.0,-60.9,-58.8],
    backgroundColor: [
    'rgba(255, 99, 132, 0.2)',
    // 'rgba(54, 162, 235, 0.2)',
    // 'rgba(255, 206, 86, 0.2)',
    // 'rgba(75, 192, 192, 0.2)',
    // 'rgba(153, 102, 255, 0.2)',
    // 'rgba(255, 159, 64, 0.2)'
    ],
    borderColor: [
    'rgba(255,99,132,1)',
    // 'rgba(54, 162, 235, 1)',
    // 'rgba(255, 206, 86, 1)',
    // 'rgba(75, 192, 192, 1)',
    // 'rgba(153, 102, 255, 1)',
    // 'rgba(255, 159, 64, 1)'
    ],
    borderWidth: 1
    },{
        label:"Yellow",
        data:[null,null,null,-14.0,-14.0,-14.0],
        backgroundColor: "rgba(255, 206, 86, 0.2)",
        borderColor: "rgba(255, 206, 86, 1)",
        borderWidth: 1
    }
]
    }

    const options = {
        responsive : true,
        scales:{
            x:{
                stacked:true,
            },
            y:{
                stacked:true,
            }
        }
    }

    return(
        <div className="card">
            <div className="card-body">
                <h4 className="card-title">Porcentaje de Intercambio</h4>
                <Bar data={dataPI} options={options}></Bar>
            </div>
        </div>
    )
}
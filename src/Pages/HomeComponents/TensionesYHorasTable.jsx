import { Table } from "react-bootstrap";

export function TensionesYHoras(){

    const datosTensionesYHoras=[
        {
            "id" : 1,
            "Nombre" : "Cruz de Piedra 132",
            "Horas" : "0,0",
            "MINA" : "130,5",
            "Hs" : "0,0",
            "MINB" : "130,7",
            "MAXA" : "133,1",
            "MAXB" : "133,4"
        },
        {
            "id" : 2,
            "Nombre" : "Cruz de Piedra 220",
            "Horas" : "0,0",
            "MINA" : "215,9",
            "Hs" : "0,0",
            "MINB" : "216,8",
            "MAXA" : "220,3",
            "MAXB" : "220,8"
        },
        {
            "id": 3,
            "Nombre" : "Montecaseros",
            "Horas" : "0,0",
            "MINA" : "133,4",
            "Hs" : "0,0",
            "MINB" : "0,0",
            "MAXA" : "137,5",
            "MAXB" : "0,0"
        },
        {
            "id": 4,
            "Nombre" : "Anchoris",
            "Horas" : "0,0",
            "MINA" : "0,0",
            "Hs" : "0,0",
            "MINB" : "130,1",
            "MAXA" : "0,0",
            "MAXB" : "132,9"
        },
        {
            "id": 5,
            "Nombre" : "Bajo Rio Tunuyan",
            "Horas" : "0,0",
            "MINA" : "128,4",
            "Hs" : "",
            "MINB" : "",
            "MAXA" : "131,5",
            "MAXB" : ""
        },
        {
            "id": 6,
            "Nombre" : "Capiz",
            "Horas" : "0,0",
            "MINA" : "130,1",
            "Hs" : "",
            "MINB" : "",
            "MAXA" : "132,6",
            "MAXB" : ""
        },
        {
            "id": 7,
            "Nombre" : "San Juan 132",
            "Horas" : "0,0",
            "MINA" : "130,1",
            "Hs" : "0,0",
            "MINB" : "0,0",
            "MAXA" : "134,1",
            "MAXB" : "133,8"
        },
        {
            "id": 8,
            "Nombre" : "San Juan 220",
            "Horas" : "0,0",
            "MINA" : "0,0",
            "Hs" : "",
            "MINB" : "",
            "MAXA" : "220,0",
            "MAXB" : ""
        },
        {
            "id": 9,
            "Nombre" : "Cañada Honda",
            "Horas" : "0,0",
            "MINA" : "129,1",
            "Hs" : "",
            "MINB" : "",
            "MAXA" : "132,9",
            "MAXB" : ""
        },
        {
            "id": 10,
            "Nombre" : "Reyunos",
            "Horas" : "0,0",
            "MINA" : "219,0",
            "Hs" : "0,0",
            "MINB" : "0,0",
            "MAXA" : "227,9",
            "MAXB" : "14,6"
        },
        {
            "id": 11,
            "Nombre" : "Agua del Toro",
            "Horas" : "0,0",
            "MINA" : "220,0",
            "Hs" : "0,0",
            "MINB" : "0,0",
            "MAXA" : "220,0",
            "MAXB" : "220,0"
        },
        {
            "id": 12,
            "Nombre" : "Pedro Vargas",
            "Horas" : "0,0",
            "MINA" : "134,9",
            "Hs" : "0,0",
            "MINB" : "0,0",
            "MAXA" : "138,5",
            "MAXB" : "0,1"
        },
        {
            "id": 13,
            "Nombre" : "San Rafael",
            "Horas" : "0,0",
            "MINA" : "134,0",
            "Hs" : "",
            "MINB" : "",
            "MAXA" : "137,4",
            "MAXB" : ""
        },
        {
            "id": 14,
            "Nombre" : "Lujan de Cuyo",
            "Horas" : "0,0",
            "MINA" : "129,3",
            "Hs" : "0,0",
            "MINB" : "130,6",
            "MAXA" : "131,8",
            "MAXB" : "132,9"
        },
        
    ]
    


    return(
        <div className="card">
            <div className="card-body">
                <h4 className="card-title">Tensiones fuera de banda en barras y cantidad de horas</h4>
                    <DatosTensionesYHorasTabla {...{datosTensionesYHoras}}/>
            </div>
        </div>
    )
}

function DatosTensionesYHorasTabla({datosTensionesYHoras}){

    return(
        <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                        <td>EE.TT</td>
                        <td>Horas</td>
                        <td>MIN (A)</td>
                        <td>HS</td>
                        <td>MIN (B)</td>
                        <td>MAX (A)</td>
                        <td>MAX (B)</td>
                    </tr>
                </thead>
                <tbody>
                    {datosTensionesYHoras.map((estacion)=>(
                        <tr key={estacion.id}>
                            <td>{estacion.Nombre}</td>
                            <td>{estacion.Horas}</td>
                            <td>{estacion.MINA}</td>
                            <td>{estacion.hs}</td>
                            <td>{estacion.MINB}</td>
                            <td>{estacion.MAXA}</td>
                            <td>{estacion.MAXB}</td>
                        </tr>
                    ))}
                </tbody>
            </Table>
    )
}
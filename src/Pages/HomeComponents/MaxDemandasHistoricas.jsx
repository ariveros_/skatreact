import { Table } from "react-bootstrap";

const datosDemandaHistorica ={
    "Mendoza":{
        "fecha" : "13/01/22",
        "valor" : "1231"
    },
    "SanJuan":{
        "fecha" : "13/01/22",
        "valor" : "583"
    },
    "Cuyo":{
        "fecha" : "13/01/22",
        "valor" : "1771"
    }
}
const datosEnergia = {
    "EMA":{
        "maximoMW" : "0"
    },
    "Energias":{
        "fabricaEMA" : "0,000",
        "sanJuan" : "7,154",
        "mendoza" : "17,888",
        "cuyo" : "25,037"
    }
}

export function MaxDemandasHistoricas (){

    return(
        <div className="card">
            <div className="card-body">
                <h4 className="card-title">Máximas Demandas Históricas</h4>
                <div className="row">
                    <div className="col-xl-6 p-1 col-sm-12 col-md-6">
                        <DemandaMax {...datosDemandaHistorica}/>
                    </div>
                    {/* <h4 className="card-title">Ema y Energias del día</h4> */}
                    <div className="col-xl-6 p-1 col-sm-12 col-md-6">
                        <DemandaMax2 {...datosEnergia}/>
                    </div>
                    {/* <Table striped bordered hover variant="dark" responsive>
                        <thead>
                            <tr>
                                <th colSpan={3}>Máximas Demandas Históricas</th>
                                <th>EMA</th>
                                <th colSpan={4}>Energías Del Día (GW/h)</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                                <th>Mendoza</th>
                                <th>San Juan</th>
                                <th>Cuyo</th>
                                <th rowSpan={2}>Máximo MW</th>
                                <th rowSpan={2}>Fábrica EMA</th>
                                <th rowSpan={2}>San Juan</th>
                                <th rowSpan={2}>Mendoza</th>
                                <th rowSpan={2}>Cuyo</th>
                            </tr>
                            <tr>
                                <td>13/01/22</td>
                                <td>13/01/22</td>
                                <td>13/01/22</td>
                            </tr>
                            <tr>
                                <td>1231</td>
                                <td>583</td>
                                <td>1771</td>
                                <td>0</td>
                                <td>0,000</td>
                                <td>7,154</td>
                                <td>17,888</td>
                                <td>25,037</td>
                            </tr>
                        </tbody>
                    </Table> */}
                </div>

                    {/* <div className="row">
                        <div className="col-md-12 col-form-label">
                            <Table striped bordered hover variant="dark" responsive>
                                <thead>
                                    <tr>
                                        <th colSpan={3}>Máximo Intercabio en 500 KV</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Horas en riesgo -40%</td>
                                        <td>Recibo (MW)</td>
                                        <td>Suministro (MW)</td>
                                    </tr>
                                    <tr>
                                        <td>22,0</td>
                                        <td>-714</td>
                                        <td>-372</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </div>
                        <div className="col-md-12 col-form-label">
                                <Table striped bordered hover variant="dark" responsive>
                                <thead>
                                    <tr>
                                        <th>Fabrica EMA</th>
                                        <th colSpan={3}>Energías Máximas Historicas</th>
                                    </tr>
                                    <tr>
                                        <th>31/01/2009</th>
                                        <th>San Juan</th>
                                        <th>Mendoza</th>
                                        <th>Cuyo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>0,821</td>
                                        <td>10,175 <br/> 22/01/16</td>
                                        <td>25,539 <br/> 16/01/14</td>
                                        <td>32,839 <br/> 22/01/14</td>
                                    </tr>
                                </tbody>
                                </Table>
                        </div>
                    </div> */}
            </div>
        </div>
    )
}

function DemandaMax(datosDemandaHistorica){
    return(
            <Table striped bordered hover variant="dark">
                        <thead>
                            <tr>
                                {/* <th colSpan={3}>Máximas Demandas Históricas</th>
                                <th>EMA</th>
                                <th colSpan={4}>Energías Del Día (GW/h)</th> */}
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                                <th>Mendoza</th>
                                <th>San Juan</th>
                                <th>Cuyo</th>
                                {/* <th rowSpan={2}>Máximo MW</th>
                                <th rowSpan={2}>Fábrica EMA</th>
                                <th rowSpan={2}>San Juan</th>
                                <th rowSpan={2}>Mendoza</th>
                                <th rowSpan={2}>Cuyo</th> */}
                            </tr>
                            <tr>
                                <td>{datosDemandaHistorica.Mendoza.fecha}</td>
                                <td>{datosDemandaHistorica.SanJuan.fecha}</td>
                                <td>{datosDemandaHistorica.Cuyo.fecha}</td>
                            </tr>
                            <tr>
                                <td>{datosDemandaHistorica.Mendoza.valor}</td>
                                <td>{datosDemandaHistorica.SanJuan.valor}</td>
                                <td>{datosDemandaHistorica.Cuyo.valor}</td>
                                {/* <td>0</td>
                                <td>0,000</td>
                                <td>7,154</td>
                                <td>17,888</td>
                                <td>25,037</td> */}
                            </tr>
                        </tbody>
                    </Table>
    )
}
function DemandaMax2(datosEnergia){
    return(
            <Table striped bordered hover variant="dark">
                        <thead>
                            <tr>
                                {/* <th colSpan={3}>Máximas Demandas Históricas</th> */}
                                <th>EMA</th>
                                <th colSpan={9}>Energías Del Día (GW/h)</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                                <th>Máximo MW</th>
                                <th>Fábrica EMA</th>
                                <th>San Juan</th>
                                <th>Mendoza</th>
                                <th>Cuyo</th>
                            </tr>
                            <tr>
                                <td>{datosEnergia.EMA.maximoMW}</td>
                                <td>{datosEnergia.Energias.fabricaEMA}</td>
                                <td>{datosEnergia.Energias.sanJuan}</td>
                                <td>{datosEnergia.Energias.mendoza}</td>
                                <td>{datosEnergia.Energias.cuyo}</td>
                            </tr>
                        </tbody>
                    </Table>
    )
}
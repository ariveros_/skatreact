import { Table } from "react-bootstrap";
export function Tiempo() {

    return(
        <div className="card">
            <div className="card-body">
                <h4 className="card-title">Prueba de Tiempo</h4>
                    <Table striped bordered hover variant="dark" responsive>
                        <thead>
                            <tr>
                                <th colSpan={6} className="text-center table-warning">Estado del tiempo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td className="table-warning" colSpan={3}>Mendoza</td>
                                <td>Bueno</td>
                            </tr>
                            <tr>
                                <td className="table-warning" colSpan={3}>San Juan</td>
                                <td>Bueno</td>
                            </tr>
                        </tbody>
                    </Table>
            </div>
        </div>
    )
}
import React from 'react';
import {Line} from 'react-chartjs-2';
import { LineGraph } from '../../Registros/Graficos/Components/LineGraph';
export const SanJuanDemandaGeneracion = () => {
    return(
        <div className='card'>
            <div className='card-body'>
                <SanJuanData />
            </div>
        </div>
    );
}

const SanJuanData = () =>{

    const labels = ['00','04','08','12','16','20']

    const dataDemanda = {
        labels: labels,
        datasets: [{
            label: "Demanda",
            data: [10, 19, 16, 25, 20, 30],
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1,
            fill: false
        },
        {
            label: "Generacion",
            data: [5, 9, 13, 15, 12, 13],
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1,
            fill: 'origin'
        }
    ]
    };
    return(
        <>
            <h4 className='card-title'>San Juan</h4>
            {dataDemanda && <LineGraph data={dataDemanda} />}
        </>
    );
}
import React from 'react';
import '../Components/css/style.css';

import {Line, Bar} from 'react-chartjs-2';
//import {Chart as ChartJS} from 'chart.js/auto'

//graficos
import {DemandaMzaLine} from './HomeComponents/DemandaGraph'
import {TemperaturaMdzaBar} from './HomeComponents/TemperaturaGraph'
import EstacionApilable from '../Registros/Graficos/EstacionApilable';
//tablas
import { DemandaMWHome } from './HomeComponents/DemandaMW';
import { MaxDemandasHistoricas } from './HomeComponents/MaxDemandasHistoricas';
import { MaximoIntercambio } from './HomeComponents/Intercambio';
import { TensionesYHoras } from './HomeComponents/TensionesYHorasTable';

function Home(){


    return(

    <>
        <div className="page-header">
            <h3 className="page-title">Home</h3>
            <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item"><a href="!#" onClick={event => event.preventDefault()}>Scadas</a></li>
                    <li className="breadcrumb-item active" aria-current="page">Gráficos</li>
                </ol>
            </nav>
        </div>
        <div className='row'>
            
                {/* <div className="col-md-6 grid-margin stretch-card">
                    <DemandaMzaLine/>
                </div>
                <div className="col-md-6 grid-margin stretch-card">
                    <TemperaturaMdzaBar />
                </div> */}
                <div className="col-md-12 grid-margin stretch-card">
                    <DemandaMWHome />
                </div>
                <div className="col-md-12 grid-margin stretch-card">
                    <MaximoIntercambio />
                </div>
                <div className="col-md-12 grid-margin stretch-card">
                    <MaxDemandasHistoricas />
                </div>
                <div className="col-md-12 grid-margin stretch-card">
                    <TensionesYHoras />
                </div>
                <div className='col-md-12 grid-margin stretch-card'>
                <EstacionApilable />
                </div>
                
        </div>
    </>

    );
}

export default Home;